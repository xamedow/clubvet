<?php $PHP_SELF = $_SERVER['PHP_SELF'];
if (!stripos($PHP_SELF, "index.php")) die ("Access denied");

$edit_array = array("edit", "config", "pereschet");
parse_str($_SERVER['QUERY_STRING']);

if (!in_array($action, $edit_array)) { //главная страница редактирования модуля

    // запросы для постраничной навигации
    $num = 20; // кол-во выводимых на страницу 
    $page = @$page;
    $Db->query = "SELECT COUNT(id) FROM mod_burial";
    $Db->query();
    $lRes = mysql_fetch_assoc($Db->lQueryResult);
    $posts = $lRes["COUNT(id)"]; //кол-во 
    $total = (($posts - 1) / $num) + 1;
    $total = intval($total); // общее число страниц
    $page = intval($page);
    if (empty($page) or $page < 0) $page = 1;
    if ($page > $total) $page = $total;
    $start = $page * $num - $num;
    if ($start < 0) $start = 0;

    $Db->query = "SELECT * FROM `mod_burial` ORDER BY `id` DESC LIMIT $start, $num";
    $Db->query();
    $content_mod = "";
    if (mysql_num_rows($Db->lQueryResult) > 0) {
        $content_mod .= <<<HTML
        <form method="post" action="index.php?mod=burial&action=pereschet">
			<table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%">
			    <tr class="head">
			        <td>Фамилия Имя Отчество</td>
			        <td>Дата смерти</td>
			        <td width="60" class="nobg">
			            <div class="conf">
			                <img src="img/icons/accept_item.png" class="pnghack" align="middle">
			                <img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7">
                        </div>
                    </td>
                </tr>
HTML;

        if (@$page) $pagestr = "&page=" . $page; else $pagestr = "";
        $num = 1;
        while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
            if (@$page) $pagestr = "&page=" . $page; else $pagestr = "";
            $content_mod .= <<<HTML
                <tr class="one_news">
                    <td>
                        <a href="index.php?mod=burial&action=edit&id={$lRes["id"]}">{$lRes["surname"]} {$lRes["name"]} {$lRes["second_name"]}</a>
                    </td>
                    <td>
                        <a href="index.php?mod=burial&action=edit&id={$lRes["id"]}">{$lRes["dod"]}</a>
                    </td>
                    <td><div class="conf">
HTML;

            $content_mod .= "<input type='hidden' value='0' name='act[" . $lRes['id'] . "]' />";
            if ($lRes['act'] != 0) $content_mod .= "<input type='checkbox' value='1' name='act[" . $lRes['id'] . "]' class='checkboxact' checked='checked' />"; else $content_mod .= "<input type='checkbox' value='1' name='act[" . $lRes['id'] . "]' class='checkboxact' />";
            $content_mod .= "<input type='checkbox' value='1' name='delete[" . $lRes['id'] . "]' class='checkbox'  />
		</div>";
            $content_mod .= '</td></tr>';
            $num++;
        }
        for ($i = 1; $i <= $total; $i++) {
            if ($page != $i) $navi .= '<a href=index.php?mod=burial&action=list&page=' . $i . '>' . $i . '</a> | '; else $navi .= '<b>' . $i . '</b> |';
        }

        $content_mod .= '<tr><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="pnghack" type="image" hspace="7" /></td><td>
				<script type="text/javascript">
				$(function () {
				
					$("#selall").live("click", function () {
						if (!$("#selall").is(":checked")){
							$(".checkbox").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkbox").attr("checked", true);
							$.uniform.update();
						}	
					});
					$("#selall_act").live("click", function () {
						if (!$("#selall_act").is(":checked")){
							$(".checkboxact").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkboxact").attr("checked", true);
							$.uniform.update();
						}	
					});
				});
			</script>
			<div class="conf"><input type="checkbox" value="1" class="checkboxact" id="selall_act" /><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';

        if ($total > 1) {
            $content_mod .= "<table class=\"pstrnav\"><tr><td class='all_page'>" . $navi . "</td></tr></table>";
        }
    } else {
        $content_mod .= "Список захоронений пуст.";
    }
} else {
    if ($action === 'edit') {
        if (!@$_POST["submit"]) { // если не нажата кнопка
            $Db->query = "SELECT * FROM `mod_burial` WHERE id='$id'";
            $Db->query();
            $action_edit = "1";
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            $filtered = array_map('htmlspecialchars', $lRes);
            $act = $lRes['act'] === '1' ? ' checked="checked"' : '';
            $cur_id = $id !== 'new' ? $id : 0;

            $gender = selectFromEnum('mod_burial', 'gender', $Db, $cur_id);
            $type = selectFromEnum('mod_burial', 'type', $Db, $cur_id);
            $content_mod .= <<<HTML
            <form method="post" enctype="multipart/form-data">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr height="30">
                        <td width="40%">
                            <p>Фамилия:</p>
                        </td>
                        <td width="60%">
                            <input type="text" name="surname" value="{$filtered['surname']}" size="80">
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Имя:</p>
                        </td>
                        <td width="60%">
                            <input type="text" name="name" value="{$filtered['name']}" size="80">
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Отчество:</p>
                        </td>
                        <td width="60%">
                            <input type="text" name="second_name" value="{$filtered['second_name']}" size="80">
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Пол:</p>
                        </td>
                        <td width="60%">
                            $gender
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Дата рождения:</p>
                        </td>
                        <td width="60%">
                            <input type="text" name="dob" value="{$filtered['dob']}" class="date_input">
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Дата смерти:</p>
                        </td>
                        <td width="60%">
                            <input type="text" name="dod" value="{$filtered['dod']}" class="date_input">
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Тип:</p>
                        </td>
                        <td width="60%">
                            $type
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Долгота:</p>
                        </td>
                        <td width="60%">
                            <input type="text" name="longitude" value="{$filtered['longitude']}" class="date_input">
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Широта:</p>
                        </td>
                        <td width="60%">
                            <input type="text" name="latitude" value="{$filtered['latitude']}" class="date_input">
                        </td>
                    </tr>
                    <tr height="30">
                        <td width="40%">
                            <p>Расположение:</p>
                        </td>
                        <td width="60%">
                            <div id="burial_map"></div>
                        </td>
                    </tr>
                </table>
                <p>Дополнительная информация</p><br />
                <textarea name="comments" class="texta">{$filtered['comments']}</textarea>
			    <input type="hidden" name="id" value="$id">
                <input class="check" name="act" type="checkbox"$act value="on" /> Активность<br />
                <p><input type="submit" value="Сохранить" class="but" name="submit"></p>
			</form>
HTML;

        } else {
            //обрабатываем форму
            if (!isset($_POST['img_load'])) { // обложка
                if (!empty($_FILES["image"]["name"])) {
                    $source = $_FILES["image"]["tmp_name"];
                    $myrand = rand();
                    create_thumbnail($source, $_SERVER['DOCUMENT_ROOT'] . "/upload/news/" . $myrand . ".jpg", $thumb_width = 50, $thumb_height = 50, $do_cut = true);
                    create_thumbnail($source, $_SERVER['DOCUMENT_ROOT'] . "/upload/news/bg" . $myrand . ".jpg", $thumb_width = 800, $thumb_height = 600, $do_cut = false);
                } else {
                    $myrand = "";
                }
            } else {
                $myrand = $_POST['img_load'];
            }

            $post = array_map(array($filter, 'html_filter'), $_POST);
            $act = empty($post['act']) ? 0 : 1;
            $post = array_map('setnull', $post);
            $exception = array('id'=>'', 'submit'=>'', 'act'=>'');
            if(array_key_exists('new_type', $post)) {
                addEnum($post['new_type'], 'mod_burial', 'type', $Db);
                $post['type'] = $post['new_type'];
                $exception['new_type'] = '';
            }
            $post = array_diff_key($post, $exception);
            $keys = implode(',', array_keys($post));
            $values = implodenull(',', array_values($post));
            $update_pair = getPair($post);
            if($id === 'new') {
                $Db->query = "INSERT INTO `mod_burial` ($keys) VALUES ({$values})";
            } else if($update_pair){
                $Db->query = "UPDATE mod_burial SET $update_pair, act='$act' WHERE id=$id";
            }

            if ($Db->query()) {
                exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=burial&action=list'></head></html>");
            } else {
                $content_mod .= "<h2>Ошибка сохранения записи</h2>";
            }

        }
    }
    if ($action == "pereschet") {
        //print_r($_POST);
        if (!empty($_POST["delete"])) {
            $query = "(";
            foreach ($_POST["delete"] as $key => $val) $query .= "$key,";
            $query = substr($query, 0, strlen($query) - 1) . ")";
            $Db->query = "DELETE FROM `mod_burial` WHERE `id` IN " . $query;
            $Db->query();
        }
        if (!empty($_POST["act"])) {
            foreach ($_POST["act"] as $key => $val) {
                $Db->query = "UPDATE `mod_burial` SET `act` = '" . $val . "' WHERE `id` ='" . $key . "'";
                $Db->query();
            }
        }
        if (!empty($_POST["yes"])) {
            foreach ($_POST["yes"] as $key => $val) {
                $Db->query = "UPDATE `mod_burial` SET `yes` = '" . $val . "' WHERE `id` ='" . $key . "'";
                $Db->query();
            }
        }
        exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=burial&action=list'></head></html>");
    }

    if ($action == "config") {
        if (!@$_POST["save"]) {
            $content_mod = '<h4>Настройки модуля:</h4>';
            $Db->query = "SELECT * FROM `configutarion` WHERE `mod`='" . $mod . "'";
            $Db->query();
            if (mysql_num_rows($Db->lQueryResult) > 0) {
                $content_mod .= "<form action='index.php?mod=" . $mod . "&action=config' method='post'>";
                while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                    if ($lRes[type] == "checkbox") $content_mod .= '<input type="hidden" name="' . $lRes[option] . '" value="0">';
                    if ($lRes[type] == "checkbox" && $lRes["value"] == "on") $chek = ' checked="checked"'; else $chek = '';
                    if ($lRes[type] == "text") $val = ' value="' . $lRes[value] . '"'; else $val = '';
                    $content_mod .= '<p><input class="check" name="' . $lRes[option] . '" type="' . $lRes[type] . '"' . $chek . $val . ' /> ' . $lRes[name] . '</p>';
                }
                $content_mod .= '<p><input type="submit" value="Обновить" class="but" name="save"></p>
			</form>';
            } else {
                $content_mod .= '<br /><p>Настройки для данного модуля не найдены.</p>';
            }
        } else {
            // обрабатываем форму сохранения настроек
            unset($_POST[save]);
            $query = '';
            foreach ($_POST as $key => $value) $query .= " WHEN `option`='" . $key . "' THEN '" . $value . "'";
            $Db->query = "UPDATE `configutarion` 
			SET `value` = CASE " . $query . "
			ELSE `value` END";
            $Db->query();
            exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=" . $mod . "&action=config'></head></html>");
        }
    }
}


echo $content_mod;