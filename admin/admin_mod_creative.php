<? $PHP_SELF = $_SERVER['PHP_SELF'];
if (!stripos($PHP_SELF, "index.php")) {
    die ("Access denied");
}

$edit_array = array(
    "list_museum",
    "list_art",
    "list_war",
    "list_wound",
    "list_tech",
    "pereschet_museum",
    "pereschet_art",
    "pereschet_war",
    "pereschet_wound",
    "pereschet_tech",
    "edit_art",
    "edit_war",
    "edit_wound",
    "edit_tech",
    "delete_img",
    "config",
    "pereschet",
    "edit_event",
    "pereschet_cat",
    "edit_cat",
    "list_cat",
    "list_award",
    "edit_award",
    "pereschet_award",
    "delete_img_award",
    "edit_arm",
    "pereschet_arm",
    "edit_pod",
    "pereschet_pod"
);
parse_str($_SERVER['QUERY_STRING']);
if (!in_array($action, $edit_array)) { //главная страница редактирования модуля
    exit("<html><head><meta http-equiv='Refresh' content='0; URL=index.php?mod=creative&action=list_museum'></head></html>");
} else {
    # Музей
    if ($action == "list_museum") {
        // запросы для постраничной навигации
        $num = 20; // кол-во выводимых на страницу
        $page = @$page;
        $Db->query = "SELECT COUNT(*) AS count FROM docs_files WHERE in_museum = 1";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $posts = $lRes["count"]; //кол-во
        $total = (($posts - 1) / $num) + 1;
        $total = intval($total); // общее число страниц
        $page = intval($page);
        if (empty($page) or $page < 0) {
            $page = 1;
        }
        if ($page > $total) {
            $page = $total;
        }
        $start = $page * $num - $num;
        if ($start < 0) {
            $start = 0;
        }

        $Db->query = "SELECT id_file, name_file, owner, source FROM docs_files WHERE in_museum = 1 ORDER BY `id_file` DESC LIMIT $start, $num";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $content_mod .= <<<EOD
            <form method="post" action="index.php?mod=creative&action=pereschet_museum" name="form1">
			    <table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%">
			        <tr class="head">
			            <td width="60"></td>
			            <td>Имя файла</td>
			            <td></td>
			            <td width="60" class="nobg">
			                <div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" />
			                </div>
                        </td>
                    </tr>
EOD;
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $num = 1;
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                if (@$page) {
                    $pagestr = "&page=" . $page;
                } else {
                    $pagestr = "";
                }
                $content_mod .= <<<EOD
                    <tr class="one_news">
                        <td></td>
                        <td>
                            <a href="/upload/gallery/{$lRes["owner"]}/docs/{$lRes["source"]}.jpg" class="fancybox"> {$lRes["name_file"]}</a>
                        </td>
                        <td></td>
                        <td>
                            <div class="conf">
                                <input type='checkbox' value='1' name='delete[{$lRes['id_file']}]' class='checkbox'>
                            </div>
                        </td>
                    </tr>
EOD;
                $num++;
            }
            for ($i = 1; $i <= $total; $i++) {
                if ($page != $i) {
                    $navi .= '<a href=index.php?mod=creative&action=list_museum&page=' . $i . '>' . $i . '</a> | ';
                } else {
                    $navi .= '<b>' . $i . '</b> |';
                }
            }

            $content_mod .= '<tr><td></td><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="refresh pnghack" type="image" hspace="7" /></td><td>
				<script type="text/javascript">
				$(function () {
				
					$("#selall").live("click", function () {
						if (!$("#selall").is(":checked")){
							$(".checkbox").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkbox").attr("checked", true);
							$.uniform.update();
						}	
					});
				});
			</script>
			<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';

            if ($total > 1) {
                $content_mod .= "<table class=\"pstrnav\"><tr><td class='all_page'>" . $navi . "</td></tr></table>";
            }

        } else {
            $content_mod = "Записей нет.";
        }
    }
    if ($action == "list_art") {
        // запросы для постраничной навигации
        $num = 20; // кол-во выводимых на страницу
        $page = @$page;
        $Db->query = "SELECT COUNT(*) AS count FROM creation";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $posts = $lRes["count"]; //кол-во
        $total = (($posts - 1) / $num) + 1;
        $total = intval($total); // общее число страниц
        $page = intval($page);
        if (empty($page) or $page < 0) {
            $page = 1;
        }
        if ($page > $total) {
            $page = $total;
        }
        $start = $page * $num - $num;
        if ($start < 0) {
            $start = 0;
        }

        $Db->query = "SELECT * FROM creation ORDER BY `id_creation` DESC LIMIT $start, $num";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $content_mod .= <<<EOD
            <form method="post" action="index.php?mod=creative&action=pereschet_art" name="form1">
			    <table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%">
			        <tr class="head">
			            <td width="60"></td>
			            <td>Творчество</td>
			            <td></td>
			            <td width="60" class="nobg">
			                <div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" />
			                </div>
                        </td>
                    </tr>
EOD;
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $num = 1;
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                if (@$page) {
                    $pagestr = "&page=" . $page;
                } else {
                    $pagestr = "";
                }
                $content_mod .= <<<EOD
                    <tr class="one_news">
                        <td></td>
                        <td>
                            <a href="index.php?mod=creative&action=edit_art&id={$lRes['id_creation']}"> {$lRes["name_creation"]}</a>
                        </td>
                        <td></td>
                        <td>
                            <div class="conf">
                                <input type='checkbox' value='1' name='delete[{$lRes['id_creation']}]' class='checkbox'>
                            </div>
                        </td>
                    </tr>
EOD;
                $num++;
            }
            for ($i = 1; $i <= $total; $i++) {
                if ($page != $i) {
                    $navi .= '<a href=index.php?mod=creative&action=list_art&page=' . $i . '>' . $i . '</a> | ';
                } else {
                    $navi .= '<b>' . $i . '</b> |';
                }
            }

            $content_mod .= '<tr><td></td><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="refresh pnghack" type="image" hspace="7" /></td><td>
				<script type="text/javascript">
				$(function () {

					$("#selall").live("click", function () {
						if (!$("#selall").is(":checked")){
							$(".checkbox").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkbox").attr("checked", true);
							$.uniform.update();
						}
					});
				});
			</script>
			<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';

            if ($total > 1) {
                $content_mod .= "<table class=\"pstrnav\"><tr><td class='all_page'>" . $navi . "</td></tr></table>";
            }

        } else {
            $content_mod = "Записей нет.";
        }
    }
    if ($action == "list_war") {
        // запросы для постраничной навигации
        $num = 20; // кол-во выводимых на страницу
        $page = @$page;
        $Db->query = "SELECT COUNT(*) AS count FROM list_war_info";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $posts = $lRes["count"]; //кол-во
        $total = (($posts - 1) / $num) + 1;
        $total = intval($total); // общее число страниц
        $page = intval($page);
        if (empty($page) or $page < 0) {
            $page = 1;
        }
        if ($page > $total) {
            $page = $total;
        }
        $start = $page * $num - $num;
        if ($start < 0) {
            $start = 0;
        }

        $Db->query = "SELECT * FROM list_war_info ORDER BY `id_war` DESC LIMIT $start, $num";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $content_mod .= <<<EOD
            <form method="post" action="index.php?mod=creative&action=pereschet_war" name="form1">
			    <table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%">
			        <tr class="head">
			            <td width="60"></td>
			            <td>Боевой путь</td>
			            <td></td>
			            <td width="60" class="nobg">
			                <div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" />
			                </div>
                        </td>
                    </tr>
EOD;
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $num = 1;
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                if (@$page) {
                    $pagestr = "&page=" . $page;
                } else {
                    $pagestr = "";
                }
                $text = substring($lRes["text_war"]);
                $content_mod .= <<<EOD
                    <tr class="one_news">
                        <td></td>
                        <td>
                            <a href="index.php?mod=creative&action=edit_war&id={$lRes['id_war']}">$text</a>
                        </td>
                        <td></td>
                        <td>
                            <div class="conf">
                                <input type='checkbox' value='1' name='delete[{$lRes['id_war']}]' class='checkbox'>
                            </div>
                        </td>
                    </tr>
EOD;
                $num++;
            }
            for ($i = 1; $i <= $total; $i++) {
                if ($page != $i) {
                    $navi .= '<a href=index.php?mod=creative&action=list_war&page=' . $i . '>' . $i . '</a> | ';
                } else {
                    $navi .= '<b>' . $i . '</b> |';
                }
            }

            $content_mod .= '<tr><td></td><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="refresh pnghack" type="image" hspace="7" /></td><td>
				<script type="text/javascript">
				$(function () {

					$("#selall").live("click", function () {
						if (!$("#selall").is(":checked")){
							$(".checkbox").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkbox").attr("checked", true);
							$.uniform.update();
						}
					});
				});
			</script>
			<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';

            if ($total > 1) {
                $content_mod .= "<table class=\"pstrnav\"><tr><td class='all_page'>" . $navi . "</td></tr></table>";
            }

        } else {
            $content_mod = "Записей нет.";
        }
    }
    if ($action == "list_wound") {
        // запросы для постраничной навигации
        $num = 20; // кол-во выводимых на страницу
        $page = @$page;
        $Db->query = "SELECT COUNT(*) AS count FROM list_wound_info";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $posts = $lRes["count"]; //кол-во
        $total = (($posts - 1) / $num) + 1;
        $total = intval($total); // общее число страниц
        $page = intval($page);
        if (empty($page) or $page < 0) {
            $page = 1;
        }
        if ($page > $total) {
            $page = $total;
        }
        $start = $page * $num - $num;
        if ($start < 0) {
            $start = 0;
        }

        $Db->query = "SELECT * FROM list_wound_info ORDER BY `id_wound` DESC LIMIT $start, $num";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $content_mod .= <<<EOD
            <form method="post" action="index.php?mod=creative&action=pereschet_wound" name="form1">
			    <table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%">
			        <tr class="head">
			            <td width="60"></td>
			            <td>Ранения</td>
			            <td></td>
			            <td width="60" class="nobg">
			                <div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" />
			                </div>
                        </td>
                    </tr>
EOD;
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $num = 1;
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                if (@$page) {
                    $pagestr = "&page=" . $page;
                } else {
                    $pagestr = "";
                }
                $text = substring($lRes["text_wound"]);
                $content_mod .= <<<EOD
                    <tr class="one_news">
                        <td></td>
                        <td>
                            <a href="index.php?mod=creative&action=edit_wound&id={$lRes['id_wound']}">$text</a>
                        </td>
                        <td></td>
                        <td>
                            <div class="conf">
                                <input type='checkbox' value='1' name='delete[{$lRes['id_wound']}]' class='checkbox'>
                            </div>
                        </td>
                    </tr>
EOD;
                $num++;
            }
            for ($i = 1; $i <= $total; $i++) {
                if ($page != $i) {
                    $navi .= '<a href=index.php?mod=creative&action=list_wound&page=' . $i . '>' . $i . '</a> | ';
                } else {
                    $navi .= '<b>' . $i . '</b> |';
                }
            }

            $content_mod .= '<tr><td></td><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="refresh pnghack" type="image" hspace="7" /></td><td>
				<script type="text/javascript">
				$(function () {

					$("#selall").live("click", function () {
						if (!$("#selall").is(":checked")){
							$(".checkbox").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkbox").attr("checked", true);
							$.uniform.update();
						}
					});
				});
			</script>
			<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';

            if ($total > 1) {
                $content_mod .= "<table class=\"pstrnav\"><tr><td class='all_page'>" . $navi . "</td></tr></table>";
            }

        } else {
            $content_mod = "Записей нет.";
        }
    }
    if ($action == "list_tech") {
        // запросы для постраничной навигации
        $num = 20; // кол-во выводимых на страницу
        $page = @$page;
        $Db->query = "SELECT COUNT(*) AS count FROM list_teh_info";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $posts = $lRes["count"]; //кол-во
        $total = (($posts - 1) / $num) + 1;
        $total = intval($total); // общее число страниц
        $page = intval($page);
        if (empty($page) or $page < 0) {
            $page = 1;
        }
        if ($page > $total) {
            $page = $total;
        }
        $start = $page * $num - $num;
        if ($start < 0) {
            $start = 0;
        }

        $Db->query = "SELECT * FROM list_teh_info ORDER BY `id_teh` DESC LIMIT $start, $num";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $content_mod .= <<<EOD
            <form method="post" action="index.php?mod=creative&action=pereschet_tech" name="form1">
			    <table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%">
			        <tr class="head">
			            <td width="60"></td>
			            <td>Техника</td>
			            <td></td>
			            <td width="60" class="nobg">
			                <div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" />
			                </div>
                        </td>
                    </tr>
EOD;
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $num = 1;
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                if (@$page) {
                    $pagestr = "&page=" . $page;
                } else {
                    $pagestr = "";
                }
                $text = substring($lRes["text_teh"]);
                $content_mod .= <<<EOD
                    <tr class="one_news">
                        <td></td>
                        <td>
                            <a href="index.php?mod=creative&action=edit_tech&id={$lRes['id_teh']}">$text</a>
                        </td>
                        <td></td>
                        <td>
                            <div class="conf">
                                <input type='checkbox' value='1' name='delete[{$lRes['id_teh']}]' class='checkbox'>
                            </div>
                        </td>
                    </tr>
EOD;
                $num++;
            }
            for ($i = 1; $i <= $total; $i++) {
                if ($page != $i) {
                    $navi .= '<a href=index.php?mod=creative&action=list_tech&page=' . $i . '>' . $i . '</a> | ';
                } else {
                    $navi .= '<b>' . $i . '</b> |';
                }
            }

            $content_mod .= '<tr><td></td><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="refresh pnghack" type="image" hspace="7" /></td><td>
				<script type="text/javascript">
				$(function () {

					$("#selall").live("click", function () {
						if (!$("#selall").is(":checked")){
							$(".checkbox").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkbox").attr("checked", true);
							$.uniform.update();
						}
					});
				});
			</script>
			<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';

            if ($total > 1) {
                $content_mod .= "<table class=\"pstrnav\"><tr><td class='all_page'>" . $navi . "</td></tr></table>";
            }

        } else {
            $content_mod = "Записей нет.";
        }
    }
    if ($action == "delete_img") {
        $Db->query = "SELECT `img_country` FROM `list_country` WHERE `id_country`='" . $id . "'";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $Db->query = "UPDATE `list_country` SET `img_country`='' WHERE `id_country` = '" . $id . "'";
        $Db->query();

        $img = $_SERVER['DOCUMENT_ROOT'] . $lRes['img_country'];
        unlink($img);
        exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=war&action=edit_cat&id=" . $id . "'></head></html>");
    }

    if ($action == "pereschet_museum") {
        if (!empty($_POST["delete"])) {
            $query = "(";
            foreach ($_POST["delete"] as $key => $val) {
                $query .= "$key,";
            }
            $query = substr($query, 0, strlen($query) - 1) . ")";

            $Db->query = "UPDATE docs_files SET in_museum = 0 WHERE `id_file` IN " . $query;
            $Db->query();
        }
        exit("<html><head><meta http-equiv='Refresh' content='0; URL=index.php?mod=creative&action=list_museum'></head></html>");
    }
    if ($action == "pereschet_art") {
        if (!empty($_POST["delete"])) {
            $query = "(";
            foreach ($_POST["delete"] as $key => $val) {
                $query .= "$key,";
            }
            $query = substr($query, 0, strlen($query) - 1) . ")";

            $Db->query = "DELETE FROM `creation` WHERE `id_creation` IN " . $query;
            $Db->query();
        }
        exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=creative&action=list_art'></head></html>");
    }
    if ($action == "pereschet_war") {
        if (!empty($_POST["delete"])) {
            $query = "(";
            foreach ($_POST["delete"] as $key => $val) {
                $query .= "$key,";
            }
            $query = substr($query, 0, strlen($query) - 1) . ")";

            $Db->query = "DELETE FROM `list_war_info` WHERE `id_war` IN " . $query;
            $Db->query();
        }
        exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=creative&action=list_art'></head></html>");
    }
    if ($action == "pereschet_wound") {
        if (!empty($_POST["delete"])) {
            $query = "(";
            foreach ($_POST["delete"] as $key => $val) {
                $query .= "$key,";
            }
            $query = substr($query, 0, strlen($query) - 1) . ")";

            $Db->query = "DELETE FROM `list_wound_info` WHERE `id_wound` IN " . $query;
            $Db->query();
        }
        exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=creative&action=list_art'></head></html>");
    }
    if ($action == "pereschet_tech") {
        if (!empty($_POST["delete"])) {
            $query = "(";
            foreach ($_POST["delete"] as $key => $val) {
                $query .= "$key,";
            }
            $query = substr($query, 0, strlen($query) - 1) . ")";

            $Db->query = "DELETE FROM `list_teh_info` WHERE `id_teh` IN " . $query;
            $Db->query();
        }
        exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=creative&action=list_art'></head></html>");
    }
    if ($action == "edit_art") {
            $Db->query = "SELECT * FROM creation WHERE id_creation='$id' LIMIT 1";
            $Db->query();
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $content_mod .= <<<EOD
                <h1>
                    {$lRes['name_creation']}
                </h1>
                <p>
                    {$lRes['text_creation']}
                </p>
EOD;

    }
    if ($action == "edit_war") {
            $Db->query = "SELECT * FROM list_war_info WHERE id_war='$id' LIMIT 1";
            $Db->query();
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $content_mod .= <<<EOD
                <h1>Боевой путь.</h1>
                <p>
                    {$lRes['text_war']}
                </p>
EOD;
    }
    if ($action == "edit_wound") {
            $Db->query = "SELECT * FROM list_wound_info WHERE id_wound='$id' LIMIT 1";
            $Db->query();
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $content_mod .= <<<EOD
                <h1>Ранения.</h1>
                <p>
                    {$lRes['text_wound']}
                </p>
EOD;
    }
    if ($action == "edit_tech") {
            $Db->query = "SELECT * FROM list_teh_info WHERE id_teh='$id' LIMIT 1";
            $Db->query();
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $content_mod .= <<<EOD
                <h1>Техника.</h1>
                <p>
                    {$lRes['text_teh']}
                </p>
EOD;
    }
    if ($action == "edit_arm") {
        if (!@$_POST["submit"]) { // если не нажата кнопка
            $Db->query = "SELECT * FROM `list_country_arm` WHERE `id_arm`='" . $id . "' LIMIT 1";
            $Db->query();
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            $country = $lRes["rel_country"];
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $lang[$lRes['lang']] = ' selected="selected"';
            $content_mod .= '<form method="post" enctype="multipart/form-data" name="newsform">
			<table border="0" cellspacing="0" cellpadding="0">
  			<tr height="30">
    		<td width="40%"><p>Название типа войск:</p></td>
    		<td width="60%"><input type="text" name="name" value="' . htmlspecialchars(
                    stripslashes($lRes['name_arm'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Страна:</p></td>
    		<td width="60%">';

            $Db->query = "SELECT * FROM `list_country` ORDER BY `name_country_rus`";
            $Db->query();
            if (mysql_num_rows($Db->lQueryResult) > 0) {
                $content_mod .= '<select name="country">';
                while ($lRes = mysql_fetch_assoc(
                    $Db->lQueryResult
                )) {
                    if ($country == $lRes["id_country"]) {
                        $content_mod .= '<option value="' . $lRes["id_country"] . '" selected="selected">' . $lRes["name_country_rus"] . '</option>';
                    } else {
                        $content_mod .= '<option value="' . $lRes["id_country"] . '">' . $lRes["name_country_rus"] . '</option>';
                    }
                }
                $content_mod .= '</select>';
            }
            $content_mod .= '</td>
  			</tr>
			</table>
			<input type="hidden" name="id" value="' . $id . '">
			<p><input type="submit" value="Сохранить" class="but" name="submit"></p>
			</form>
			';

            if ($id != "new") {
                $content_mod .= '<div style="margin-top:10px;"><a href="index.php?mod=war&action=edit_pod&id=new&war=' . $id . '" class="red">+ Добавить новое воинское формирование</a></div>';
                $Db->query = "SELECT * FROM `list_country_arm_pod` WHERE `rel_arm`='" . $id . "' ORDER BY `id_pod` DESC";
                $Db->query();
                if (mysql_num_rows($Db->lQueryResult) > 0) {
                    $content_mod .= '<br /><form method="post" action="index.php?mod=war&action=pereschet_pod" name="form1">
				<table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%"><tr class="head"><td>Воинское формирование</td><td width="130"></td><td width="60" class="nobg"><div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" /></div></td></tr>';
                    if (@$page) {
                        $pagestr = "&page=" . $page;
                    } else {
                        $pagestr = "";
                    }
                    $num = 1;
                    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                        if (@$page) {
                            $pagestr = "&page=" . $page;
                        } else {
                            $pagestr = "";
                        }
                        $content_mod .= '<tr class="one_news">
									<td><a href="index.php?mod=war&action=edit_pod&id=' . $lRes["id_pod"] . '&war=' . $id . '"> ' . $lRes["name_pod"] . '</a></td>
									<td></td>
									<td><div class="conf">';

                        $content_mod .= "<input type='checkbox' value='1' name='delete[" . $lRes['id_pod'] . "]' class='checkbox'  />
			</div>";
                        $content_mod .= '</td>
									</tr>';
                        $num++;
                    }

                    $content_mod .= '<tr><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="pnghack" type="image" hspace="7" /></td><td>
					<script type="text/javascript">
					$(function () {
					
						$("#selall").live("click", function () {
							if (!$("#selall").is(":checked")){
								$(".checkbox").removeAttr("checked");
								$.uniform.update();
							}
							else{
								$(".checkbox").attr("checked", true);
								$.uniform.update();
							}	
						});
					});
				</script>
				<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';
                }
            }


        } else //обрабатываем форму
        {
            $filter = new filter;
            $name_war = $filter->html_filter($_POST["name"]);
            $id = $_POST["id"];
            $cat = $_POST["country"];

            $Db->query = "INSERT INTO `list_country_arm` (`id_arm`, `name_arm`, `rel_country`)
						VALUES ('" . $id . "','" . $name_war . "','" . $cat . "')
						ON DUPLICATE KEY UPDATE
						`id_arm`=VALUES(`id_arm`),
						`name_arm`=VALUES(`name_arm`),
						`rel_country`=VALUES(`rel_country`)";

            if ($Db->query()
            ) {
                exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=war&action=edit_cat&id=" . $cat . "'></head></html>");
            }

        }

    }
    if ($action == "edit") {
        if (!@$_POST["submit"]) { // если не нажата кнопка
            $Db->query = "SELECT * FROM `mod_war` WHERE id_war='" . $id . "' LIMIT 1";
            $Db->query();
            $action_edit = "1";
            $lRes = mysql_fetch_assoc($Db->lQueryResult);

            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $lang[$lRes['lang']] = ' selected="selected"';
            $content_mod .= '<form method="post" enctype="multipart/form-data" name="newsform">
			<table border="0" cellspacing="0" cellpadding="0">
  			<tr height="30">
    		<td width="40%"><p>Временной период:</p></td>
    		<td width="60%"><input type="text" name="name_war" value="' . htmlspecialchars(
                    stripslashes($lRes['name_war'])
                ) . '" size="80"> 						</td>
  			</tr>
			</table>
			<input type="hidden" name="id" value="' . $id . '">
			<p><input type="submit" value="Сохранить" class="but" name="submit"></p>
			</form>
			';

            if ($id != "new") {
                $content_mod .= '<div style="margin-top:10px;"><a href="index.php?mod=war&action=edit_event&id=new&war=' . $id . '" class="red">+ Добавить новое сражение</a></div>';
                $Db->query = "SELECT * FROM `mod_war_event` WHERE `rel_war`='" . $id . "' ORDER BY `id_event` DESC";
                $Db->query();
                if (mysql_num_rows($Db->lQueryResult) > 0) {
                    $content_mod .= '<br /><form method="post" action="index.php?mod=war&action=pereschet_event" name="form1">
				<table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%"><tr class="head"><td>Сражение</td><td width="130"></td><td width="60" class="nobg"><div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" /></div></td></tr>';
                    if (@$page) {
                        $pagestr = "&page=" . $page;
                    } else {
                        $pagestr = "";
                    }
                    $num = 1;
                    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                        if (@$page) {
                            $pagestr = "&page=" . $page;
                        } else {
                            $pagestr = "";
                        }
                        $content_mod .= '<tr class="one_news">
									<td><a href="index.php?mod=war&action=edit_event&id=' . $lRes["id_event"] . '&war=' . $id . '"> ' . $lRes["name_event_rus"] . '</a></td>
									<td></td>
									<td><div class="conf">';

                        $content_mod .= "<input type='checkbox' value='1' name='delete[" . $lRes['id_event'] . "]' class='checkbox'  />
			</div>";
                        $content_mod .= '</td>
									</tr>';
                        $num++;
                    }

                    $content_mod .= '<tr><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="pnghack" type="image" hspace="7" /></td><td>
					<script type="text/javascript">
					$(function () {
					
						$("#selall").live("click", function () {
							if (!$("#selall").is(":checked")){
								$(".checkbox").removeAttr("checked");
								$.uniform.update();
							}
							else{
								$(".checkbox").attr("checked", true);
								$.uniform.update();
							}	
						});
					});
				</script>
				<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';
                }
            }

        } else //обрабатываем форму
        {
            $filter = new filter;
            $name_war = $filter->html_filter($_POST["name_war"]);
            $id = $_POST["id"];

            $Db->query = "INSERT INTO `mod_war` (`id_war`, `name_war`)
						VALUES ('" . $id . "','" . $name_war . "')
						ON DUPLICATE KEY UPDATE
						`id_war`=VALUES(`id_war`),
						`name_war`=VALUES(`name_war`)";

            if ($Db->query()
            ) {
                exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=war&action=list'></head></html>");
            }

        }
    }

    if ($action == "list_cat") {
        $Db->query = "SELECT * FROM `list_country` ORDER BY `name_country_rus`";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $content_mod .= '<br /><form method="post" action="index.php?mod=war&action=pereschet_cat" name="form1">
			<table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%"><tr class="head"><td>Страна (рус)</td><td>Страна (анг)</td><td>Страна (нем)</td><td width="60" class="nobg"><div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" /></div></td></tr>';
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $num = 1;
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                if (@$page) {
                    $pagestr = "&page=" . $page;
                } else {
                    $pagestr = "";
                }
                $content_mod .= '<tr class="one_news">
								<td><a href="index.php?mod=war&action=edit_cat&id=' . $lRes["id_country"] . '&war=' . $id . '">' . $lRes["name_country_rus"] . '</a></td>
								<td>' . $lRes["name_country_eng"] . '</td>
								<td>' . $lRes["name_country_ger"] . '</td>
								<td><div class="conf">';

                $content_mod .= "<input type='checkbox' value='1' name='delete[" . $lRes['id_country'] . "]' class='checkbox'  />
		</div>";
                $content_mod .= '</td>
								</tr>';
                $num++;
            }

            $content_mod .= '<tr><td></td><td></td><td><input src="img/icons/tick_red_icon.png" align="right" class="pnghack" type="image" hspace="7" /></td><td>
				<script type="text/javascript">
				$(function () {
				
					$("#selall").live("click", function () {
						if (!$("#selall").is(":checked")){
							$(".checkbox").removeAttr("checked");
							$.uniform.update();
						}
						else{
							$(".checkbox").attr("checked", true);
							$.uniform.update();
						}	
					});
				});
			</script>
			<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';
        }
    }
    if ($action == "edit_event") {
        if (!@$_POST["submit"]) { // если не нажата кнопка
            $Db->query = "SELECT * FROM `mod_war_event` WHERE `id_event`='" . $id . "' LIMIT 1";
            $Db->query();
            $action_edit = "1";
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            if (empty($war)) {
                $war = $lRes["rel_war"];
            }
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $lang[$lRes['lang']] = ' selected="selected"';
            $content_mod .= '<form method="post" enctype="multipart/form-data" name="newsform">
			<table border="0" cellspacing="0" cellpadding="0">
  			<tr height="30">
    		<td width="40%"><p>Название сражения на русском:</p></td>
    		<td width="60%"><input type="text" name="name_event_rus" value="' . htmlspecialchars(
                    stripslashes($lRes['name_event_rus'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Название сражения на английском:</p></td>
    		<td width="60%"><input type="text" name="name_event_eng" value="' . htmlspecialchars(
                    stripslashes($lRes['name_event_eng'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Название сражения на немецком:</p></td>
    		<td width="60%"><input type="text" name="name_event_ger" value="' . htmlspecialchars(
                    stripslashes($lRes['name_event_ger'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Ссылка на википедию:</p></td>
    		<td width="60%"><input type="text" name="link" value="' . $lRes['link'] . '" size="80"></td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Временной период:</p></td>
    		<td width="60%">';

            $Db->query = "SELECT * FROM `mod_war` ORDER BY `name_war`";
            $Db->query();
            if (mysql_num_rows($Db->lQueryResult) > 0) {
                $content_mod .= '<select name="war">';
                while ($lRes = mysql_fetch_assoc(
                    $Db->lQueryResult
                )) {
                    if ($war == $lRes["id_war"]) {
                        $content_mod .= '<option value="' . $lRes["id_war"] . '" selected="selected">' . $lRes["name_war"] . '</option>';
                    } else {
                        $content_mod .= '<option value="' . $lRes["id_war"] . '">' . $lRes["name_war"] . '</option>';
                    }
                }
                $content_mod .= '</select>';
            }
            $content_mod .= '</td>
  			</tr>
			</table>
			<input type="hidden" name="id" value="' . $id . '">
			<p><input type="submit" value="Сохранить" class="but" name="submit"></p>
			</form>
			';
        } else //обрабатываем форму
        {
            $filter = new filter;
            $name_event_rus = $filter->html_filter($_POST["name_event_rus"]);
            $name_event_eng = $filter->html_filter($_POST["name_event_eng"]);
            $name_event_ger = $filter->html_filter($_POST["name_event_ger"]);
            $link = mysql_escape_string($_POST["link"]);
            $id = $_POST["id"];
            $war = $_POST["war"];

            $Db->query = "INSERT INTO `mod_war_event` (`id_event`, `name_event_rus`, `name_event_eng`, `name_event_ger`, `link`, `rel_war`)
						VALUES ('" . $id . "','" . $name_event_rus . "','" . $name_event_eng . "','" . $name_event_ger . "','" . $link . "','" . $war . "')
						ON DUPLICATE KEY UPDATE
						`id_event`=VALUES(`id_event`),
						`name_event_rus`=VALUES(`name_event_rus`),
						`name_event_eng`=VALUES(`name_event_eng`),
						`name_event_ger`=VALUES(`name_event_ger`),
						`link`=VALUES(`link`),
						`rel_war`=VALUES(`rel_war`)";

            if ($Db->query()
            ) {
                exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=war&action=edit&id=" . $war . "'></head></html>");
            }
        }
    }

    if ($action == "edit_award") {
        if (!@$_POST["submit"]) { // если не нажата кнопка
            $Db->query = "SELECT * FROM `mod_award` WHERE `id_award`='" . $id . "' LIMIT 1";
            $Db->query();
            $action_edit = "1";
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            $country = $lRes["rel_country"];
            if ($lRes['in_main'] == 1) {
                $chek = " checked";
            }
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $lang[$lRes['lang']] = ' selected="selected"';
            $content_mod .= '<form method="post" enctype="multipart/form-data" name="newsform">
			<table border="0" cellspacing="0" cellpadding="0">
  			<tr height="30">
    		<td width="40%"><p>Название награды/медали:</p></td>
    		<td width="60%"><input type="text" name="name" value="' . htmlspecialchars(
                    stripslashes($lRes['name_award'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Описание награды/медали:</p></td>
    		<td width="60%"><input type="text" name="text" value="' . htmlspecialchars(
                    stripslashes($lRes['text_award'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Изображение:<br />
			<span class="small">формат PNG, размер 250х300 пикс.</span></p></td>
    		<td width="60%">';
            if ($id == "new" or empty($lRes['img_award'])) {
                $content_mod .= '<input name="image" type="file">';
            } else {
                $content_mod .= "<img src='" . $lRes['img_award'] . "' align=middle hspace=10 border=0><a title='Удалить картинку' data-action='delete_img_award' data-mod='war' data-name='флаг' data-id='" . $id . "'><img src='img/icons/trash.png' border='0' border=0 /></a><input type='hidden' name='img_load' value='" . $lRes['img_award'] . "'>";
            }
            $content_mod .= '</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Страна:</p></td>
    		<td width="60%">';

            $Db->query = "SELECT * FROM `list_country` ORDER BY `name_country_rus`";
            $Db->query();
            if (mysql_num_rows($Db->lQueryResult) > 0) {
                $content_mod .= '<select name="country">';
                while ($lRes = mysql_fetch_assoc(
                    $Db->lQueryResult
                )) {
                    if ($country == $lRes["id_country"]) {
                        $content_mod .= '<option value="' . $lRes["id_country"] . '" selected="selected">' . $lRes["name_country_rus"] . '</option>';
                    } else {
                        $content_mod .= '<option value="' . $lRes["id_country"] . '">' . $lRes["name_country_rus"] . '</option>';
                    }
                }
                $content_mod .= '</select>';
            }
            $content_mod .= '</td>
  			</tr>
			</table>
			<input type="hidden" name="id" value="' . $id . '">
			<p><br /><input type="submit" value="Сохранить" class="but" name="submit"></p>
			</form>
			';
        } else //обрабатываем форму
        {

            if (!isset($_POST['img_load'])) {
                if (!empty($_FILES["image"]["name"])) {
                    $source = $_FILES["image"]["tmp_name"];
                    $myrand = rand();
                    $img_name_full = "/upload/award/" . $myrand . ".png";
                    move_uploaded_file($source, $_SERVER['DOCUMENT_ROOT'] . $img_name_full);
                } else {
                    $myrand = "";
                }
            } else {
                $myrand = $_POST['img_load'];
            }

            $filter = new filter;
            $name = $filter->html_filter($_POST["name"]);
            $text = $filter->html_filter($_POST["text"]);

            $country = $_POST["country"];
            $id = $_POST["id"];

            $Db->query = "INSERT INTO `mod_award` (`id_award`, `name_award`, `text_award`, `img_award`, `rel_country`)
						VALUES ('" . $id . "','" . $name . "','" . $text . "','" . $myrand . "','" . $country . "')
						ON DUPLICATE KEY UPDATE
						`id_award`=VALUES(`id_award`),
						`name_award`=VALUES(`name_award`),
						`text_award`=VALUES(`text_award`),
						`img_award`=VALUES(`img_award`),
						`rel_country`=VALUES(`rel_country`)";

            if ($Db->query()
            ) {
                exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=war&action=list_award'></head></html>");
            }
        }
    }

    if ($action == "edit_cat") {
        if (!@$_POST["submit"]) { // если не нажата кнопка
            $Db->query = "SELECT * FROM `list_country` WHERE `id_country`='" . $id . "' LIMIT 1";
            $Db->query();
            $action_edit = "1";
            $lRes = mysql_fetch_assoc($Db->lQueryResult);
            if ($lRes['in_main'] == 1) {
                $chek = " checked";
            }
            if (@$page) {
                $pagestr = "&page=" . $page;
            } else {
                $pagestr = "";
            }
            $lang[$lRes['lang']] = ' selected="selected"';
            $content_mod .= '<form method="post" enctype="multipart/form-data" name="newsform">
			<table border="0" cellspacing="0" cellpadding="0">
  			<tr height="30">
    		<td width="40%"><p>Название страны на русском:</p></td>
    		<td width="60%"><input type="text" name="name_country_rus" value="' . htmlspecialchars(
                    stripslashes($lRes['name_country_rus'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Название страны на английском:</p></td>
    		<td width="60%"><input type="text" name="name_country_eng" value="' . htmlspecialchars(
                    stripslashes($lRes['name_country_eng'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Название страны на немецком:</p></td>
    		<td width="60%"><input type="text" name="name_country_ger" value="' . htmlspecialchars(
                    stripslashes($lRes['name_country_ger'])
                ) . '" size="80"> 						</td>
  			</tr>
			<tr height="30">
    		<td width="40%"><p>Флаг:<br />
			<span class="small">формат JPG</span></p></td>
    		<td width="60%">';
            if ($id == "new" or empty($lRes['img_country'])) {
                $content_mod .= '<input name="image" type="file">';
            } else {
                $content_mod .= "<img src='" . $lRes['img_country'] . "' align=middle hspace=10 border=0><a title='Удалить картинку' data-action='delete_img' data-mod='war' data-name='флаг' data-id='" . $id . "'><img src='img/icons/trash.png' border='0' border=0 /></a><input type='hidden' name='img_load' value='" . $lRes['img_country'] . "'>";
            }
            $content_mod .= '</td>
  			</tr>
			</table>
			<input type="hidden" name="id" value="' . $id . '">
			<input class="check" name="main" type="checkbox"' . $chek . ' value="on" /> Вывести на главную
			<p><br /><input type="submit" value="Сохранить" class="but" name="submit"></p>
			</form>
			';


            if ($id != "new") {
                $content_mod .= '<div style="margin-top:10px;"><a href="index.php?mod=war&action=edit_arm&id=new&country=' . $id . '" class="red">+ Добавить новый род войск к этой стране</a></div>';
                $Db->query = "SELECT * FROM `list_country_arm` WHERE `rel_country`='" . $id . "' ORDER BY `id_arm` DESC";
                $Db->query();
                if (mysql_num_rows($Db->lQueryResult) > 0) {
                    $content_mod .= '<br /><form method="post" action="index.php?mod=war&action=pereschet_arm" name="form1">
				<table border="0" cellspacing="0" cellpadding="0" id="my-list" width="100%"><tr class="head"><td>Род войск</td><td width="130"></td><td width="60" class="nobg"><div class="conf"><img src="img/icons/trash.png" class="pnghack" align="middle" hspace="7" /></div></td></tr>';
                    if (@$page) {
                        $pagestr = "&page=" . $page;
                    } else {
                        $pagestr = "";
                    }
                    $num = 1;
                    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {

                        if (@$page) {
                            $pagestr = "&page=" . $page;
                        } else {
                            $pagestr = "";
                        }
                        $content_mod .= '<tr class="one_news">
									<td><a href="index.php?mod=war&action=edit_arm&id=' . $lRes["id_arm"] . '&war=' . $id . '"> ' . $lRes["name_arm"] . '</a></td>
									<td></td>
									<td><div class="conf">';

                        $content_mod .= "<input type='checkbox' value='1' name='delete[" . $lRes['id_arm'] . "]' class='checkbox'  />
			</div>";
                        $content_mod .= '</td>
									</tr>';
                        $num++;
                    }

                    $content_mod .= '<tr><td></td><td><input src="img/icons/tick_red_icon.png" align="middle" class="pnghack" type="image" hspace="7" /></td><td>
					<script type="text/javascript">
					$(function () {
					
						$("#selall").live("click", function () {
							if (!$("#selall").is(":checked")){
								$(".checkbox").removeAttr("checked");
								$.uniform.update();
							}
							else{
								$(".checkbox").attr("checked", true);
								$.uniform.update();
							}	
						});
					});
				</script>
				<div class="conf"><input type="checkbox" value="1" class="checkbox" id="selall" /></div></td></tr></table></form>';
                }
            }

        } else //обрабатываем форму
        {

            if (!isset($_POST['img_load'])) {
                if (!empty($_FILES["image"]["name"])) {
                    $source = $_FILES["image"]["tmp_name"];
                    $myrand = rand();
                    $img_name_full = "/upload/country/" . $myrand . ".jpg";
                    create_thumbnail(
                        $source,
                        $_SERVER['DOCUMENT_ROOT'] . $img_name_full,
                        $thumb_width = 76,
                        $thumb_height = 53,
                        $do_cut = true
                    );
                } else {
                    $img_name_full = "";
                }
            } else {
                $img_name_full = $_POST['img_load'];
            }

            $filter = new filter;
            $name_country_rus = $filter->html_filter($_POST["name_country_rus"]);
            $name_country_eng = $filter->html_filter($_POST["name_country_eng"]);
            $name_country_ger = $filter->html_filter($_POST["name_country_ger"]);
            if (@$_POST["main"]) {
                $main = 1;
            } else {
                $main = 0;
            }
            $id = $_POST["id"];

            $Db->query = "INSERT INTO `list_country` (`id_country`, `name_country_rus`, `name_country_eng`, `name_country_ger`, `img_country`, `in_main`)
						VALUES ('" . $id . "','" . $name_country_rus . "','" . $name_country_eng . "','" . $name_country_ger . "','" . $img_name_full . "','" . $main . "')
						ON DUPLICATE KEY UPDATE
						`id_country`=VALUES(`id_country`),
						`name_country_rus`=VALUES(`name_country_rus`),
						`name_country_eng`=VALUES(`name_country_eng`),
						`name_country_ger`=VALUES(`name_country_ger`),
						`img_country`=VALUES(`img_country`),
						`in_main`=VALUES(`in_main`)";

            if ($Db->query()
            ) {
                exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=war&action=list_cat'></head></html>");
            }
        }
    }
}
if ($action == "config") {
    if (!@$_POST["save"]) {
        $content_mod = '<h4>Настройки модуля:</h4>';
        $Db->query = "SELECT * FROM `configutarion` WHERE `mod`='" . $mod . "'";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $content_mod .= "<form action='index.php?mod=" . $mod . "&action=config' method='post'>";
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                if ($lRes[type] == "checkbox") {
                    $content_mod .= '<input type="hidden" name="' . $lRes[option] . '" value="0">';
                }
                if ($lRes[type] == "checkbox" && $lRes["value"] == "on") {
                    $chek = ' checked="checked"';
                } else {
                    $chek = '';
                }
                if ($lRes[type] == "text") {
                    $val = ' value="' . $lRes[value] . '"';
                } else {
                    $val = '';
                }
                $content_mod .= '<p><input class="check" name="' . $lRes[option] . '" type="' . $lRes[type] . '"' . $chek . $val . ' /> ' . $lRes[name] . '</p>';
            }
            $content_mod .= '<p><input type="submit" value="Обновить" class="but" name="save"></p>
			</form>';
        } else {
            $content_mod .= '<br /><p>Настройки для данного модуля не найдены.</p>';
        }
    } else {
        // обрабатываем форму сохранения настроек
        unset($_POST[save]);
        $query = '';
        foreach ($_POST as $key => $value) {
            $query .= " WHEN `option`='" . $key . "' THEN '" . $value . "'";
        }
        $Db->query = "UPDATE `configutarion`
			SET `value` = CASE " . $query . "
			ELSE `value` END";
        $Db->query();
        exit("<html><head><meta  http-equiv='Refresh' content='0; URL=index.php?mod=" . $mod . "&action=config'></head></html>");
    }
}

echo $content_mod;