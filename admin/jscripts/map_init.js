(function () {
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init() {
        var lat = $('input[name="latitude"]'),
            long = $('input[name="longitude"]'),
            longVal = long.val() || 37.622093,
            latVal = lat.val() || 55.753994,
            myPlacemark,
            myMap = new ymaps.Map('burial_map', {
                center: [latVal, longVal],
                zoom: 12,
                controls: []
            }),
            target = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: 'Место захоронения'
            }, {
                iconLayout: 'default#image'
            });
        myMap.geoObjects.add(target);
        // Слушаем клик на карте
        myMap.events.add('click', function (e) {
            var coords = e.get('coords');
            long.val(coords[1]);
            lat.val(coords[0]);
            // Если метка уже создана – просто передвигаем ее
            if (myPlacemark) {
                myPlacemark.geometry.setCoordinates(coords);
            }
            // Если нет – создаем.
            else {
                myPlacemark = createPlacemark(coords);
                myMap.geoObjects.add(myPlacemark);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemark.events.add('dragend', function () {
                    getAddress(myPlacemark.geometry.getCoordinates());
                });
            }
            getAddress(coords);
        });

        // Создание метки
        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconContent: 'поиск...'
            }, {
                preset: 'islands#violetStretchyIcon',
                draggable: true
            });
        }

        // Определяем адрес по координатам (обратное геокодирование)
        function getAddress(coords) {
            myPlacemark.properties.set('iconContent', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);

                myPlacemark.properties
                    .set({
                        iconContent: firstGeoObject.properties.get('name'),
                        balloonContent: firstGeoObject.properties.get('text')
                    });
            });
        }
    }
}());