(function () {
    var select = $('select[name="type"]');
    select.change(function () {
        var opt = $(this).find('option:selected').text();
        if(opt === 'добавить тип') {
            select
                .parent()
                .after("<input type='text' size='40' name='new_type' class='text new_type' placeholder='Добавьте новый тип'>");
        }
    });
}());