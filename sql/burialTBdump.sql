-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.20-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.3.0.4807
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных clubvet_db
CREATE DATABASE IF NOT EXISTS `clubvet_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `clubvet_db`;


-- Дамп структуры для таблица clubvet_db.burial_persons
DROP TABLE IF EXISTS `burial_persons`;
CREATE TABLE IF NOT EXISTS `burial_persons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL DEFAULT '0',
  `burial_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_burial_persons_mod_person` (`person_id`),
  KEY `FK_burial_persons_mod_burial` (`burial_id`),
  CONSTRAINT `FK_burial_persons_mod_burial` FOREIGN KEY (`burial_id`) REFERENCES `mod_burial` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_burial_persons_mod_person` FOREIGN KEY (`person_id`) REFERENCES `mod_person` (`id_person`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Таблица соспоставления ветеранов и объектов карты';

-- Дамп данных таблицы clubvet_db.burial_persons: ~1 rows (приблизительно)
DELETE FROM `burial_persons`;
/*!40000 ALTER TABLE `burial_persons` DISABLE KEYS */;
INSERT INTO `burial_persons` (`id`, `person_id`, `burial_id`) VALUES
	(1, 4, 1),
	(2, 27, 1),
	(3, 4, 3);
/*!40000 ALTER TABLE `burial_persons` ENABLE KEYS */;


-- Дамп структуры для таблица clubvet_db.mod_burial
DROP TABLE IF EXISTS `mod_burial`;
CREATE TABLE IF NOT EXISTS `mod_burial` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Имя',
  `comments` text COMMENT 'Доп информация',
  `type` enum('могила','воинское захоронение','памятник','добавить тип') DEFAULT NULL,
  `longitude` double DEFAULT '55.753994' COMMENT 'Долгота',
  `latitude` double DEFAULT '37.622093' COMMENT 'Широта',
  `act` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Активность',
  `author_id` int(11) DEFAULT NULL COMMENT 'Кто добавил',
  `marker` varchar(100) NOT NULL DEFAULT 'default',
  `location` text COMMENT 'Расположение объекта',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы clubvet_db.mod_burial: ~39 rows (приблизительно)
DELETE FROM `mod_burial`;
/*!40000 ALTER TABLE `mod_burial` DISABLE KEYS */;
INSERT INTO `mod_burial` (`id`, `name`, `comments`, `type`, `longitude`, `latitude`, `act`, `author_id`, `marker`, `location`) VALUES
	(1, '', '', 'могила', 37.6298363958415, 55.7444322034166, 1, 50, 'default', 'Примерное местонаходение могилы'),
	(3, 'Петр Великий', 'О́рдена Нахи́мова а́томный кре́йсер «Пётр Вели́кий» ) — четвёртый по счёту и единственный находящийся в строю[1] тяжёлый атомный ракетный крейсер (ТАРКР) третьего поколения проекта 1144 «Орлан». На 2011 год это самый большой в мире действующий неавианесущий ударный боевой корабль', 'памятник', 37.625174378306, 55.7519756684063, 1, 50, 'default', NULL),
	(4, 'Таисия', '5645546', 'памятник', 37.635174378316, 55.7519756684063, 1, NULL, 'default', NULL),
	(5, 'Таисия', '5645546', 'памятник', 37.645174378326, 55.7519756684063, 1, NULL, 'default', NULL),
	(6, 'Таисия', '5645546', 'памятник', 37.655174378336, 55.7519756684063, 1, NULL, 'default', NULL),
	(7, 'Таисия', '5645546', 'памятник', 37.665174378346, 55.7519756684063, 1, NULL, 'default', NULL),
	(8, 'Таисия', '5645546', 'памятник', 37.675174378356, 55.7519756684063, 1, NULL, 'default', NULL),
	(9, 'Таисия', '5645546', 'памятник', 37.685174378366, 55.7519756684063, 1, NULL, 'default', NULL),
	(10, 'Таисия', '5645546', 'памятник', 37.695174378376, 55.7519756684063, 1, NULL, 'default', NULL),
	(11, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(12, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(13, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(14, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(15, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(16, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(17, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(18, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(19, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(20, 'Таисия', 'О́рдена Нахи́мова а́томный кре́йсер «Пётр Вели́кий» ) — четвёртый по счёту и единственный находящийся в строю[1] тяжёлый атомный ракетный крейсер (ТАРКР) третьего поколения проекта 1144 «Орлан». На 2011 год это самый большой в мире действующий неавианесущий ударный боевой корабль', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(21, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(25, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, 50, 'default', NULL),
	(26, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(27, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(28, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(29, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(30, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(31, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(32, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(33, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(34, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(35, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(36, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(37, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(38, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(39, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(40, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(41, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(42, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL),
	(43, 'Таисия', '5645546', 'памятник', 37.725174378386, 55.7519756684063, 1, NULL, 'default', NULL);
/*!40000 ALTER TABLE `mod_burial` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
