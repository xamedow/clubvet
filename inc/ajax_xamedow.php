<?php
header('Content-type: text/html; charset=utf-8');
session_start();

require_once("../mod/mod_config.php");
$filter = new filter;

if (isset($_POST['table'])) {
    // Friends remover.
    if ($_POST['table'] === 'friends') {
        $person_id = $filter->html_filter(@$_POST['person_id']);
        $friend_id = $filter->html_filter(@$_POST['friend_id']);

        if (is_numeric($person_id) && is_numeric($friend_id)) {
            $Db->query = "SELECT id FROM person_friends WHERE (person_id = $person_id AND friend_id = $friend_id)
            OR (person_id = $friend_id AND friend_id = $person_id)";
            $Db->query();
            while ($q_res = mysql_fetch_assoc($Db->lQueryResult)) {
                $delete_id .= $q_res['id'] . ',';
            }
            $delete_id = substr($delete_id, 0, -1);

            $Db->query = "SELECT id FROM person_invitations WHERE status = 'accepted' AND init_by_id={$person_id} AND target_id={$friend_id}";
            $Db->query();
            while ($q_res = mysql_fetch_assoc($Db->lQueryResult)) {
                $delete_invite_id .= $q_res['id'] . ',';
            }
            $delete_invite_id = substr($delete_invite_id, 0, -1);

            $Db->query = "DELETE FROM person_friends WHERE id IN ($delete_id)";
            $Db->query();
            $Db->query = "DELETE FROM person_invitations WHERE id IN ($delete_invite_id)";
            $Db->query();
        }
    }
// Invitations handler.
    if ($_POST['table'] === 'invitations') {
        $id = $filter->html_filter(@$_POST['id']);
        $init_id = $filter->html_filter(@$_POST['init_id']);
        $person_id = $filter->html_filter(@$_POST['person_id']);
        $action = $filter->html_filter(@$_POST['action']);
        if (is_numeric($id)) {
            if ($action === 'accepted' || $action === 'rejected') {
                $Db->query = "UPDATE `person_invitations` SET status='$action' WHERE id = $id";
            }
        }
        if ($action === 'add') {
            $Db->query = "INSERT INTO person_invitations (target_id, init_by_id, status) VALUES($person_id, $init_id, 'open')";
        }
        $Db->query();

        if ($action === 'accepted') {
            $Db->query = "INSERT INTO person_friends (friend_id, person_id) VALUES($init_id, $person_id),($person_id, $init_id)";
            $Db->query();
        }
    }
    if ($_POST['table'] === 'blacklist') {
        $id = $filter->html_filter(@$_POST['id']);
        $banned_id = $filter->html_filter(@$_POST['banned_id']);
        $person_id = $filter->html_filter(@$_POST['person_id']);
        $action = $filter->html_filter(@$_POST['action']);

        if ($action === 'add') {
            $Db->query = "INSERT INTO person_blacklist (person_id, banned_id) VALUES($person_id, $banned_id)";
            $Db->query();
        }
        if ($action === 'delete') {
            $Db->query = "DELETE FROM person_blacklist WHERE id = $id";
            $Db->query();
        }
    }
// Banned remover.
    if ($_POST['table'] === 'blacklist') {
        $id = $filter->html_filter(@$_POST['id']);
        if (is_numeric($id)) {
            $Db->query = "DELETE FROM `person_blacklist` WHERE `id` = '" . $id . "' LIMIT 1";
            $Db->query();
        }
    }
    // Group remover.
    if ($_POST['table'] === 'group') {
        if ($_POST['action'] === 'quit' || $_POST['action'] === 'delete_from') {
            $person_id = $filter->html_filter(@$_POST['person_id']);
            $group_id = $filter->html_filter(@$_POST['group_id']);
            if (is_numeric($person_id) && is_numeric($group_id)) {
                $Db->query = "SELECT id FROM person_groups WHERE person_id=$person_id AND group_id=$group_id";
                $Db->query();
                $id = mysql_fetch_assoc($Db->lQueryResult);
                $Db->query = "DELETE FROM `person_groups` WHERE `id` = '" . $id['id'] . "' LIMIT 1";
                $Db->query();
            }
        }
        if ($_POST['action'] === 'delete') {
            $id = $filter->html_filter(@$_POST['id']);
            if (is_numeric($id)) {
                $Db->query = "DELETE FROM `mod_groups` WHERE `id` = '" . $id . "' LIMIT 1";
                $Db->query();
            }
        }
        if ($_POST['action'] === 'query') {
            $query = '';
            $Db->query = "SELECT name FROM mod_groups";
            $Db->query();
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $query .= $lRes['name'] . '/';
            }
            echo $query;
        }
    }
// Group invitations handler.
    if ($_POST['table'] === 'group_invitations') {
        $id = $filter->html_filter(@$_POST['id']);
        $init_id = $filter->html_filter(@$_POST['init_id']);
        $person_id = $filter->html_filter(@$_POST['person_id']);
        $action = $filter->html_filter(@$_POST['action']);
        if (is_numeric($id)) {
            $Db->query = "UPDATE `person_group_invitations` SET status='$action' WHERE id = $id";
            $Db->query();
        }
        if ($action === 'accepted') {
            $Db->query = "INSERT INTO person_groups (group_id, person_id) VALUES($init_id, $person_id)";
            $Db->query();
        }
    }
}

// Query handler.
if (isset($_GET['term'])) {
    $term = $filter->html_filter($_GET['term']);
    $id = $filter->html_filter($_GET['id']);
    $group_id = $filter->html_filter($_GET['group_id']);
    $users_list = array();

    $Db->query = "SELECT DISTINCT
                      id_person,
                      name
                    FROM mod_person
                         LEFT JOIN person_groups ON id_person = person_groups.person_id
                         LEFT JOIN person_friends ON id_person = person_friends.person_id
                    WHERE id_person NOT IN (SELECT person_groups.person_id FROM person_groups WHERE group_id = $group_id)
                    AND name LIKE '%$term%' AND person_friends.friend_id = $id";
    $Db->query();

    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $users_list[] = array('value' => $lRes['name'], 'label' => $lRes['name'], 'id' => $lRes['id_person']);
    }

    echo json_encode($users_list);
}


// Translations handler.

if ($_POST['table'] === 'translations') {
    echo json_encode($lang);
}

// Подстановка ссылок в текстах разделов "Боевой путь" и "Техника".
if ($_POST['table'] === 'tech') {
    $tech = array();
    $Db->query = "SELECT name_t, link_t FROM list_teh";
    $Db->query();

    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $tech[] = array(
            'name_t' => $lRes['name_t'],
            'link_t' => $lRes['link_t']
        );
    }
    echo json_encode($tech);
}
// Формирование ленты обновлений
if ($_POST['action'] === 'feed') {
    $id = $_POST['id'];
    $feed = array();

    $Db->query = "SELECT * FROM feed
                    LEFT JOIN person_friends ON feed.person_id = friend_id
                    LEFT JOIN mod_person ON feed.person_id = id_person
                    WHERE person_friends.person_id = $id OR feed.event = 'Ищет человека.' OR feed.event = 'Добавил просьбу о помощи.'";
    $Db->query();

    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $id = $lRes["id_person"];
        $img = (file_exists(DOCUMENT_ROOT . "/upload/gallery/" . $id . "/logo.jpg"))
            ? '<img src="/upload/gallery/' . $id . '/logo.jpg" alt="" />'
            : '<img src="/images/default_profile_pic.jpg" alt="" />';

        $feed[] = '<div class="reply">
                        <a class="user" href="/person/' . $id . '.html">
                            <div class="user_avatar">' . $img . '</div>
                            <div class="user_name">' . $lRes["name"] . '</div>
                        </a>
                        <div class="comment">
                            <div class="date">' . news_date($lRes["date"]) . '</div>
                            <p>' . $lRes["event"] . '</p>
                        </div>
                    </div>';
    }
    echo json_encode($feed);
}

// Карта
if ($_POST['action'] === 'deletePoint' && isset($_SESSION['id_person']) && $_SESSION['id_person'] === $_POST['author_id']) {
    $point_id = (int)$_POST['id'];
    $Db->query = "DELETE FROM mod_burial WHERE id = $point_id";
    $Db->query();
}
if ($_POST['action'] === 'get_points') {
    $out = array();
    $Db->query = "SELECT mod_burial.*, group_concat(distinct mod_person.id_person, '|', mod_person.name) as 'person_name'
FROM mod_burial
left join burial_persons on mod_burial.id = burial_persons.burial_id
left join mod_person on burial_persons.person_id = mod_person.id_person
where mod_burial.act='1'
group by mod_burial.id";
    $Db->query();
    while ($data = mysql_fetch_assoc($Db->lQueryResult)) {
        if (isset($_SESSION['id_person'])) {
            $data['current_user'] = ($_SESSION['id_person']);
        }
        $out[] = $data;
    }
    echo json_encode($out);
}
if ($_POST['action'] === 'getSelect') {
    echo "<li><label>{$_POST['label']}</label>" . selectFromEnum($_POST['table'], $_POST['field'], $Db) . '</li>';
}
if ($_POST['action'] === 'saveForm') {
    if (isset($_SESSION['id_person'])) {
        $post = clean_arr($_POST, $filter);
        parse_str($post['data'], $new_post);
        $keys = implode(',', array_keys($new_post));
        $values = implode(',', array_map('stringify', array_values($new_post)));
        $Db->query = "INSERT INTO mod_burial ($keys) VALUES ($values)";
        $Db->query();
    }
}
function stringify($el)
{
    $el = $el === '' ? 'null' : $el;
    return "'$el'";
}

function convert($el)
{
    return iconv('windows-1251', 'utf-8', $el);
}