<?php $PHP_SELF=$_SERVER['PHP_SELF']; if (!stripos($PHP_SELF,"index.php")) die ("Access denied"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="msvalidate.01" content="6C1C234AD4F049A0DEE96C7D1483F42A" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="title" content="<?php if (empty($title)) echo $config["main"]["main_title_".$set]; else echo $title;?>" />
	<meta name="keywords" content="<?php if (empty($keys)) echo $config["main"]["main_keys_".$set]; else echo $keys;?>" />
	<meta name="description" content="<?php if (empty($title)) echo $config["main"]["main_title_".$set]; else echo $title;?>" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon" /> 
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link href="/css/style.css" rel="stylesheet" type="text/css" />
	<link href="/css/xamedow.css" rel="stylesheet" type="text/css" />
	<link href="/css/yarygin.css" rel="stylesheet" type="text/css" />
	<link href="/css/fileuploader.css" rel="stylesheet" type="text/css" />
	<link href="/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="/fancybox/jquery.fancybox.css" />
    <?php if ($page_active == 'office' && $global_res["first"]==0) echo '
		<link href="/css/style_reg.css" rel="stylesheet" type="text/css" />';
	
	if ($page_active == 'office') echo '
    <link href="/css/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">';
	?>
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/themes/smoothness/jquery-ui.css" />
	<title><?php if (empty($title)) echo $config["main"]["main_title_".$set]; else echo $title;?></title>
</head>
<body>
<?php
if($autorized) {
//    echo "<a class=\"fresh_feed\" href=\"#\" data-id=\"$global_user\"></a>";
    echo "<a class=\"new_feed\" href=\"#\" data-id=\"$global_user\"></a>";
}
/*
if ($page_active == 'office' && $global_res["first"]==1 && $global_res["pay"]==0)
{
	echo '<div id="my_layer"></div><div class="layer_info">Ваш профиль недоступен. Вы можете оплатить активацию любым способом из списка ниже:
<br /><br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td valign="top">';

				// подключаем робокассу
			// (логин, пароль #1)
			$mrh_login = "clubveteranwar";
			$mrh_pass1 = "veteranclub64";

			$inv_id = $global_user; // номер человека нашего
			$inv_desc = $lang["95"]; // описание заказа
			$out_summ = $config["main"]["pay_reg"]; // сумма заказа
			$shp_item = 1; // тип товара
			$in_curr = ""; // предлагаемая валюта платежа
			$culture = "ru"; // язык
			$encoding = "utf-8"; // кодировка

			// формирование подписи
			$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");

			// HTML-страница с кассой // https://merchant.roboxchange.com/Index.aspx
			echo
					  "<img src='/images/robokassa.png' alt='' style='margin-top: 5px;
margin-left: 15px;' /><br />
<form action='https://merchant.roboxchange.com/Index.aspx' method=POST>".
					  "<input type=hidden name=MrchLogin value=$mrh_login>".
					  "<input type=hidden name=OutSum value=$out_summ>".
					  "<input type=hidden name=InvId value=$inv_id>".
					  "<input type=hidden name=Desc value='$inv_desc'>".
					  "<input type=hidden name=SignatureValue value=$crc>".
					  "<input type=hidden name=Shp_item value='$shp_item'>".
					  "<input type=hidden name=IncCurrLabel value=$in_curr>".
					  "<input type=hidden name=Culture value=$culture>".
					  "<input type=submit value='".$lang["97"]."' class='button_enter' style='margin: 0 auto; float: none; margin-top: 20px;' />".
					  "</form>";

				echo '</td>
					  </tr>
					</table></div>';
}
*/
echo print_banners($banners, 1, $pageactive);?>
<div class="banners left" id="banleft">
    <?=print_banners($banners, 4, $pageactive);?>
</div>
<div class="banners right">
    <?=print_banners($banners, 5, $pageactive);?>
</div>
<div id="header_bg"></div>
<div id="menu_under"></div>
<div id="wrapper_before_scroll">
	<div id="header">
		<a href="/<?php if($autorized == 1) echo 'office/index/'; ?>" id="logo">
			<div id="logo_pic"></div>
			<div id="logo_title"><?=$config["main"]["main_name_".$set]?></div>
			<div id="logo_title_small">www.clubveteranwar.com</div>
		</a>
		<div id="searchbar">
			<form name="search" action="/search/" method="post">
				<input name="search_field" class="box" type="text" value="<?=$lang["3"];?>" name="query" onfocus="if (this.value == '<?=$lang["3"];?>') {this.value = '';}" />
				<input name="submit" type="submit" class="submit" value="" />
			</form>
		</div>
		<div class="flags_up">
			<a href="#" onclick="document.cookie = 'lang=ger; path=/; expires=Wed, 1 Mar 20012 00:00:00'; location.reload(); return false;"><img src="/images/up_flag3.jpg" alt="German" /></a>
			<a href="#" onclick="document.cookie = 'lang=eng; path=/; expires=Wed, 1 Mar 20012 00:00:00'; location.reload(); return false;"><img src="/images/up_flag2.jpg" alt="English" /></a>
			<a href="#" onclick="document.cookie = 'lang=rus; path=/; expires=Wed, 1 Mar 20012 00:00:00'; location.reload(); return false;"><img src="/images/up_flag1.jpg" alt="Русский" /></a>
		</div>

		<h3><?=$lang["1"];?></h3>
	</div><!--#header-->

    <?

	$Db->query = "SELECT `id_content` FROM `mod_content` WHERE `redirect`='".$set."'";
	$Db->query();
	if (mysql_num_rows($Db->lQueryResult)>0)
	{
		$lRes = mysql_fetch_assoc($Db->lQueryResult);
		$id = $lRes["id_content"];

			$Db->query = "SELECT * FROM `mod_content` WHERE `act`='1' AND `in_menu`='1' ORDER BY `parent`,`rank`";
            $Db->query();
                        $row_num = mysql_num_rows($Db->lQueryResult);
                        $num = 1;
                        if($row_num > 0)
                        {
                            echo '<div id="menu_bg">';
                            while($lRes = mysql_fetch_assoc($Db->lQueryResult)) $data1[$lRes['parent']][] = $lRes;

                            $data1 = getTreeContent($data1, $id);
                            echo sub_pages($data1, 0, $autorized);
                                    echo '
                                </div><!--#menu_bg-->';
                        }
	}
			echo print_banners($banners, 2, $pageactive);
	if ($page_active=="office" && $autorized == 1 && $global_res["first"]!=0)
	{
		// New message count
		$Db->query = "SELECT COUNT(*) as newmsg FROM `messages` WHERE `in`=".$global_user." AND `read`=0";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $new_msg_count = (empty($lRes['newmsg'])) ? '' : " <div class='counter'>{$lRes['newmsg']}</div>";

		// Invites count.
        $Db->query = "SELECT COUNT(*) as invites FROM person_invitations WHERE target_id=$global_user AND status='open'";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $invites_count = (empty($lRes['invites'])) ? '' : " <div class='counter'>{$lRes['invites']}</div>";
        // Group invites count.
        $Db->query = "SELECT COUNT(*) as invites FROM person_group_invitations WHERE person_id=$global_user AND status='open'";
        $Db->query();
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
        $group_invites_count = (empty($lRes['invites'])) ? '' : " <div class='counter'>{$lRes['invites']}</div>";
		$img_name_full = $_SERVER['DOCUMENT_ROOT']."/upload/gallery/".$global_user."/logo.jpg";

		if (!file_exists($img_name_full))  $logo = '<a class="edit_profile_image" href="/office/photo1/"></a><img src="/images/default_profile_pic.jpg" alt="" />';  else $logo = '<img src="/upload/gallery/'.$global_user.'/logo.jpg" alt="Logo"><a class="del_profile_image" href="#" data-id="'.$global_user.'"></a>';
		$array_nav_link = array(
			"index"=>'<a href="/office/"><img src="/images/icon_profile.png">Моя страница</a>',
			"message"=>'<a href="/office/message/"><img src="/images/icon_mail.png">Сообщения'.$new_msg_count.'</a>',
			"friends"=>'<a href="/office/friends/"><img src="/images/icon_heart.png">Список друзей'.$invites_count.'</a>',
			"groups"=>'<a href="/office/groups/"><img src="/images/icon_wheel.png">Сообщества'.$group_invites_count.'</a>',
			"photo"=>'<a href="/office/photo/"><img src="/images/icon_photo.png">Фотоальбомы</a>',
			"media"=>'<a href="/office/media/"><img src="/images/icon_media.png">Медиа-файлы</a>',
			"docs"=>'<a href="/office/docs/"><img src="/images/icon_doc.png">Документы</a>',
			"family"=>'<a href="/office/family/"><img src="/images/icon_fam.png">Семья</a>',
			"creation"=>'<a href="/office/creation/"><img src="/images/icon_art.png">Творчество</a>',
			"config"=>'<a href="/office/config/"><img src="/images/icon_settings.png">Настройки</a>',
		);
		$array_nav_span = array(
			"index"=>'<span><img src="/images/icon_profile.png">Моя страница</span>',
			"message"=>'<span><img src="/images/icon_mail.png">Сообщения'.$new_msg_count.'</span>',
			"friends"=>'<span><img src="/images/icon_heart.png">Список друзей'.$invites_count.'</span>',
			"groups"=>'<span><img src="/images/icon_wheel.png">Сообщества'.$group_invites_count.'</span>',
			"photo"=>'<span><img src="/images/icon_photo.png">Фотоальбомы</span>',
			"media"=>'<span><img src="/images/icon_media.png">Медиа-файлы</span>',
			"docs"=>'<span><img src="/images/icon_doc.png">Документы</span>',
			"family"=>'<span><img src="/images/icon_fam.png">Семья</span>',
			"creation"=>'<span><img src="/images/icon_art.png">Творчество</span>',
			"config"=>'<span><img src="/images/icon_settings.png">Настройки</span>',
		);

		echo '
			<div id="middle" style="min-height: 630px;">
                <div id="left_navigation">
				<p class="person_email">'.$global_res["mail"].'</p>
				<div class="profile_image">'.$logo.'</div>
				<div class="underline_border"></div>
				<ul id="profile_menu_vert">';
			foreach ($array_nav_link as $key=>$value)
			{
				if ($page_name==$key) echo '<li>'.$array_nav_span[$key].'</li>'; else echo '<li>'.$value.'</li>';
			}

				echo '<li><a href="/office/off/"><img src="/images/icon_exit.png">Выход</a></li>
				</ul>
			</div>';
	}  else if ($page_active!="person") echo '<div id="middle">'; else echo '<div id="middle" style="padding-bottom: 50px; min-height: 600px;">';
	?>
