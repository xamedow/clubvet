﻿<?php 
require_once("../mod/mod_config.php");

function forech_pages_select($data, $parent, $sub, $tire)
	{
		foreach($data as $k=>$v)
		{
			if (is_array($parent))
			{
				if(in_array($v['id_cat'],$parent)) 
					$chek = " selected='selected'"; 
				else 
					$chek = "";
			}
			else
			{
				if($v['id_cat']==$parent) 
					$chek = " selected='selected'"; 
				else 
					$chek = "";
			}
			$sub.='<option value="'.$v["id_cat"].'"'.$chek.'>'.$tire.$v['name_cat'].'</option>';
			
			if(isset($v['childs'])) 
				$sub .= forech_pages_select($v['childs'],$parent,"", $tire."----");
		}
		return $sub;
	}

$filter = new filter;
if (isset($_POST['page']) && $_POST['page']=='delimg'){
	$id = $filter->html_filter(@$_POST['comid']);
	if (is_numeric($id) && $id==$global_user)
	{
		unlink($_SERVER['DOCUMENT_ROOT']."/upload/gallery/".$id."/logo.jpg");
		unlink($_SERVER['DOCUMENT_ROOT']."/upload/gallery/".$id."/bg_logo.jpg");
		$Db->query="UPDATE `mod_person` SET `photo`='0' WHERE `id_person` = '".$global_user."' LIMIT 1"; 
		$Db->query();
	}
}

if (isset($_POST['page']) && $_POST['page']=='delete_comment'){
	$id = $filter->html_filter(@$_POST['id']);
	if (is_numeric($id)) 
	{
		$Db->query="DELETE FROM `mod_comment` WHERE `id_comment` = '".$id."' AND `page_id`='".$global_user."' LIMIT 1"; 
		$Db->query();
	}
}

if (isset($_POST['page']) && $_POST['page']=='del_list_war'){
	$id = $filter->html_filter(@$_POST['comid']);
	if (is_numeric($id)) 
	{
		$Db->query="DELETE FROM `list_war_info` WHERE `id_war` = '".$id."' AND `rel_person`='".$global_user."' LIMIT 1"; 
		$Db->query();
	}
}
if (isset($_POST['page']) && $_POST['page']=='del_event_war'){
	$id = $filter->html_filter(@$_POST['comid']);
	if (is_numeric($id)) 
	{
		$Db->query="DELETE FROM `mod_war_rel` WHERE `id_rel` = '".$id."' AND `rel_person`='".$global_user."' LIMIT 1"; 
		$Db->query();
	}
}

if (isset($_POST['page']) && $_POST['page']=='del_list_teh'){
	$id = $filter->html_filter(@$_POST['comid']);
	if (is_numeric($id)) 
	{
		$Db->query="DELETE FROM `list_teh_info` WHERE `id_teh` = '".$id."' AND `rel_person`='".$global_user."' LIMIT 1"; 
		$Db->query();
	}
}
if (isset($_POST['page']) && $_POST['page']=='del_list_wound'){
	$id = $filter->html_filter(@$_POST['comid']);
	if (is_numeric($id)) 
	{
		$Db->query="DELETE FROM `list_wound_info` WHERE `id_wound` = '".$id."' AND `rel_person`='".$global_user."' LIMIT 1"; 
		$Db->query();
	}
}
if (isset($_POST['page']) && $_POST['page']=='del_award'){
	$id = $filter->html_filter(@$_POST['comid']);
	if (is_numeric($id)) 
	{
		$Db->query="DELETE FROM `mod_award_rel` WHERE `id_rel` = '".$id."' AND `rel_person`='".$global_user."' LIMIT 1"; 
		$Db->query();
	}
}
if (isset($_POST['page']) && $_POST['page']=='send_family_tree'){
	$value = explode("&",$_POST['value']);

	foreach($value as $perf) {
		$perf_key_values = explode("=", $perf);
		$key = urldecode($perf_key_values[0]);
		$values = urldecode($perf_key_values[1]);
		if ($key=="name") $name = $filter->html_filter(stripslashes($values));
		if ($key=="phone") $phone = $filter->html_filter(stripslashes($values));
		if ($key=="email") $email = $filter->html_filter(stripslashes($values));
	}
	
	$Db->query="INSERT INTO `mod_family_tree` (`contact`,`date`,`rel_person`,`lang`) VALUES('<p>".$name."<br />".$phone."<br />".$email."</p>', NOW(),'".$global_user."','".$set."')";
	$Db->query();
	
			$subject = 'Заявка с сайта на родословную';
            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="30%"><p align="center"><img src="http://clubveteranowar.com/images/logo.png" /></p></td>
						  <td><p>'.$config["main"]["main_name_".$set].'<br />
						  '.$config["main"]["main_email"].'</p></td>
							</tr>
						  </table>
							Здравствуйте! Поступила заявка на родословную.
							<p>'.$name.'<br />'.$phone.'<br />'.$email.'</p>
							Данная информация занесена в базу данных.
							<br />
							<br /><br />С уважением, администрация '.$DomenName;
				
						$headers= "MIME-Version: 1.0\r\n";
						$headers.= "Content-type: text/html; charset=utf-8\r\n";
						$headers.= $config["main"]["main_name_".$set]." <noreplay@noreplay.ru>";				
							
    		mail($config["main"]["main_email"], $subject, $message, $headers);
	
}
if (isset($_POST['page']) && $_POST['page']=='send_my_info'){
	$value = explode("&",$_POST['value']);

	foreach($value as $perf) {
		$perf_key_values = explode("=", $perf);
		$key = urldecode($perf_key_values[0]);
		$values = urldecode($perf_key_values[1]);
		if ($key=="male") $male = $filter->html_filter(stripslashes($values));
		if ($key=="date_birth") $date_birth = $filter->html_filter(stripslashes($values));
		if ($key=="place_birth") $place_birth = $filter->html_filter(stripslashes($values));
		if ($key=="location") $location = $filter->html_filter(stripslashes($values));
		if ($key=="front") $front = $filter->html_filter(stripslashes($values));
		if ($key=="country") $country = $filter->html_filter(stripslashes($values));
		if ($key=="arm") $arm = $filter->html_filter(stripslashes($values));
		if ($key=="pod") $pod = $filter->html_filter(stripslashes($values));
		if ($key=="date_war") $date_war = $filter->html_filter(stripslashes($values));
		if ($key=="war_text") $war_text = $filter->html_filter(stripslashes($values));
		
		if ($key=="country_form") $country_form = $filter->html_filter(stripslashes($values));
		if ($key=="rod_voisk") $rod_voisk = $filter->html_filter(stripslashes($values));
		if ($key=="voin_form") $voin_form = $filter->html_filter(stripslashes($values));
	}
	
	if (strlen($date_birth)>4) {
	$date = explode("/", $date_birth);
	$date_birth = $date[2]."-".$date[0]."-".$date[1];
	}
	else $date_birth = $date_birth."-00-00";
	
	if (!is_numeric($country) or !is_numeric($arm) or !is_numeric($pod)) // если нет такой страны - добавляем страны / род войск / воинское формирование
	{
		if (!is_numeric($country))
		{
			$Db->query="INSERT INTO `list_country` (`name_country_rus`,`name_country_eng`,`name_country_ger`) VALUES('".$country_form."', '".$country_form."','".$country_form."')";
			$Db->query();
			$country = mysql_insert_id();
		}
		if (!is_numeric($arm))
		{
			$Db->query="INSERT INTO `list_country_arm` (`name_arm`,`rel_country`) VALUES('".$rod_voisk."', '".$country."')";
			$Db->query();
			$arm = mysql_insert_id();
		}
		if (!is_numeric($pod))
		{
			$Db->query="INSERT INTO `list_country_arm_pod` (`name_pod`,`rel_arm`) VALUES('".$voin_form."', '".$arm."')";
			$Db->query();
			$pod = mysql_insert_id();
		}
	}
	
	
	$Db->query="UPDATE `mod_person` SET `sex`='".$male."', `place_birth`='".$place_birth."', `location`='".$location."', `date_birth`='".$date_birth."', `front`='".$front."',`rel_country`='".$country."',`rel_arm`='".$arm."',`rel_pod`='".$pod."',`date_war`='".$date_war."'  WHERE `id_person`='".$global_user."' LIMIT 1";
	$Db->query();
	if (!empty($war_text))
	{
		$Db->query="INSERT INTO `list_war_info` (`text_war`,`date_add`,`rel_person`,`lang`) VALUES('".nl2br($war_text)."', NOW(),'".$global_user."','".$set."')";
		$Db->query();
	}
	
}

if (isset($_POST['page']) && $_POST['page']=='save_war_info'){
	$value = explode("&",$_POST['value']);

	foreach($value as $perf) {
		$perf_key_values = explode("=", $perf);
		$key = urldecode($perf_key_values[0]);
		$values = urldecode($perf_key_values[1]);
		if ($key=="country") $country = $filter->html_filter(stripslashes($values));
		if ($key=="arm") $arm = $filter->html_filter(stripslashes($values));
		if ($key=="pod") $pod = $filter->html_filter(stripslashes($values));
		
		if ($key=="country_form") $country_form = $filter->html_filter(stripslashes($values));
		if ($key=="rod_voisk") $rod_voisk = $filter->html_filter(stripslashes($values));
		if ($key=="voin_form") $voin_form = $filter->html_filter(stripslashes($values));
	}
	
	if (!is_numeric($country) or !is_numeric($arm) or !is_numeric($pod)) // если нет такой страны - добавляем страны / род войск / воинское формирование
	{
		if (!is_numeric($country))
		{
			$Db->query="INSERT INTO `list_country` (`name_country_rus`,`name_country_eng`,`name_country_ger`) VALUES('".$country_form."', '".$country_form."','".$country_form."')";
			$Db->query();
			$country = mysql_insert_id();
		}
		if (!is_numeric($arm))
		{
			$Db->query="INSERT INTO `list_country_arm` (`name_arm`,`rel_country`) VALUES('".$rod_voisk."', '".$country."')";
			$Db->query();
			$arm = mysql_insert_id();
		}
		if (!is_numeric($pod))
		{
			$Db->query="INSERT INTO `list_country_arm_pod` (`name_pod`,`rel_arm`) VALUES('".$voin_form."', '".$arm."')";
			$Db->query();
			$pod = mysql_insert_id();
		}
	}
	
	
	$Db->query="UPDATE `mod_person` SET `rel_country`='".$country."',`rel_arm`='".$arm."',`rel_pod`='".$pod."'  WHERE `id_person`='".$global_user."' LIMIT 1";
	$Db->query();
}


if (isset($_POST['page']) && $_POST['page']=='send_i_search'){
	$fpost = clean_arr($_POST, $filter);
    extract($fpost);

    $Db->query="INSERT INTO `mod_i_search` (`text_about`,`text_search`,`contact_fio`,`contact_email`,`date`,`lang`)
	 VALUES('$i_search','$i_search_text','$name','$email', NOW(),'$set')";
	$Db->query();

    $id = mysql_insert_id();

    $myDir = $_SERVER['DOCUMENT_ROOT']."/upload/isearch/".$id.'/';
    if (! file_exists($myDir)) mkdir($myDir);

    if (!empty($_FILES["file"]["name"]))
    {
        $radio_cant_download_images=0;
        $i = 0;
        $msg = "";
        $msg_full = "";
        $files_count = sizeof($_FILES["file"]["name"]);
        for ($i = 0; $i < $files_count; $i++) {
            if($_FILES["image"]["size"] <= 2097152)
            {
                if ($i == 0)
                {
                    $myname = rand();
                    $myname_full = $_SERVER['DOCUMENT_ROOT']."/upload/isearch/cover".$myname.".jpg";
                    create_thumbnail($_FILES["file"]['tmp_name'][$i], $myname_full, $thumb_width=160, $thumb_height=160, $do_cut=true);
                    $Db->query="UPDATE `mod_i_search` SET `img`='".$myname."' WHERE `id_search` = '".$id."' LIMIT 1";
                    $Db->query();
                }

                $myname = rand();
                $myname_full = $myDir."sm".$myname.".jpg";
                $myname_full_large = $myDir."bg".$myname.".jpg";
                create_thumbnail($_FILES["file"]['tmp_name'][$i], $myname_full, $thumb_width=160, $thumb_height=160, $do_cut=true);
                create_thumbnail($_FILES["file"]['tmp_name'][$i], $myname_full_large, $thumb_width=300, $thumb_height=300, $do_cut=false);
                $msg.= $myname."|";
                @unlink($_FILES["file"][$i]);
            }
            else {$radio_cant_download_images=1;}
        }
    }
    if (!empty($msg)) {
        $file = explode("|", substr($msg,0,-1));
        $input = "";
        foreach ($file as $key=>$value) $input.= "('".$value."','".$id."'),";
        $input = substr($input,0,-1);
        $Db->query="INSERT INTO `mod_i_search_file` (`source`,`rel_id`) VALUES ".$input;
        $Db->query();
    }

    to_feed($Db, 'Ищет человека.', $global_user);
	
			$subject = 'Заявка на поиск';
            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="30%"><p align="center"><img src="http://clubveteranowar.com/images/logo.png" /></p></td>
						  <td><p>'.$config["main"]["main_name_".$set].'<br />
						  '.$config["main"]["main_email"].'</p></td>
							</tr>
						  </table>
							Здравствуйте! Поступила заявка на поиск на сайте.<br />
							<p>Чтобы она появилась на сайте, необходимо активировать запись в системе управления сайтом.</p>
							<br />
							<br /><br />С уважением, администрация '.$DomenName;
				
						$headers= "MIME-Version: 1.0\r\n";
						$headers.= "Content-type: text/html; charset=utf-8\r\n";
						$headers.= $config["main"]["main_name_".$set]." <noreplay@noreplay.ru>";				
							
    		mail($config["main"]["main_email"], $subject, $message, $headers);
	
}

if (isset($_POST['page']) && $_POST['page']=='send_comment'){
	$value = explode("&",$_POST['value']);
	$mod = $filter->html_filter($_POST['mod']);
	$id = $filter->html_filter($_POST['id']);
	
	if ($mod=='main') $act=0; else $act=1;

	foreach($value as $perf) {
		$perf_key_values = explode("=", $perf);
		$key = urldecode($perf_key_values[0]);
		$values = urldecode($perf_key_values[1]);
		if ($key=="name") $name = $filter->html_filter(stripslashes($values));
		if ($key=="email") if (is_email($values)) $email = $values;
		if ($key=="comment") $comment = $filter->html_filter($values);
		if ($key=="present") $present = $filter->html_filter($values);
	}
	
	$Db->query="INSERT INTO `mod_comment` 
	(`date`, `fio_comment`, `email_comment`, `text_comment`, `mod`, `page_id`, `rel_person`,`lang`,`act`,`present`) VALUES 
	( NOW(), '".$name."', '".$email."', '".nl2br($comment)."', '".$mod."', '".$id."', '".$global_user."','".$set."','".$act."','".$present."')";
	$Db->query();
	
	if ($mod=="main")
	{
			$subject = 'На сайте новый комментарий';
            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="30%"><p align="center"><img src="http://clubveteranowar.com/images/logo.png" /></p></td>
						  <td><p>'.$config["main"]["main_name_".$set].'<br />
						  '.$config["main"]["main_email"].'</p></td>
							</tr>
						  </table>
							Здравствуйте! На сайте оставлен новый комментарий.
							<br />
							<br /><br />С уважением, администрация '.$DomenName;
				
						$headers= "MIME-Version: 1.0\r\n";
						$headers.= "Content-type: text/html; charset=utf-8\r\n";
						$headers.= $config["main"]["main_name_".$set]." <noreplay@noreplay.ru>";				
							
    		mail($config["main"]["main_email"], $subject, $message, $headers);
	}
	
}

if (isset($_POST['page']) && $_POST['page']=='send_comment_answer'){
	$value = explode("&",$_POST['value']);
	$id = $filter->html_filter($_POST['id']);

	foreach($value as $perf) {
		$perf_key_values = explode("=", $perf);
		$key = urldecode($perf_key_values[0]);
		$values = urldecode($perf_key_values[1]);
		if ($key=="comment") $comment = $filter->html_filter($values);
	}
	
	$Db->query="INSERT INTO `mod_comment_answer` 
	(`date_answer`, `text_answer`, `rel_comment`, `rel_person`) VALUES 
	( NOW(), '".nl2br($comment)."', '".$id."', '".$global_user."')";
	$Db->query();

}

if (isset($_POST['page']) && $_POST['page']=='edit_info'){
	
	$value = $filter->html_filter(@$_POST['value']);
	$param = $filter->html_filter(@$_POST['param']);
	
	if ($param=='date_birth' or $param=='date_death')
	{
		$date = explode(".",$value);
		if (($date[1]!='')&&($date[2]!=''))	$value = $date[2]."-".$date[1]."-".$date[0];
		else $value = $value."-00-00";
	}
	
	$Db->query="UPDATE `mod_person` SET `".$param."`='".$value."' WHERE `id_person` = '".$global_user."' LIMIT 1"; 
	if ($value!="не указано") $Db->query();
}
if (isset($_POST['page']) && $_POST['page']=='save_war'){
    $value = $filter->html_filter(@$_POST['value']);
    $value = links_subst($value, $Db);
	$Db->query="INSERT INTO `list_war_info` (`text_war`,`date_add`,`rel_person`,`lang`) VALUES('".nl2br($value)."', NOW(),'".$global_user."','".$set."')";
	$Db->query();
    to_feed($Db, 'Обновил информацию о Боевом пути.', $global_user);
}
if (isset($_POST['page']) && $_POST['page']=='save_teh'){
	$value = $filter->html_filter(@$_POST['value']);
    $value = links_subst($value, $Db);
	$Db->query="INSERT INTO `list_teh_info` (`text_teh`,`date_add`,`rel_person`,`lang`) VALUES('".nl2br($value)."', NOW(),'".$global_user."','".$set."')";
	$Db->query();
    to_feed($Db, 'Обновил информацию о Технике.', $global_user);
}
if (isset($_POST['page']) && $_POST['page']=='save_wound'){
	$value = $filter->html_filter(@$_POST['value']);
    $value = links_subst($value, $Db);
	$Db->query="INSERT INTO `list_wound_info` (`text_wound`,`date_add`,`rel_person`,`lang`) VALUES('".nl2br($value)."', NOW(),'".$global_user."','".$set."')";
	$Db->query();
    to_feed($Db, 'Обновил информацию о Ранениях.', $global_user);
}
if (isset($_POST['page']) && $_POST['page']=='save_event_war'){
	$value = $filter->html_filter(@$_POST['value']);
	
	$Db->query="INSERT INTO `mod_war_rel` (`rel_person`,`rel_event`) VALUES('".$global_user."','".$value."')";
	$Db->query();
    to_feed($Db, 'Обновил информацию о Сражениях.', $global_user);
}

if (isset($_POST['page']) && $_POST['page']=='save_award'){
	$value = $filter->html_filter(@$_POST['value']);
	$text = $filter->html_filter(@$_POST['text']);
	
	$Db->query="INSERT INTO `mod_award_rel` (`rel_person`,`rel_award`,`text_about`) VALUES('".$global_user."','".$value."', '".nl2br($text)."')";
	$Db->query();

    to_feed($Db, 'Обновил информацию о Наградах.', $global_user);
}

if (isset($_POST['page']) && $_POST['page']=='select_cat_award'){
	
				echo '<option value="0">Выберите категорию</option>';
				$Db->query = "SELECT `id_cat`,`name_cat`,`parent` FROM `mod_award_cat` ORDER BY `parent`,`name_cat`";
				$Db->query();
				while($lRes=mysql_fetch_assoc($Db->lQueryResult)) 
					$data[$lRes['parent']][] = $lRes;
					
				$data = getTree($data, 0);
				echo forech_pages_select($data, 0, "", "--");

}
if (isset($_POST['page']) && $_POST['page']=='select_war'){
	
	// массив уже добавленных сражений
	$Db->query="SELECT `rel_event` FROM `mod_war_rel` WHERE `rel_person`='".$global_user."'";
	$Db->query();
	if (mysql_num_rows($Db->lQueryResult)>0) while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) $array_my[] = $lRes["rel_event"]; else $array_my = array();
	
	
	$war = $filter->html_filter($_POST["war"]); 
	$name = $filter->html_filter($_POST["name"]); 
	if (is_numeric($war))
	{
		$content = '<option value="0">Список сражений</option>'; 
				$Db->query="SELECT * FROM `mod_war_event` WHERE `rel_war`='".$war."' AND `name_event_".$set."`!='' ORDER BY `name_event_".$set."`";
				$Db->query();
				if (mysql_num_rows($Db->lQueryResult)>0) {
					while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) if (!in_array($lRes["id_event"], $array_my)) $content.='<option value="'.$lRes["id_event"].'" data-name="'.$lRes["name_event_".$set].'" data-id="'.$name.'">'.$lRes["name_event_".$set].'</option>';	
				}
				else { $content = '<option>Сражений не найдено.</option>'; }
				echo $content;
	}			
}

if (isset($_POST['page']) && $_POST['page']=='select_country'){
	
	// массив уже добавленных наград
	$Db->query="SELECT `rel_award` FROM `mod_award_rel` WHERE `rel_person`='".$global_user."'";
	$Db->query();
	if (mysql_num_rows($Db->lQueryResult)>0) while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) $array_my[] = $lRes["rel_award"]; else $array_my = array();
	
	
	$country = $filter->html_filter($_POST["country"]); 
	$name = $filter->html_filter($_POST["name"]); 
	if (is_numeric($country))
	{
		$content = '<option value="0">Список наград</option>'; 
				$Db->query="SELECT * FROM `mod_award` WHERE `rel_country`='".$country."' ORDER BY `name_award`";
				$Db->query();
				if (mysql_num_rows($Db->lQueryResult)>0) {
					while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) if (!in_array($lRes["id_award"], $array_my)) $content.='<option value="'.$lRes["id_award"].'" data-name="'.$lRes["name_award"].'" data-id="'.$name.'" data-rel="'.$lRes["img_award"].'">'.$lRes["name_award"].'</option>';	
				}
				else { $content = '<option>Наград не найдено</option>'; }
				echo $content;
	}			
}

if (isset($_POST['page']) && $_POST['page']=='select_cat_award2'){
	
	// массив уже добавленных наград
	$Db->query="SELECT `rel_award` FROM `mod_award_rel` WHERE `rel_person`='".$global_user."'";
	$Db->query();
	if (mysql_num_rows($Db->lQueryResult)>0) while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) $array_my[] = $lRes["rel_award"]; else $array_my = array();

	$cat_award = $filter->html_filter($_POST["cat_award"]); 
	
	if (is_numeric($cat_award))
	{
		$content = '<option value="0">Список наград</option>'; 
				$Db->query="SELECT * FROM `mod_award` WHERE `rus_cat`='".$cat_award."' ORDER BY `name_award`";
				$Db->query();
				if (mysql_num_rows($Db->lQueryResult)>0) {
					while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) if (!in_array($lRes["id_award"], $array_my)) $content.='<option value="'.$lRes["id_award"].'" data-name="'.$lRes["name_award"].'" data-id="'.$name.'" data-rel="'.$lRes["img_award"].'">'.$lRes["name_award"].'</option>';	
				}
				else { $content = '<option>Наград не найдено</option>'; }
				echo $content;
	}			
}

if (isset($_POST['page']) && $_POST['page']=='select_country2'){
	
	$country = $filter->html_filter($_POST["country"]); 
	$name = $filter->html_filter($_POST["name"]); 
	
	if (is_numeric($country))
	{
		$content.= '<option value="0">Род войск</option>'; 
				$Db->query="SELECT * FROM `list_country_arm` WHERE `rel_country`='".$country."' ORDER BY `name_arm`";
				$Db->query();
				if (mysql_num_rows($Db->lQueryResult)>0) {
					while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) $content.='<option value="'.$lRes["id_arm"].'" data-name="'.$lRes["name_arm"].'" data-id="'.$name.'">'.$lRes["name_arm"].'</option>';	
					$content.= '<option value="999">'.$lang["34"].'</option>';
				}
				else { $content = '0'; }
				echo $content;
	}			
}


if (isset($_POST['page']) && $_POST['page']=='select_country_list'){

				$Db->query="SELECT * FROM `list_country` ORDER BY `name_country_".$set."`";
				$Db->query();
				if (mysql_num_rows($Db->lQueryResult)>0) {
					while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) $content.='<option value="'.$lRes["id_country"].'">'.$lRes["name_country_".$set].'</option>';	
				}
				echo $content;			
}

if (isset($_POST['page']) && $_POST['page']=='select_arm'){
	
	$arm = $filter->html_filter($_POST["arm"]); 
	$name = $filter->html_filter($_POST["name"]); 
	if (is_numeric($arm))
	{
		$content = '<option value="0">Выберите подразделение</option>'; 
				$Db->query="SELECT * FROM `list_country_arm_pod` WHERE `rel_arm`='".$arm."' ORDER BY `name_pod`";
				$Db->query();
				if (mysql_num_rows($Db->lQueryResult)>0) {
					while ($lRes=mysql_fetch_assoc($Db->lQueryResult)) $content.='<option value="'.$lRes["id_pod"].'" data-name="'.$lRes["name_pod"].'" data-id="'.$name.'">'.$lRes["name_pod"].'</option>';	
					$content.= '<option value="999">'.$lang["34"].'</option>';
				}
				else { $content = '<option>Список пуст</option>'; }
				echo $content;
	}			
}



?>