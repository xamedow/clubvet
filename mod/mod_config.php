<?
//$PHP_SELF=$_SERVER['PHP_SELF']; if (!stripos($PHP_SELF,"index.php")) die ("Access denied"); 
session_start();
// подключаем основные функции и классы
require_once("mod_func.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/admin/db.php");

$Db = new Db ($DBServer, $DBLogin, $DBPassword, $DBName);
$Db->connect();

// глобальные настройки для модулей
$Db->query = "SELECT `mod`,`option`,`value` FROM `mod_config`";
$Db->query();
while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
    $config[$lRes[mod]][$lRes[option]] = $lRes[value];
}

// проверям есть ли ограничения доступа к сайту 
if (!empty($config["main"]["allow_ip"])) {
    $array_ip = explode(",", $config["main"]["allow_ip"]);
    $you_ip = RealIP();
    if (!in_array($you_ip, $array_ip)) {
        include "close.html";
        die();
    }
}

// язык и куки
if (!isset($_COOKIE['lang'])) {
    if (@$global_res["lang"]) {
        $set = $global_res["lang"];
    } else {
        $set = CURRENT_LANG;
    }
    setcookie('lang', $set, time() + 3600 * 24 * 30, "/"); //устанавливаем куки на месяц
} else {
    $filter = new filter;
    $set = $filter->html_filter(@$_COOKIE['lang']);
}

// глобальная авторизация


if ((empty($_SESSION['id_person']) or empty($_SESSION['hash'])) and ($_COOKIE['hash']=='')) {
    $autorized = 0;
    unset($_SESSION['id_person']);
    unset($_SESSION['hash']);
} 
elseif ((empty($_SESSION['id_person']) or empty($_SESSION['hash'])) and ($_COOKIE['hash']!='') and !empty($_COOKIE['myid']))
{
	$global_user=$_COOKIE['myid'];
	$Db->query = "SELECT * FROM mod_person WHERE id_person='".$global_user."' LIMIT 1";
    $Db->query();
	$lRes=mysql_fetch_assoc($Db->lQueryResult);
	if ($_COOKIE['hash']==$lRes[hash_cookie]){
		$autorized = 1;
		$_SESSION['id_person']=$global_user; 
		$hash = pass_solt(generateCode(10));
		$_SESSION['hash']=$hash;
		
		$filter = new filter;
		$id_session = $filter->html_filter(session_id());

		$Db->query = "SELECT mod_person.*, list_country.name_country_" . $set . ", list_country_arm.name_arm, list_country_arm_pod.name_pod, list_country.id_country, list_country_arm.id_arm, list_country_arm_pod.id_pod  FROM `mod_person`
				LEFT JOIN `list_country` ON (list_country.id_country=mod_person.rel_country)
				LEFT JOIN `list_country_arm` ON (list_country_arm.id_arm=mod_person.rel_arm)
				LEFT JOIN `list_country_arm_pod` ON (list_country_arm_pod.id_pod=mod_person.rel_pod)
				WHERE `id_person`='" . $global_user . "' AND act='1' LIMIT 1";
		$Db->query();
		$global_res = mysql_fetch_assoc($Db->lQueryResult);		
		
        $Db->query = "SELECT `id` FROM `list_sessions` WHERE `id_session` ='" . $id_session . "' LIMIT 1";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            mysql_query(
                "UPDATE `list_sessions` SET `up_date`=NOW(), `user`='" . $global_user . "' WHERE `id_session` ='" . $id_session . "' LIMIT 1"
            );
            mysql_query("UPRATE `mod_person` SET `last_date`=NOW() WHERE `user`='" . $global_user . "' LIMIT 1");
        } else {
            mysql_query(
                "INSERT INTO `list_sessions` (`id_session`, `up_date`, `user`) VALUES ('" . $id_session . "',NOW(),'" . $global_user . "')"
            );
        }
	}
}
else {
    $filter = new filter;
    $global_user = $filter->html_filter($_SESSION['id_person']);
    $hash = $filter->html_filter($_SESSION['hash']);

    $Db->query = "SELECT mod_person.*, list_country.name_country_" . $set . ", list_country_arm.name_arm, list_country_arm_pod.name_pod, list_country.id_country, list_country_arm.id_arm, list_country_arm_pod.id_pod  FROM `mod_person`
			LEFT JOIN `list_country` ON (list_country.id_country=mod_person.rel_country)
			LEFT JOIN `list_country_arm` ON (list_country_arm.id_arm=mod_person.rel_arm)
			LEFT JOIN `list_country_arm_pod` ON (list_country_arm_pod.id_pod=mod_person.rel_pod)
			WHERE `id_person`='" . $global_user . "' AND act='1' LIMIT 1";
    $Db->query();
    $global_res = mysql_fetch_assoc($Db->lQueryResult);

	
    if ($global_res["hash"] != $hash) {
        $autorized = 0;
        unset($_SESSION['id_person']);
        unset($_SESSION['hash']);
    } else {
        $autorized = 1;
        // для учета он-лайн
        $filter = new filter;
        $id_session = $filter->html_filter(session_id());
        $Db->query = "SELECT `id` FROM `list_sessions` WHERE `id_session` ='" . $id_session . "' LIMIT 1";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            mysql_query(
                "UPDATE `list_sessions` SET `up_date`=NOW(), `user`='" . $global_user . "' WHERE `id_session` ='" . $id_session . "' LIMIT 1"
            );
            mysql_query("UPRATE `mod_person` SET `last_date`=NOW() WHERE `user`='" . $global_user . "' LIMIT 1");
        } else {
            mysql_query(
                "INSERT INTO `list_sessions` (`id_session`, `up_date`, `user`) VALUES ('" . $id_session . "',NOW(),'" . $global_user . "')"
            );
        }
    }
}

// вытаскиваем модули
$Db->query = "SELECT `id_mod`, `name_mod` FROM `modules` WHERE `act_mod`='1' AND `act_admin_mod`='1'";
$Db->query();
while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
    $pages[] = $lRes[name_mod];
}

// вытаскиваем баннеры
$Db->query = "SELECT * FROM `mod_banners` WHERE `act`='1'";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        if ($lRes["rotate"] != 1) {
            if ($lRes["where"] == 0) {
                $banners["banners_everywhere"][$lRes["cat"]][] = str_replace(
                        "../upload/",
                        "/upload/",
                        stripslashes($lRes["source"])
                    ) . "|" . $lRes["link"] . "|" . $lRes["width"] . "|" . $lRes["height"] . "|" . $lRes["id_ban"];
            } else {
                if ($lRes["where"] == 1) {
                    $banners["banners_only_main"][$lRes["cat"]][] = str_replace(
                            "../upload/",
                            "/upload/",
                            stripslashes($lRes["source"])
                        ) . "|" . $lRes["link"] . "|" . $lRes["width"] . "|" . $lRes["height"] . "|" . $lRes["id_ban"];
                } else {
                    if ($lRes["where"] == 2) {
                        $banners["banners_only_notmain"][$lRes["cat"]][] = str_replace(
                                "../upload/",
                                "/upload/",
                                stripslashes($lRes["source"])
                            ) . "|" . $lRes["link"] . "|" . $lRes["width"] . "|" . $lRes["height"] . "|" . $lRes["id_ban"];
                    }
                }
            }
        } else {
            if ($lRes["where"] == 0) {
                $banners["banners_everywhere_rotate"][$lRes["cat"]][] = str_replace(
                        "../upload/",
                        "/upload/",
                        stripslashes($lRes["source"])
                    ) . "|" . $lRes["link"] . "|" . $lRes["width"] . "|" . $lRes["height"] . "|" . $lRes["id_ban"];
            } else {
                if ($lRes["where"] == 1) {
                    $banners["banners_only_main_rotate"][$lRes["cat"]][] = str_replace(
                            "../upload/",
                            "/upload/",
                            stripslashes($lRes["source"])
                        ) . "|" . $lRes["link"] . "|" . $lRes["width"] . "|" . $lRes["height"] . "|" . $lRes["id_ban"];
                } else {
                    if ($lRes["where"] == 2) {
                        $banners["banners_only_notmain_rotate"][$lRes["cat"]][] = str_replace(
                                "../upload/",
                                "/upload/",
                                stripslashes($lRes["source"])
                            ) . "|" . $lRes["link"] . "|" . $lRes["width"] . "|" . $lRes["height"] . "|" . $lRes["id_ban"];
                    }
                }
            }
        }
    }
}

// перевод на другие языки
$Db->query = "SELECT * FROM `list_translate`";
$Db->query();
global $lang;
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        if (!empty($lRes[$set])) {
            $lang[$lRes["id"]] = stripslashes($lRes[$set]);
        } else {
            $lang[$lRes["id"]] = stripslashes(
                $lRes["word"]
            );
        }
    }
}