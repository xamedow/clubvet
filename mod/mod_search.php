<?
$PHP_SELF = $_SERVER['PHP_SELF'];
if (!stripos($PHP_SELF, "index.php")) {
    die ("Access denied");
}

$filter = new filter;
$page_name = substr($filter->html_filter(@$param[1]), 0, -5);
$page = $filter->html_filter(@$param[1]);

$f_post = array();
if(isset($_POST)) {
    $f_post = clean_arr($_POST, $filter);
}
$male = 'checked';
$female = '';
if($f_post['sex'] === 'male') {
    $male = 'checked';
    $female = '';
} else if($f_post['sex'] === 'female'){
    $female = 'checked';
    $male = '';
}
$Db->query = "SELECT * FROM `mod_war`";
$Db->query();
if (mysql_num_rows($Db->lQueryResult)>0)
{
    while($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $current_war = ($f_post['war'] === $lRes['id_war']) ? ' selected="selected"' : '';
        $war_select .= "<option value=\"{$lRes["id_war"]}\" data-name=\"{$lRes["name_war"]}\">{$lRes["name_war"]}</option>";
    }
}



	//  текст описательный
		$array_text_id = array("rus"=>3, "eng"=>24, "ger"=>43);
		
		$Db->query = "SELECT * FROM `mod_content` WHERE id_content='".$array_text_id[$set]."' LIMIT 1";
        $Db->query();
		if (mysql_num_rows($Db->lQueryResult)>0)
		{
			$lRes = mysql_fetch_assoc($Db->lQueryResult);
			$normtext = str_replace("../", "/", stripslashes($lRes['text']));
			if (empty($lRes['sh1'])) $name = $lRes['name']; else $name = $lRes['sh1'];
			$title = $lRes['title'];
			$keys = $lRes['keys'];
			$meta = $lRes['meta'];

			$content.="<h1>".$name."</h1><div class='normtext'>".$normtext."</div>";
			unset($normtext);
			unset($name);
		}


	$content.= <<<EOD
    <form class='search_form' action='/search/' method='post'>
        <ul>
            <li>
                <input type='text' name='search_query' id='search_query' value='{$f_post['search_query']}' placeholder='{$lang[386]}'><br>
            </li>
            <li>
                <input type='radio' name='sex' value='male' id='male'$male>
                <label for='male'>{$lang[387]}</label><br>
                <input type='radio' name='sex' value='female' id='female'$female>
                <label for='female'>{$lang[388]}</label>

            </li>
            <li>
                <select name="war" id="war">
                    <option value="0">{$lang[389]}</option>
                    $war_select
                </select>
                <select name="event" id="event" disabled="disabled"><option value="0">{$lang[390]}</option></select>

            </li>
            <li>
                <label for='age_min'>{$lang[391]}: </label>
                <input type='input' name='age_min' id='age_min' value='{$f_post['age_min']}'>
                <label for='age_max'>{$lang[392]}: </label>
                <input type='input' name='age_max' id='age_min' value='{$f_post['age_max']}'><br>

            </li>
            <li class='buttons'>
                <input type='submit' name='search_submit' value='{$lang[393]}' class='button_enter'>
                <button type="reset">{$lang[394]}</button>
            </li>
        </ul>
    </form>
EOD;

if (isset($f_post['search_submit'])) {
    $subquery = '';
    extract($f_post);
    $subquery .= ($search_query) ? " AND name LIKE '%$search_query%'" : '';
    $subquery .= ($sex) ? " AND sex = '$sex'" : '';
	
	if (!empty($search_query))
	{
			$Db->query="INSERT INTO `mod_search` VALUES('','".$search_query."',now())";
			$Db->query();
			$Db->query="INSERT INTO `mod_stat` (`name`, `date`) VALUES ('На сайте ищут: ".$search_query."', NOW())";
			$Db->query(); 
	}
    $now = getdate();
    $age_max = $now['year'] - (int)$age_max;
    $age_min = $now['year'] - (int)$age_min;

    if($age_min!==$now['year'] && $age_max!==$now['year'] && ($age_min > $age_max)) {
        $subquery .= " AND DATE_FORMAT(date_birth, '%Y') BETWEEN '$age_max' AND '$age_min'";
    } else if($age_min !== $now['year']) {
        $subquery .= " AND DATE_FORMAT(date_birth, '%Y') < '$age_min'";
    } else if($age_max !== $now['year']) {
        $subquery .= " AND DATE_FORMAT(date_birth, '%Y') > '$age_max'";
    }

    $subquery .= ($event) ? " AND mod_war_rel.rel_event = $event" : '';

    $content .= "<h2 class=\"search_results\">{$lang[395]}</h2>";
    $query = "SELECT * FROM `mod_person`
				LEFT JOIN `list_sessions` ON (list_sessions.user=mod_person.id_person)
				LEFT JOIN `mod_war_rel` ON (rel_person=mod_person.id_person)
				WHERE `act`='1' AND `lang`='$set'$subquery GROUP BY `id_person` ORDER BY reg_date DESC";
//    var_dump($query);
    $Db->query = $query;
    $Db->query();
    if (mysql_num_rows($Db->lQueryResult) > 0) {

        while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
            $img = (file_exists(DOCUMENT_ROOT . "/upload/gallery/" . $lRes['id_person'] . "/logo.jpg"))
                ? "/upload/gallery/" . $lRes['id_person'] . "/logo.jpg"
                : '/images/default_profile_pic.jpg';

            $name = str_replace(' ', '<br>', trim($lRes['name']));

            if (!empty($lRes["user"])) {
                $timeFrom = strtotime(date("H:i:s", strtotime($lRes["up_date"])));
                $timeNow = strtotime(date("H:i:s"));
                $diff = $timeNow - $timeFrom;

                $days = floor($diff / (3600 * 24));
                $hours = floor(($diff - ($days * 3600 * 24)) / 3600);
                $minutes = floor(($diff - ($hours * 3600 + $days * 24 * 3600)) / 60);
                if ($minutes > 15) {
                    $time = news_date($lRes["last_date"]);
                    $online = '<span class="online">Был ' .$lang[358]. $time . '</span>';
                } else {
                    $online = '<span class="online">Online</span>';
                }

            } else {
                $time = news_date($lRes["last_date"]);
                $online = '<span class="online">' .$lang[358]. $time . '</span>';
            }

            $content .= '<div class="user_card">
								<a href="/person/' . $lRes['id_person'] . '.html">
									<img src="' . $img . '" alt="' . $lRes['name'] . '">
									<span>' . $name . '</span>
								</a>
								' . $online . '
							</div>';
        }
    } else {
        $content .= $lang[396];
    }
}
include("inc/header.php");
echo $content;
unset($content);
include("inc/footer.php");