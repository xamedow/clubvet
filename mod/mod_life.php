﻿<?
$PHP_SELF = $_SERVER['PHP_SELF'];
if (!stripos($PHP_SELF, "index.php")) {
    die ("Access denied");
}
$filter = new filter;

if ($param[1] == '') {
    $page = 1;
} else {
    $page = $filter->html_filter(@$param[1]);
}

$page_active = 'life';

$num = 14; // кол-во выводимых на страницу
$Db->query = "SELECT COUNT(id_file) FROM myfamily_files WHERE in_lifecont=1 AND private=1";
$Db->query();
$lRes = mysql_fetch_assoc($Db->lQueryResult);
$items = $lRes["COUNT(id_file)"]; //кол-во 
$total = (($items - 1) / $num) + 1;

$a = substr($items, strlen($items) - 1, 1);
$b = substr($items, strlen($items) - 2, 2);
// склоняем кол-во
if ($a == 0 or ($a >= 5 and $a <= 9) or ($b >= 11 and $b <= 14)) {
    $items_print = $items . ' файлов';
}
if ($a == 1 and $b != 11) {
    $items_print = $items . ' файл';
}
if ($a >= 2 and $a <= 4 and $b != 12 and $b != 13 and $b != 14) {
    $items_print = $items . ' файла';
}
$total = intval($total); // общее число страниц
$page = intval($page);
if (empty($page) or $page < 0) {
    $page = 1;
}
if ($page > $total) {
    $page = $total;
}
$start = $page * $num - $num;
if ($start < 0) {
    $start = 0;
}

for ($i = 1; $i <= $total; $i++) {
    if ($page != $i) {
        $navi .= ' <a href=/life/' . $i . '/>' . $i . '</a> ';
    } else {
        $navi .= ' <b>' . $i . '</b> ';
    }
}

    $Db->query = "SELECT myfamily_files.*,mod_person.name FROM myfamily_files
                    LEFT JOIN mod_person ON owner=id_person
                    WHERE in_lifecont=1 AND myfamily_files.private=1 ORDER BY id_file LIMIT $start, $num";
$Db->query();
$photo_clearcount = 0;
if (mysql_num_rows($Db->lQueryResult) > 0) {
    $content .= '<h1>' . $lang['319'] . '</h1><div id="life_wrap">';
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        if ($photo_clearcount == 7) {
            $content .= '<div class="clear"></div>';
        }
        $photo_clearcount = 0;

        switch ($lRes['type']) {
            case 1:
                $icon = 'image.png';
                $ftype = $lang['143'];
                break;
            case 2:
                $icon = 'music.png';
                $ftype = $lang['144'];
                break;
            case 3:
                $icon = 'movie.png';
                $ftype = $lang['145'];
                break;
        }

        $content .= <<<EOD
	<div class="one_mf_lc">
		<a rel="group" title="{$lang['154']} ($ftype)" href="/upload/gallery/$global_user/mf/{$lRes["source"]}">
			<img src="/images/$icon" />
			<span class="mf_name_lc">{$lRes['name_file']}<br>{$lang['320']}: {$lRes['name']}</span>
		</a>
	</div>
EOD;
        $photo_clearcount++;
    }
    if ($total > 1) {
        $navigation = "<div class='in_museum_paginator'>" . $navi . "</div>";
    }
    $content .= '</div><div class="clear"></div>' . $navigation;
} else {
    $content .= $lang['155'];
}
include("inc/header.php");
echo $content;
include("inc/footer.php");