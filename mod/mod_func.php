<? 
// класс подключения к базе
class Db {
private $host;
private $database;
private $name;
private $pass;
private $link;

public $query;

	// Конструктор
	public function __construct ($h, $n, $p, $b) {
$this->host=$h;
$this->name=$n;
$this->pass=$p;
		$this->database=$b;
	}

	// Функция подключения к БД
	public function connect() {
    
		if (!($this->link=mysql_connect($this->host,$this->name,$this->pass))) {
			$this->error('Не могу подключиться к '.$this->host);
			$noerrors=false;
		}

		if (!mysql_select_db($this->database)) {
			$this->error('Не могу войти в базу '.$this->database);
			$noerrors=false;
		}
		mysql_query("SET NAMES 'utf8'");
		mysql_query("SET GLOBAL time_zone = 'Europe/Moscow'");
		
		return $noerrors;

	}

	// Функция выполнения запроса
	public function query() {

		$this->lQueryResult=mysql_query($this->query)
		or $this->error('Не могу выполнить запрос'.mysql_error());
		return $this->lQueryResult;

	}
	function error ($what_error) {
	echo $what_error;
	}

}

// Класс фильтра входящих данных
class filter {
	public function html_filter($input) {
	
	$text = preg_replace('%&\s*\{[^}]*(\}\s*;?|$)%'
	, '', $input);
	$text = preg_replace('/[<>]/', '', $text);
	
			if(!get_magic_quotes_gpc()) {
			$text = addslashes($text);
			}
	$badwords = array('input', 'union', 'script', 'select', 'update', 'script', 'www', 'http', '.ru');
			$text = str_replace($badwords, '', $text);
	
	return $text;
	
	}
}
/* Функция обрезания текстовой строки до нужного кол-ва символов */
function substring($str,$count=150){
  $str=strip_tags($str);
  if (strlen($str)>$count) {
    $substr=substr($str,0,$count-1);
    return substr($substr,0,strlen($substr)-strlen(strrchr(substr($str,0,$count-1)," "))+1)."...";
  }else{
    return $str;
  }
}
/* Функция преобразования даты к нормальному виду */
function news_date($postdate, $time = true){ 
    $lastpost=date("d.m.Y", strtotime($postdate)); 
    $lastposttime=date("H:i", strtotime($postdate));     
    if ($time == true) $lastpost.= ", ".$lastposttime;
    return $lastpost; 
}
/* Функция раскладывающая страницы по дереву */	
	function getTree(&$data, $parent)
	{
		$out = array();
		if(!isset($data[$parent]))
			return $out;
		foreach($data[$parent] as $row)
		{
			$chidls = getTree($data, $row['id_cat']);	
			if ($chidls)
				$row['childs'] = $chidls;
			$out[] = $row;
		}
		return $out;
	}
	function getTreeContent(&$data, $parent)
	{
		$out = array();
		if(!isset($data[$parent]))
			return $out;
		foreach($data[$parent] as $row)
		{
			$chidls = getTreeContent($data, $row['id_content']);	
			if ($chidls)
				$row['childs'] = $chidls;
			$out[] = $row;
		}
		return $out;
	}
/* Функция возвращает список с категориями в древовидном виде */
function sub_pages($data, $num, $autorized)
{ 
if ($num == 0) $sub = '<ul id="menu">'; else  $sub = '<ul>';
foreach ($data as $k=>$v)
    {
		if (empty($v['redirect'])) $href = '/content/'.$v['id_content'].'-'.$v['anchor'].'.html'; else $href = $v['redirect'];
		if ($v['redirect']=="/" && $autorized==1) {$href='/office/index/'; $v['name']="Моя страница";}
		if ($active==$v['anchor']) $class = ' class="active"'; else $class = '';
        $sub.='
		<li'.$class.'><a href="'.$href.'">'.$v['name'].'</a>';
        if (isset($v['childs'])) $sub.=sub_pages($v['childs'], 1, $autorized);
		$sub.="</li>
		";
    }
$sub.= "</ul>
";
return $sub;
}
/* Функция удаления директории с вложеными в нее файлами */
function clear($dir)  
{  
    $opdir=opendir($dir);  
    while ($a = readdir($opdir))  
    {  
        if ($a != "." && $a != ".." && !is_dir($dir .'/'.$a))    
        {unlink($dir .'/'.$a);}  
        elseif($a != "." && $a != ".." && is_dir($dir .'/'.$a))  
        {clear($dir .'/'.$a);}  
    }  
 closedir ($opdir);  
 if(rmdir($dir)){return TRUE;}else{return FALSE;}  
} 
/* Функция шифровки пароля в md5 с солью */
function pass_solt($pass,$salt='tato86')  
{  
 $spec=array('~','!','@','#','$','%','^','&','*','?');  
 $crypted=md5(md5($salt).md5($pass));  
 $c_text=md5($pass);  
 for ($i=0;$i<strlen($crypted);$i++)  
 {  
 if (ord($c_text[$i])>=48 and ord($c_text[$i])<=57){  
  $temp.=$spec[$c_text[$i]];  
 } elseif(ord($c_text[$i])>=97 and ord($c_text[$i])<=100){  
  $temp.=strtoupper($crypted[$i]);  
 } else {  
  $temp.=$crypted[$i];  
 }  
 }  
 return md5($temp);  
} 
/* Функция определения расширения файла */
function what_ras($r,$t=null) 
{ 
$f=explode('.',$r); 
return strtolower($f[count($f)-1-$t]); 
} 
/* Транслит из анг в руский и наоборот */
function trans($str, $direction = 'ru_en')
{
        $ru = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я', ' ', '/', '-', '.', ',','"', '?','!','А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я','№','«','»',"%","\\","_","+","'","#","@","$","^",":",";","[","]","{","}","*");
        $en = array('a','b','v','g','d','e','e','zh','z','i','i','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','','y','','e','yu','ya','_', '_','_','_','_','','','','a','b','v','g','d','e','e','zh','z','i','i','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','','y','','e','yu','ya','','','','','','','_','','','','','','','','','','','','');
        if ($direction == 'en_ru')
                return str_replace($en, $ru, strtolower($str));
        else
                return str_replace($ru, $en, strtolower($str));
}
/* Функция определения реального IP */
function RealIP() 
{ 
   if( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ) 
   { 
      $client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" ); 
      $entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']); 

      reset($entries); 
      while (list(, $entry) = each($entries)) 
      { 
         $entry = trim($entry); 
         if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) ) 
         { 
            $private_ip = array( 
                  '/^0\./', 
                  '/^127\.0\.0\.1/', 
                  '/^192\.168\..*/', 
                  '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/', 
                  '/^10\..*/'); 
            $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]); 
            if ($client_ip != $found_ip) 
            { 
               $client_ip = $found_ip; 
               break; 
            } 
         } 
      } 
   } 
   else 
   { 
      $client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" ); 
   } 

   return $client_ip; 
} 
# Функция для генерации случайной строки 
function generateCode($length=6) { 
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789"; 
    $code = ""; 
    $clen = strlen($chars) - 1;   
    while (strlen($code) < $length) { 
            $code .= $chars[mt_rand(0,$clen)];   
    } 
    return $code; 
}
# Функция проверки емаил на валидность
function is_email($email){
  if (function_exists("filter_var")){
    $s=filter_var($email, FILTER_VALIDATE_EMAIL);
    return !empty($s);
  }
  $p = '/^[a-z0-9!#$%&*+-=?^_`{|}~]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*';
  $p.= '@([-a-z0-9]+\.)+([a-z]{2,3}';
  $p.= '|info|arpa|aero|coop|name|museum|mobi)$/ix';
  return preg_match($p, $email);
}
# Функция по созданию "тумбочек"
function create_thumbnail($orig_fname, $thum_fname, $thumb_width=100, $thumb_height=100, $do_cut=false)
{
    $rgb = 0xFFFFFF;
    $quality = 80;
    $size = @getimagesize($orig_fname);
    $src_x = $src_y = 0;

    if( $size === false) return false;
    $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
    $icfunc = "imagecreatefrom" . $format;
    if (!function_exists($icfunc)) return false;

    $orig_img = $icfunc($orig_fname);
    if (($size[0] <= $thumb_width) && ($size[1] <= $thumb_height))
    {
        // use original size
        $width  = $size['0'];
        $height = $size['1'];
    }
    else
    {
        $width  = $thumb_width;
        $height = $thumb_height;
        // calculate fit ratio
        $ratio_width  = $size['0'] / $thumb_width;
        $ratio_height = $size['1'] / $thumb_height;
        if ($ratio_width < $ratio_height)
        {
            if ($do_cut)
            {
                $src_y = ($size['1'] - $thumb_height * $ratio_width) / 2;
                $size['1'] = $thumb_height * $ratio_width;
            }
            else
            {
                $width  = $size['0'] / $ratio_height;
                $height = $thumb_height;
            }
        } else {
            if ($do_cut)
            {
                $src_x = ($size['0'] - $thumb_width * $ratio_height) / 2;
                $size['0'] = $thumb_width * $ratio_height;
            }
            else
            {
                $width  = $thumb_width;
                $height = $size['1'] / $ratio_width;
            }
        }
    }
    $thum_img = imagecreatetruecolor($width, $height);
    imagefill($thum_img, 0, 0, $rgb);
    imagecopyresampled($thum_img, $orig_img, 0, 0, $src_x, $src_y, $width, $height, $size[0], $size[1]);

    imagejpeg($thum_img, $thum_fname, $quality);
    flush();
    imagedestroy($orig_img);
    imagedestroy($thum_img);
    return true;
}
# проверка на правильность введенного урл
function check_url($url) 
{
    if (preg_match("/^((www.)?([\w, -]+.)(com|net|org|info|biz|spb\.ru|msk\.ru|com\.ru|org\.ru|net\.ru|ru|su|us|bz|ws))$/", $url)) {
        return true;
    }
    return false;
}
function print_banners ($banners, $cat, $pageavtive)
{
	$print = '';
	
		if (!empty($banners["banners_everywhere"][$cat])) 
		{
			foreach ($banners["banners_everywhere"][$cat] as $key=>$value) 
			{
				$print.=this_print($value);
			}
		}
		if (!empty($banners["banners_everywhere_rotate"][$cat])) 
		{
			$banrand = rand(0, count($banners["banners_everywhere_rotate"][$cat])-1);
			$print.=this_print($banners["banners_everywhere_rotate"][$cat][$banrand]);
		}
		
	if ($pageavtive == 'index')
	{
		if (!empty($banners["banners_only_main"][$cat])) 
		{
			foreach ($banners["banners_only_main"][$cat] as $key=>$value) 
			{
				$print.=this_print($value);
			}
		}
		if (!empty($banners["banners_only_main_rotate"][$cat])) 
		{
			$banrand = rand(0, count($banners["banners_only_main_rotate"][$cat])-1);
			$print.=this_print($banners["banners_only_main_rotate"][$cat][$banrand]);
		}
	}
	else
	{
		if (!empty($banners["banners_only_notmain"][$cat])) 
		{
			foreach ($banners["banners_only_notmain"][$cat] as $key=>$value) 
			{
				$print.=this_print($value);
			}
		}
		if (!empty($banners["banners_only_notmain_rotate"][$cat])) 
		{
			$banrand = rand(0, count($banners["banners_only_notmain_rotate"][$cat])-1);
			$print.=this_print($banners["banners_only_notmain_rotate"][$cat][$banrand]);
		}
	}
	return $print;
	
}
function this_print($str)
{
			$banprint = explode("|", $str);
			$link = pass_solt($banprint[1]);
			$banprint[0] = str_replace("<p>", "", $banprint[0]);
			$banprint[0] = str_replace("</p>", "", $banprint[0]);
			$str = '<div class="banblock" style="width: '.$banprint[2].'px; height:'.$banprint[3].'px;"><a href="/goto.php?data='.base64_encode($banprint[1]).'&amp;linkhash='.$link.'&amp;id='.$banprint[4].'" target="_blank">'.$banprint[0].'</a><a href="/goto.php?data='.base64_encode($banprint[1]).'&amp;linkhash='.$link.'&amp;id='.$banprint[4].'" target="_blank" style="display: block; position: absolute; left: 0pt; top: 0pt; width:'.$banprint[2].'px; height:'.$banprint[3].'px; z-index: 99; border: 0pt none; cursor: pointer;"><img alt="" src="/images/space.gif" width="'.$banprint[2].'" height="'.$banprint[3].'" /></a></div>';
		return $str;
}
	
function news_oc_date($date, $lang = "rus")
{
		$month["rus"] = array(
                    1 => 'января', 
                    2 => 'февраля', 
                    3 => 'марта', 
                    4 => 'апреля', 
                    5 => 'мая', 
                    6 => 'июня', 
                    7 => 'июля', 
                    8 => 'августа', 
                    9 => 'сентября', 
                    10 => 'октября', 
                    11 => 'ноября', 
                    12 => 'декабря');
					
		$month["eng"] = array(
                    1 => 'january', 
                    2 => 'february', 
                    3 => 'march', 
                    4 => 'april', 
                    5 => 'may', 
                    6 => 'june', 
                    7 => 'july', 
                    8 => 'august', 
                    9 => 'september', 
                    10 => 'october', 
                    11 => 'november', 
                    12 => 'december');
					
		$month["ger"] = array(
                    1 => 'januar', 
                    2 => 'februar', 
                    3 => 'märz', 
                    4 => 'april', 
                    5 => 'mai', 
                    6 => 'juni', 
                    7 => 'juli', 
                    8 => 'august', 
                    9 => 'september', 
                    10 => 'oktober', 
                    11 => 'november', 
                    12 => 'dezember');			
		$date = date_parse($date);

		$asd['day'] = $date['day'];
		$asd['month'] = $month[$lang][$date['month']];
		$asd['year'] = $date['year'];
		return $asd;
}
// склонение по словам
function num2word($num,$words) {
  $num=$num%100;
  if ($num>19) { $num=$num%10; }
  switch ($num) {
    case 1:  { return($words[0]); }
    case 2: case 3: case 4:  { return($words[1]); }
    default: { return($words[2]); }
  }
}
function unixToMySQL($timestamp)
{
    return date('Y-m-d H:i:s', $timestamp);
}
function br2nl ( $text ) 
{ 
 $text = preg_replace ( '/<br\\s*?\/??>/i' , '' , $text );
 $text = str_replace("<p>", "", $text);
 $text = str_replace("</p>", "", $text);
 return $text;
}
function Clear_array($array)
{
	$c=sizeof($array);
	$tmp_array=array();
	for($i=0; $i<$c; $i++)
	{
	if (!(trim($array[$i])==""))
		{
		$tmp_array[]=$array[$i];
		}
	}
	return $tmp_array;
}
function cat($id){ 
	$return=array();
	$m=mysql_query('SELECT * FROM `mod_content` WHERE `id_content`='.$id); 
	if(mysql_num_rows($m)){ 
		$m=mysql_fetch_assoc($m);
		$return[]=array($m['id_content'],$m['name'],$m['anchor'],$m['redirect']); 
			if($m['parent']){
				$return=array_merge($return,cat($m['parent'])); 
			}
	}
	return $return;
}
function cat_print($id){ 
	$print=cat($id);
	$print=array_reverse($print); 
	$what = '<div class="where"><a href="/">Главная</a> / <a href="/gallery/">Портфолио</a> / ';
		for($i=0,$s=sizeof($print);$i<$s;$i++){ 
			$what.= '<a href="/gallery/'.$print[$i][2].'.html">'.$print[$i][1].'</a> / ';
		}
	return $what;
}
// функция набора ошибочно данных 
function error_login() {
							mysql_query("DELETE FROM `error_login` WHERE UNIX_TIMESTAMP() - UNIX_TIMESTAMP(date) > 900");
							$res = mysql_query("SELECT `col` FROM `error_login` WHERE `ip`='".RealIP()."'");
							$lRes=mysql_fetch_assoc($res);
							if ($lRes['col'] > 2) 
							{
									//если ошибок больше двух, т.е три, то выдаем сообщение.
									$error = 1;
									
							}      
							else
							{
								$error = 0;

								$res = mysql_query("SELECT ip FROM error_login WHERE ip='".RealIP()."'");
								$lRes=mysql_fetch_assoc($res);
									if (RealIP() == $lRes[ip]) 
									{
       									mysql_query("UPDATE `error_login` SET `col`=col+1,`date`=NOW() WHERE `ip`='".RealIP()."'");
									}          
										else 
									{
										mysql_query("INSERT INTO error_login (ip,date,col) VALUES  ('".RealIP()."',NOW(),'1')");
									}   
							}
	return $error;
}
/**
 * Фильтрует ассоциативный массив, с помощью метода html_filter, объекта класса filter.
 * @param  array $in              входящий массив
 * @param  object $filter         объекта класса filter
 * @return array | boolean        выходящий массив
 */
function clean_arr($in, $filter) {
    $out = array();
    if(method_exists($filter, 'html_filter')) {
      foreach($in as $key=>$val) {
          $out[$key] = $filter->html_filter($val);
      }
      return $out;
    }
    return false;
}

/**
 * Заменяет ключевые слова ссылками.
 * @param $value input string
 * @param $Db object Db class instance
 * @return false or string
 */
function links_subst($value, $Db)
{
    if ($value) {
        $tech = getTechArr($Db);
        foreach ($tech as $v) {
            $temp = mb_strtolower($v['name_t'], 'UTF-8');
            var_dump($temp);
            $value = str_ireplace(
                $temp,
                "<a class=\"red_e\" target=\"_blank\" href=\"{$v['link_t']}\">{$v['name_t']}</a>",
                $value
            );
        }
        return $value;
    }
    return false;
}

/**
 * Хелпер к предидущей функции, получает массив пар имя/адрес ссылки.
 * @param $Db
 * @return array
 */
function getTechArr($Db)
{
    $tech = array();
    $Db->query = "SELECT name_t, link_t FROM list_teh";
    $Db->query();

    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $tech[] = array(
            'name_t' => $lRes['name_t'],
            'link_t' => $lRes['link_t']
        );
    }
    return $tech;
}

function to_feed($Db, $event, $id) {
    $Db->query = "INSERT INTO feed (date, event, person_id) VALUES(NOW(), '{$event}', $id)";
    $Db->query();
}

/* Формирует массив страниц и их детей */
function treepagesformap(&$data, $parent)
{
	$out=array();
	if (!isset($data[$parent]))
		return $out;
	foreach ($data[$parent] as $row)
	{
		$chidls=treepagesformap($data, $row['id_content']);
		if ($chidls)
			$row['childs']=$chidls;
		$out[]=$row;
	}
	return $out;
}

/* Формирует и выводит список страниц с подстраницами */
function viewmap($data)
{ 
echo '<ul class="map_ul">';
foreach ($data as $k=>$v)
    {
		if ($v['redirect']!='') $url=$v['redirect']; else $url='/content/'.$v['id_content'].'-'.$v['anchor'].'.html';
        echo '<li><a href="'.$url.'">'.$v['name'].'</a>'; 
        if (isset($v['childs'])) viewmap($v['childs']);
		echo "</li>";
    }
echo '</ul>';
}
function selectFromEnum($table, $field, $db, $current_id = 0)
{
    $options = null;
    $data = getEnumData($table, $field, $db);
    $data_arr = explode(',', $data);
    $current = getCurrent($table, $field, $db, $current_id);
    foreach ($data_arr as $option) {
        $option = trim($option, "'");
        $selected = $option === $current ? ' selected="selected"' : '';
        $options .= "<option value='$option'$selected>$option</option>";
    }

    return $options ? "<select name='$field'>$options</select>" : null;
}

function getCurrent($table, $field, $db, $current_id)
{
    $db->query = "SELECT $field FROM $table WHERE id={$current_id}";
    $db->query();
    $data = mysql_fetch_assoc($db->lQueryResult);

    return trim($data[$field], "'");
}

function getEnumData($table, $field, $db)
{
    $db->query = "SHOW columns FROM $table WHERE Field='$field'";
    $db->query();
    $data = mysql_fetch_assoc($db->lQueryResult);

    return strpos($data['Type'], 'enum') !== false
        ? substr($data['Type'], 5, -1)
        : null;
}
function getPair($arr) {
    $out = null;
    if(is_array($arr)) {
        foreach($arr as $key => $val) {
            $val = $val === null ? 'null' : "'$val'";
            $out .= "$key=$val,";
        }
    }
    return rtrim($out, ',');
}
function setnull($elem) {
    return $elem === '' ? null : $elem;
}
function implodenull($delim, $arr) {
    $out = null;
    if(is_array($arr)) {
        foreach($arr as $val) {
            $val = $val === null ? 'null' : "'$val'";
            $out .= $val . $delim;
        }
    }
    return rtrim($out, $delim);
}
function addEnum($new_item, $table, $field, $db) {
    $enum = "'$new_item', " . getEnumData($table, $field, $db);
    $db->query = "ALTER TABLE $table MODIFY COLUMN $field ENUM($enum)";
    $db->query();
}