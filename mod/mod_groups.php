<?php
$PHP_SELF = $_SERVER['PHP_SELF'];
if (!stripos($PHP_SELF, "index.php")) {
    die ("Access denied");
}

$filter = new filter;
$page_name = substr($filter->html_filter(@$param[1]), 0, -5);
$page = $filter->html_filter(@$param[1]);
$page_active = 'groups';

// Group invitations handling.
if(isset($_POST['invite_submit'])) {
    $f_post = clean_arr($_POST, $filter);

    $Db->query = "INSERT INTO person_group_invitations 
                    (status, group_id, person_id) 
                    VALUES ('open',{$f_post['group_id']},{$f_post['person_id']})";
    if($Db->query()) {
        echo "
            <script>alert('".$lang['79']."');</script>
        ";
    }
}

// Update group request handling.
if(isset($_POST['group_setup_submit'])) {
    $f_post = clean_arr($_POST, $filter);

    $img =  '';
    if (!empty($_FILES["group_file"]["name"])) {
        $source = $_FILES["group_file"]["tmp_name"];
        $myrand = rand();
        $img_name_full = "/upload/groups/" . $myrand . ".jpg";
        create_thumbnail(
            $source,
            $_SERVER['DOCUMENT_ROOT'] . $img_name_full,
            $thumb_width = 150,
            $thumb_height = 170,
            $do_cut = true
        );
        $img = ", `img`='$myrand' ";
    }
	
	if ($_POST['private_group']) $private_group=1; else $private_group=0;

    $Db->query = "UPDATE `mod_groups` SET 
        `name`= '{$f_post['group_name']}', 
        `description`='{$f_post['group_description']}',
        country_id='{$f_post['group_country']}',
		`private_group`='{$private_group}'
        $img
         WHERE id={$f_post['group_id']}";
    $Db->query();
}

// All groups.
if (!@($page_name)) {
    $country = $search_query = $subquery = '';
    if(isset($_POST['search_submit'])) {
        $filter = new filter();
        $search_query = (empty($_POST['search_query'])) ? '' : $filter->html_filter($_POST['search_query']);
        $country = (empty($_POST['country'])) ? 0 : $filter->html_filter($_POST['country']);

        $subquery .= ($search_query) ? "name LIKE '%$search_query%'" : '';
        $amp = ($subquery) ? ' AND ' : '';
        $subquery .= ($country) ? "{$amp}country_id = $country" : '';
        $subquery = ($subquery) ? 'WHERE '.$subquery : '';
    }

    $countries = '';
    $Db->query = "SELECT id_country, name_country_$set as name FROM list_country";
    $Db->query();
    if (mysql_num_rows($Db->lQueryResult) > 0) {
        while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
            $selected = ($lRes['id_country'] == $country) ? ' selected' : '';
            $countries .= "<option$selected value=\"{$lRes['id_country']}\">{$lRes['name']}</option>>";
        }
        $countries = ($country === 0)
            ? '<option selected value=\'0\'>'.$lang['301'].'</option>'.$countries
            : '<option value=\'0\'>'.$lang['301'].'</option>'.$countries;
    }
    $content = <<<EOD
        <h1>{$lang['176']}</h1>
        <form action='' method='post' class='search_form'>
            <input type='text' name='search_query' value='$search_query' placeholder="{$lang['302']}">
            <select name='country'>
                $countries
            </select>
            <input type='submit' value='{$lang['303']}' name='search_submit' class='button_search'>
        </form>
EOD;

	$global_user = ($global_user) ? $global_user : 0;
    $Db->query = "SELECT person_groups.person_id, mod_groups.* FROM mod_groups LEFT JOIN person_groups ON (mod_groups.id=person_groups.group_id) WHERE person_groups.person_id=".$global_user." OR mod_groups.private_group='0' GROUP BY id ORDER BY name";
    $Db->query();

    if (mysql_num_rows($Db->lQueryResult) > 0) {
        while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
            $img = (file_exists($_SERVER['DOCUMENT_ROOT']."/upload/groups/{$lRes['img']}.jpg"))
                ? "/upload/groups/{$lRes['img']}.jpg"
                : '/images/default_profile_pic.jpg';
//            $name = str_replace(' ', '<br>', trim($lRes['name']));
            $content .= <<<EOD
        <div class="user_card" id="card{$lRes['id']}">
             <a href="/groups/{$lRes['id']}.html">
                <img src="$img" alt="{$lRes['name']}">
                <span>{$lRes['name']}</span>
            </a>
        </div>
EOD;

        }
    } else {
        $content .= $lang['304'];
    }
} // Current group.
else {
    $Db->query = "
SELECT
  mod_groups.id,
  description,
  mod_groups.name,
  creator_id,
  country_id,
  mod_person.name AS host_name,
  img,
  private_group,
  GROUP_CONCAT(person_id SEPARATOR ',') AS users
FROM mod_groups
  LEFT JOIN mod_person ON mod_groups.creator_id = id_person
  LEFT JOIN person_groups ON group_id = mod_groups.id
WHERE mod_groups.id = $page_name
GROUP BY id;
";
    $Db->query();
    if (mysql_num_rows($Db->lQueryResult) > 0) {
        $lRes = mysql_fetch_assoc($Db->lQueryResult);
		if ($lRes['private_group']) $private_group_box=' checked'; else $private_group_box='';
        $normtext = str_replace("../", "/", stripslashes($lRes['text']));
        $group_img = (file_exists($_SERVER['DOCUMENT_ROOT']."/upload/groups/{$lRes['img']}.jpg"))
            ? "/upload/groups/{$lRes['img']}.jpg"
            : '/images/default_profile_pic.jpg';
        $users_arr = explode(',', $lRes['users']);
        $users_count = count($users_arr);
        $actions = ($autorized && $global_user !== $lRes['creator_id'])
            ? (in_array($global_user, $users_arr))
                ? "<a href=\"#\" class=\"quit_group button_enter\" data-group_id=\"{$lRes['id']}\" data-person_id=\"$global_user\">Выйти из сообщества</a>"
                : "<a href=\"#\" class=\"join_group button_enter\" data-id=\"$global_user\" data-group_id=\"{$lRes['id']}\">Вступить в сообщество</a>"
            : "";

// Creator actions.
        if($global_user === $lRes['creator_id']) {
            $countries = '';
            $Db->query = "SELECT id_country as id, name_country_".$set." as name FROM list_country ORDER BY name_country_".$set." ASC";
            $Db->query();
            if (mysql_num_rows($Db->lQueryResult) > 0) {
                while ($c_res = mysql_fetch_assoc($Db->lQueryResult)) {
                    $selected = ($lRes['country_id'] === $c_res['id']) ? ' selected' : '';
                    $countries .= "<option value=\"{$c_res['id']}\"$selected>{$c_res['name']}</option>";
                }
            }

            $group_invite_li = "<li><a date-mod=\"invite\">".$lang['305']."</a></li>";
            $group_setup_li = "<li><a date-mod=\"setup\">".$lang['109']."</a></li>";
            $group_invite_div = <<<EOD
            					<div id="invite" data-id="$global_user" class="tabs_all">
                                    <form action="#" method="post">
                                        <input type="text" name="target_name" id="target_list"required class="input_field">                            
                                        <input type="hidden" name="init_by_id" value='$global_user'>
                                        <input type="hidden" name="group_id" value='{$lRes['id']}'>
                                        <input type="hidden" name="person_id" value=''>
                                        <input type="submit" name="invite_submit" value="{$lang['305']}" class="button_search">
                                    </form>
                                </div>
EOD;
            $group_setup_div = <<<EOD
            					<div id="setup" data-id="$global_user" class="tabs_all">
                                    <form action="#" method="post" enctype="multipart/form-data">
                                    	<ul>
                                    	<li>
                                        	<label for="group_name">{$lang['179']}</label>
                                        	<input type="text" name="group_name" id="group_name" class="input_field" value="{$lRes['name']}" required>
                                        </li>
                                        <li>
	                                        <label for="group_description">{$lang['140']}</label>
	                                        <textarea id="group_description" name="group_description">{$lRes['description']}</textarea>
                                        </li>
                                        <li>
											<label for="group_file">{$lang['306']}</label>
											<input type="file" id="group_file" name="group_file">
                                        </li>
                                        <li>
                                            <label for="group_country">{$lang['181']}</label>
                                            <select id="group_country" name="group_country" required="required">
                                                $countries
                                            </select>
                                        </li>
										<li>
											<label for="private_group">Закрытое сообщество (доступ только по приглашениям)</label>
											<input type="checkbox" name="private_group" id="private_group" $private_group_box>
										</li>
                                        <li>
                                        	<input type="submit" name="group_setup_submit" value="{$lang['113']}" class="button_search">
                                    	</li>
                                    	</ul>
                                        <input type="hidden" name="group_id" value="{$lRes['id']}">
                                    </form>
                                    <a href="#" class="delete_group" data-id="{$lRes['id']}">{$lang['307']}</a>
								</div>
EOD;
        }

// Group members list.        
        $members_list = '';
        $Db->query = "SELECT id_person as id, name FROM mod_person WHERE id_person IN ({$lRes['users']})";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            while ($q_res = mysql_fetch_assoc($Db->lQueryResult)) {
                $img = (file_exists(DOCUMENT_ROOT."/upload/gallery/{$q_res['id']}/logo.jpg"))
                    ? "/upload/gallery/{$q_res['id']}/logo.jpg"
                    : '/images/default_profile_pic.jpg';
                $name = str_replace(' ', '<br>', trim($q_res['name']));
// Actions.                
                $remover = ($global_user === $lRes['creator_id'] && $q_res['id'] !== $lRes['creator_id'])
                    ? "<a href='' class='delete_from_group' data-group_id='{$lRes['id']}' data-person_id=\"{$q_res['id']}\">".$lang['158']."</a>"
                    : '';
            	$writer = ($autorized && $q_res['id'] !== $global_user) 
            		? "<a href=\"/office/message/new?to={$q_res['id']}\">".$lang['157']."</a>"
            		: '';

                $members_list .= <<<EOD
        <div class="user_card" id="card{$q_res['id']}">
            <a href="/person/{$q_res['id']}.html">
                <img src="$img" alt="{$q_res['name']}">
                <span>$name</span>
            </a>
            <div>
                $writer
                $remover
            </div>
        </div>
EOD;
            }
        } else {
            $members_list .= '<p>'.$lang['160'].'</p>';
        }
// Comments.
        $comments_form = (in_array($global_user, $users_arr))
            ? <<<EOD
               <form id="send_comment" data-mod="groups" data-id="$page_name" style="overflow: hidden;">
					<input type="text" name="name" class="name" value="" placeholder="{$lang['297']}" required="required" />
					<input type="text" name="email" class="email" value="" placeholder="{$lang['299']}" required="required" />
					<textarea name="comment" class="comments" placeholder="{$lang['308']}" required="required"></textarea>
					<input type="submit" class="button_enter send_comment" value="{$lang['309']}" name="submit_comment" />
				</form>
EOD
            : '';
        $comments = '';
        $Db->query = "SELECT * FROM `mod_comment` WHERE `page_id`='".$page_name."' AND `mod`='groups' ORDER BY `date` DESC";
        $Db->query();
        if(mysql_num_rows($Db->lQueryResult) > 0)
        {
            while ($q_res1=mysql_fetch_assoc($Db->lQueryResult)) {
                $comments.= '<div class="one_comment">
					<div class="date_comment">'.news_date($q_res1["date"]).'</div>
						<span>'.$q_res1["fio_comment"].'</span>
						<p>'.$q_res1["text_comment"].'</p>
					</div>';
            }
        }

// View.
        $content = <<<EOD
            <img class='group_logo' src="$group_img" alt="{$lRes['name']}">
            <h1>{$lRes['name']}</h1>
            <p class="host">{$lang['310']}: <a href="/person/{$lRes['creator_id']}.html">{$lRes['host_name']}</a></p>
            <p>{$lRes['description']}</p>
            $actions
            <ul id="profile_menu_hor">
                <li class="active"><span date-mod="first">{$lang['311']}</span></li>
                <li><a date-mod="engaged">{$lang['312']} ($users_count {$lang['313']})</a></li>
                $group_invite_li
                $group_setup_li
            </ul>
            <div id="first" data-id="$global_user" class="tabs_all">
                <h2 style="margin-left:30px;">{$lang['311']}</h2>
			    <br>
				$comments_form
                <div class="list_comment">
                    $comments
                </div>
            </div>
            <div id="engaged" data-id="$global_user" class="tabs_all">
                $members_list
            </div>
            $group_invite_div
            $group_setup_div
EOD;
;
        unset($lRes);
        unset($normtext);
    } else {
        header('HTTP/1.0 404 not found');
        $content = "<h1>".$lang['295'].".</h1><p>".$lang['284'].".</p><div class='eror404'></div>";
    }
}
include("inc/header.php");
echo $content;
unset($content);
include("inc/footer.php");