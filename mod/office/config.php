<?php
$PHP_SELF=$_SERVER['PHP_SELF']; if (!stripos($PHP_SELF,"index.php")) die ("Access denied"); 
if(isset($_POST)) {
    $filter = new filter();
    $f_post = array();
    foreach($_POST as $key => $value) {
        $f_post[$key] = $filter->html_filter($value);
    }
}
// Pass form handler.
if(isset($_POST['pass_submit'])) {
    $Db->query = "SELECT pass, mail FROM mod_person WHERE id_person = $global_user";
    $Db->query();

    $q_res = mysql_fetch_assoc($Db->lQueryResult);

    if( (pass_solt($f_post['old_pass']) === $q_res['pass']) && ($f_post['new_pass'] === $f_post['check_pass'])) {
        $new_pass = pass_solt($f_post["new_pass"]);
        $Db->query = "UPDATE mod_person SET pass = '$new_pass' WHERE id_person = $global_user";
        $Db->query();

        $mail_to = $f_post["mail"];
        $body = $lang['101'].' '.$DomenName.'. '.$lang['102'].': '.$f_post['new_pass'];
        $subject = $lang['103'].' '.$DomenName;
        $headers = 'From: ' . $config['main']['main_email'] . "\r\n" . 'Content-Type: text/html; charset=utf-8';

        if(mail($mail_to, $subject, $body, $headers)) {
            $content .= "<script>alert('".$lang['104']."');</script>";
        }

    }
}

// Form handler.
if(isset($_POST['notification_submit'])) {
    $subquery = '';
    foreach($f_post as $key => $value) {
        if($value === 'on') {
            $subquery .= " $key";
        }
    }
    $subquery = trim( $subquery );

    $Db->query = "UPDATE mod_person SET notifications = '$subquery' WHERE id_person = $global_user";
    $Db->query();
}

// Form filler.
$Db->query = "SELECT notifications FROM mod_person WHERE id_person = $global_user";
$Db->query();

$res_string = mysql_fetch_assoc($Db->lQueryResult);
$res_string = $res_string['notifications'];

$notification_arr = array(
    'messages' => $lang['105'],
    'invites' => $lang['106'],
    'birthdays' => $lang['107'],
    'comments' => $lang['108']
);

$notifications = '';
foreach( $notification_arr as $key => $value ) {
    $checked = (strpos($res_string, $key) !== false) ? 'checked' : '';
    $notifications .= <<<EOD
                <li>
                    <input type="checkbox" name="$key" id="$key"$checked>
                    <label for="$key">$value</label>
                </li>
EOD;

}

//Private
$Db->query = "SELECT `private` FROM `mod_person` WHERE `id_person`=$global_user LIMIT 1";
$Db->query();
if (mysql_num_rows($Db->lQueryResult)>0) {
$current_private = mysql_fetch_assoc($Db->lQueryResult);
$current_private = $current_private['private'];
}

//Private handler
if($_POST['private_submit']) {
	$Db->query = "UPDATE mod_person SET private = '".$_POST['privates']."' WHERE id_person = '$global_user'";
	$Db->query();
	$current_private=$_POST['privates'];
}

$ch1='';$ch2='';$ch3='';
switch ($current_private) {
	case 1:
		$ch1=' checked';
		break;
	case 2:
		$ch2=' checked';
		break;
	case 3:
		$ch3=' checked';
		break;
}

// Content output.
$content .= <<<EOD
    <h1 class="user_profile_name">{$lang['109']}</h1>
    <ul id="profile_menu_hor">
        <li class="active"><span date-mod="first">{$lang['110']}</span></li>
        <li><a date-mod="private_change">{$lang['118']}</a></li>		
        <li><a date-mod="pass_change">{$lang['111']}</a></li>
    </ul>
    <div id="first" data-id="$global_user" class="tabs_all">
        <form method="post" action="">
            <ul>
                 $notifications
                 <li>
                    <input type="submit" name="notification_submit" class="button_search" value="{$lang['113']}">
                 </li>
            </ul>
        </form>
    </div>
    <div id="private_change" data-id="$global_user" class="tabs_all">
        <form method="post" action="">
            <ul>
				<li><input type="radio" name="privates" value="1" $ch1>{$lang['408']}</li>
				<li><input type="radio" name="privates" value="2" $ch2>{$lang['409']}</li>
				<li><input type="radio" name="privates" value="3" $ch3>{$lang['410']}</li>
                <li><input type="submit" name="private_submit" class="button_search" value="{$lang['113']}"></li>
            </ul>
        </form>
    </div>
    <div id="pass_change" data-id="$global_user" class="tabs_all">
    <form method="post" action="">
            <ul>
                <li>
                    <label class="db" for="old_pass">{$lang['114']}</label>
                    <input type="password" name="old_pass" id="old_pass" required>
                </li>
                <li>
                    <label class="db" for="new_pass">{$lang['115']}</label>
                    <input type="password" name="new_pass" id="new_pass" required>
                </li>
                <li>
                    <label class="db" for="check_pass">{$lang['116']}</label>
                    <input type="password" name="check_pass" id="check_pass" required>
                </li>
                 <li>
                    <input type="submit" name="pass_submit" class="button_enter" value="{$lang['113']}">
                 </li>
            </ul>
        </form>
    </div>
EOD;
