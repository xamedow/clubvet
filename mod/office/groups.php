<?php
$PHP_SELF=$_SERVER['PHP_SELF']; if (!stripos($PHP_SELF,"index.php")) die ("Access denied"); 
// New group request handling.
if(isset($_POST['new_group'])) {
    $filter = new filter();
    $f_post = array();
    foreach($_POST as $key=>$val) {
        $f_post[$key] = $filter->html_filter($val);
    }
	if ($_POST['private_group']) $private_group=1; else $private_group=0;

    if (!empty($_FILES["image"]["name"])) {
        $source = $_FILES["image"]["tmp_name"];
        $myrand = rand();
        $img_name_full = "/upload/groups/" . $myrand . ".jpg";
        create_thumbnail(
            $source,
            $_SERVER['DOCUMENT_ROOT'] . $img_name_full,
            $thumb_width = 150,
            $thumb_height = 170,
            $do_cut = true
        );
    }

    $Db->query="INSERT INTO `mod_groups` (`name`, `description`, `img`, `country_id`, creator_id, `private_group`)
              VALUES ('{$f_post['group_name']}', '{$f_post['group_description']}', '$myrand', '{$f_post['group_state']}', $global_user, $private_group)";
    $Db->query();
    $current_id = mysql_insert_id();
    $Db->query="INSERT INTO `person_groups` (person_id, group_id)
              VALUES ($global_user, $current_id)";
    $Db->query();
}

// Groups list.
$group_list = '';
$Db->query = "SELECT person_groups.id, name, img, creator_id, group_id
                FROM mod_groups left JOIN person_groups ON mod_groups.id = group_id
                WHERE person_id=$global_user";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $img = (file_exists(DOCUMENT_ROOT."/upload/groups/{$lRes['img']}.jpg"))
            ? "/upload/groups/{$lRes['img']}.jpg"
            : '/images/default_profile_pic.jpg';
        $name = str_replace(' ', '<br>', trim($lRes['name']));
        $action = ($lRes['creator_id'] !== $global_user)
            ? "<a href=\"#\" class=\"quit_group\" data-group_id=\"{$lRes['group_id']}\" data-person_id=\"$global_user\">".$lang['172']."</a>"
            : "<a href=\"#\" class=\"delete_group\" data-id=\"{$lRes['id']}\">".$lang['158']."</a>";

        $group_list .= <<<EOD
        <div class="user_card" id="group_card{$lRes['id']}">
            <a href="/groups/{$lRes['group_id']}.html">
                <img src="$img" alt="{$lRes['name']}">
                <p>$name</p>
            </a>
            $action
        </div>
EOD;
    }
} else {
    $group_list .= '<p><br />'.$lang['174'].'.</p>';
}
// Invites.
$invites_list = '';
$invites_count = 0;
$Db->query = "SELECT person_group_invitations.id, group_id, mod_groups.name FROM mod_groups left JOIN person_group_invitations
                ON mod_groups.id = group_id WHERE person_id=$global_user AND person_group_invitations.status='open'";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $img = (file_exists(DOCUMENT_ROOT."/upload/groups/{$lRes['group_id']}.jpg"))
            ? "/upload/groups/{$lRes['group_id']}.jpg"
            : '/images/default_profile_pic.jpg';
        $name = str_replace(' ', '<br>', trim($lRes['name']));

        $invites_list .= <<<EOD
        <div class="user_card" id="card{$lRes['id']}">
             <a href="/group/{$lRes['group_id']}.html">
                <img src="$img" alt="{$lRes['name']}">
                <span>$name</span>
            </a>
            <div>
                <a href="#" class="reject_group_invite" data-id="{$lRes['id']}">{$lang['161']}</a>
                <a href="#" class="accept_group_invite" data-id="{$lRes['id']}" data-init_id="{$lRes['group_id']}" data-person_id="{$global_user}">{$lang['162']}</a>
            </div>
        </div>
EOD;

        ++$invites_count;
    }
} else {
    $invites_list .= '<p><br />'.$lang["175"].'.</p>';
}

$invites_count = ($invites_count)
    ? array(" class=\"with_counter\"","<i class=\"counter\">$invites_count</i>")
    : array('','');

// Countries.
$countries = '';
$Db->query = "SELECT id_country as id, name_country_".$set." as name FROM list_country ORDER BY name_country_".$set." ASC";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $countries .= "<option value=\"{$lRes['id']}\">{$lRes['name']}</option>";
    }
}

// Content output.
$content .= <<<EOD
    <h1 class="user_profile_name">{$lang['176']}</h1>
    <ul id="profile_menu_hor">
        <li class="active"><span date-mod="first">{$lang['177']}</span></li>
        <li><a{$invites_count[0]} date-mod="invites">{$lang['170']}{$invites_count[1]}</a></li>
        <li><a class="right" date-mod="add_group">{$lang['178']}</a></li>
    </ul>
    <div id="first" class="tabs_all" data-id="$global_user" class="tabs_all">
        $group_list
    </div>
    <div id="invites" class="tabs_all" data-id="$global_user" class="tabs_all">
        $invites_list
    </div>
    <div id="add_group" class="tabs_all" data-id="$global_user" class="tabs_all">
        <form method="post" enctype="multipart/form-data" action="">
            <ul>
                <li>
                    <input name="group_name" type="text" placeholder="{$lang['179']}" required="required" />
                    <span class="warn"><img></span>
                </li>
                <li>
                    <textarea name="group_description" placeholder="{$lang['140']}" required="required"></textarea>
                </li>
                <li>
                    <label for="group_img">{$lang['180']}</label>
                    <input type="file" name="image" id="group_img" required="required" />
                </li>
                <li>
                    <label for="group_state">{$lang['181']}</label>
                    <select id="group_state" name="group_state" required="required">
                        $countries
                    </select><br />&nbsp;
                </li>
				<li>
					<label for="private_group">Закрытое сообщество (доступ только по приглашениям)</label>
					<input type="checkbox" name="private_group" id="private_group">
				</li>
                <li style="height:60px;">
                    <input type="submit" name="new_group" value="{$lang['113']}" class="button_enter"/>
                </li>
            </ul>
        </form>
    </div>
EOD;
