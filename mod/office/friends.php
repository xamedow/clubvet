<?php
$PHP_SELF=$_SERVER['PHP_SELF']; if (!stripos($PHP_SELF,"index.php")) die ("Access denied"); 
// Friend list.
$friend_list = '';
$Db->query = "SELECT person_friends.id, friend_id, name FROM mod_person left JOIN person_friends ON mod_person.id_person = friend_id WHERE person_id=$global_user";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $img = (file_exists(DOCUMENT_ROOT."/upload/gallery/{$lRes['friend_id']}/logo.jpg"))
            ? "/upload/gallery/{$lRes['friend_id']}/logo.jpg"
            : '/images/default_profile_pic.jpg';
        $name = str_replace(' ', '<br>', trim($lRes['name']));

        $friend_list .= <<<EOD
        <div class="user_card" id="card{$lRes['id']}">
            <a href="/person/{$lRes['friend_id']}.html">
                <img src="$img" alt="{$lRes['name']}">
                <span>$name</span>
            </a>
            <div>
                <a href="/office/message/new?to={$lRes['friend_id']}">{$lang['157']}</a>
                <a href="#" class="delete_friend" data-person_id="{$personRes["friend_id"]}" data-friend_id="$global_user"">{$lang['158']}</a>
            </div>
        </div>
EOD;
    }
} else {
    $friend_list .= '<p><br />'.$lang["159"].'</p>';
}

// Friends of friends list.
$frfr_list = '';
$Db->query = "
SELECT person_friends.id, friend_id, name FROM mod_person
LEFT JOIN person_friends ON mod_person.id_person = friend_id
WHERE friend_id!=$global_user AND person_id in(
    SELECT friend_id FROM mod_person
    LEFT JOIN person_friends ON mod_person.id_person = friend_id
    WHERE person_id=$global_user)
";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $img = (file_exists(DOCUMENT_ROOT."/upload/gallery/{$lRes['friend_id']}/logo.jpg"))
            ? "/upload/gallery/{$lRes['friend_id']}/logo.jpg"
            : '/images/default_profile_pic.jpg';
        $name = str_replace(' ', '<br>', trim($lRes['name']));

        $frfr_list .= <<<EOD
        <div class="user_card" id="card{$lRes['id']}">
            <a href="/person/{$lRes['friend_id']}.html">
                <img src="$img" alt="{$lRes['name']}">
                <span>$name</span>
            </a>
        </div>
EOD;
    }
} else {
    $frfr_list .= '<br /><p>'.$lang["160"].'</p>';
}

// Invites.
$invites_list = '';
$invites_count = 0;
$Db->query = "SELECT person_invitations.id, init_by_id, name FROM mod_person left JOIN person_invitations
                ON mod_person.id_person = init_by_id WHERE target_id=$global_user AND person_invitations.status='open'";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $img = (file_exists(DOCUMENT_ROOT."/upload/gallery/{$lRes['init_by_id']}/logo.jpg"))
            ? "/upload/gallery/{$lRes['init_by_id']}/logo.jpg"
            : '/images/default_profile_pic.jpg';
        $name = str_replace(' ', '<br>', trim($lRes['name']));

        $invites_list .= <<<EOD
        <div class="user_card" id="card{$lRes['id']}">
             <a href="/person/{$lRes['init_by_id']}.html">
                <img src="$img" alt="{$lRes['name']}">
                <span>$name</span>
            </a>
            <div class="small">
                <a href="/office/message/new?to={$lRes['friend_id']}">{$lang['157']}</a>
                <a href="#" class="reject_invite" data-id="{$lRes['id']}">{$lang['161']}</a>
                <a href="#" class="accept_invite" data-id="{$lRes['id']}" data-init_id="{$lRes['init_by_id']}" data-person_id="{$global_user}">{$lang['162']}</a>
            </div>
        </div>
EOD;

        ++$invites_count;
    }
} else {
    $invites_list .= '<br /><p>'.$lang["163"].'</p>';
}

$invites_count = ($invites_count)
    ? array(" class=\"with_counter\"","<i class=\"counter\">$invites_count</i>")
    : array('','');

// Black list.
$blacklist = '';
$Db->query = "SELECT person_blacklist.id, banned_id, name FROM mod_person left JOIN person_blacklist ON mod_person.id_person = banned_id WHERE person_id=$global_user";
$Db->query();
if (mysql_num_rows($Db->lQueryResult) > 0) {
    while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $img = (file_exists(DOCUMENT_ROOT."/upload/gallery/{$lRes['banned_id']}/logo.jpg"))
            ? "/upload/gallery/{$lRes['banned_id']}/logo.jpg"
            : '/images/default_profile_pic.jpg';
        $name = str_replace(' ', '<br>', trim($lRes['name']));

        $blacklist .= <<<EOD
        <div class="user_card" id="card{$lRes['id']}">
             <a href="/person/{$lRes['banned_id']}.html">
                <img src="$img" alt="{$lRes['name']}">
                <span>$name</span>
            </a>
            <div>
                <a href="#" class="delete_banned" data-id="{$lRes['id']}">{$lang['164']}</a>
            </div>
        </div>
EOD;
    }
} else {
    $blacklist .= '<br /><p>'.$lang["165"].'</p>';
}

// Content output.
$content .= <<<EOD
    <h1 class="user_profile_name">{$lang['166']}</h1>
    <ul id="profile_menu_hor">
        <li class="active"><span date-mod="first">{$lang['167']}</span></li>
        <li><a date-mod="frfr">{$lang['168']}</a></li>
        <li><a date-mod="whats_new">{$lang['169']}</a></li>
        <li><a{$invites_count[0]} date-mod="invites">{$lang['170']}{$invites_count[1]}</a></li>
        <li><a date-mod="black_list">{$lang['171']}</a></li>
    </ul>
    <div id="first" data-id="$global_user" class="tabs_all">
        $friend_list
    </div>
    <div id="frfr" data-id="$global_user" class="tabs_all">
        $frfr_list
    </div>
    <div id="invites" data-id="$global_user" class="tabs_all">
        $invites_list
    </div>
    <div id="black_list" data-id="$global_user" class="tabs_all">
        $blacklist
    </div>
EOD;
