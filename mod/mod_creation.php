﻿<?
$PHP_SELF = $_SERVER['PHP_SELF'];
if (!stripos($PHP_SELF, "index.php")) {
    die ("Access denied");
}
$filter = new filter;

if ($param[1] == '') {
    $page = 1;
} else {
    $page = $filter->html_filter(@$param[1]);
}

$page_active = 'life';

$num = 7; // кол-во выводимых на страницу
$Db->query = "SELECT COUNT(*) FROM creation WHERE private=1";
$Db->query();
$lRes = mysql_fetch_assoc($Db->lQueryResult);
$items = $lRes["COUNT(id_file)"]; //кол-во 
$total = (($items - 1) / $num) + 1;

$a = substr($items, strlen($items) - 1, 1);
$b = substr($items, strlen($items) - 2, 2);
// склоняем кол-во
if ($a == 0 or ($a >= 5 and $a <= 9) or ($b >= 11 and $b <= 14)) {
    $items_print = $items . ' файлов';
}
if ($a == 1 and $b != 11) {
    $items_print = $items . ' файл';
}
if ($a >= 2 and $a <= 4 and $b != 12 and $b != 13 and $b != 14) {
    $items_print = $items . ' файла';
}
$total = intval($total); // общее число страниц
$page = intval($page);
if (empty($page) or $page < 0) {
    $page = 1;
}
if ($page > $total) {
    $page = $total;
}
$start = $page * $num - $num;
if ($start < 0) {
    $start = 0;
}

for ($i = 1; $i <= $total; $i++) {
    if ($page != $i) {
        $navi .= ' <a href=/life/' . $i . '/>' . $i . '</a> ';
    } else {
        $navi .= ' <b>' . $i . '</b> ';
    }
}

    $Db->query = "SELECT * FROM creation
                    LEFT JOIN mod_person ON owner=id_person
                    WHERE private=1 LIMIT $start, $num";
    $Db->query();

if (mysql_num_rows($Db->lQueryResult) > 0) {
    $content .= '<h1>' . $lang['319'] . '</h1>';
    while($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
        $id = $lRes["owner"];
        $img = (file_exists(DOCUMENT_ROOT."/upload/gallery/".$id."/logo.jpg"))
            ? '<img src="/upload/gallery/'.$id.'/logo.jpg" alt="" />'
            : '<img src="/images/default_profile_pic.jpg" alt="" />';

        $content .= <<<EOD
                                        <div class="reply">
											<a class="user" href="/person/'.$id.'.html">
												<div class="user_avatar">'.$img.'</div>
												<div class="user_name">{$lRes["name"]}</div>
											</a>
											<div class="comment">
												<p>{$lRes["text_creation"]}</p>
											</div>
										</div>
EOD;
    }
    if ($total > 1) {
        $navigation = "<div class='in_museum_paginator'>" . $navi . "</div>";
    }
    $content .= '</div><div class="clear"></div>' . $navigation;
} else {
    $content .= $lang['155'];
}
include("inc/header.php");
echo $content;
include("inc/footer.php");