<? $PHP_SELF=$_SERVER['PHP_SELF']; if (!stripos($PHP_SELF,"index.php")) die ("Access denied"); 
	// задаем страцниы работы модуля
	$pages_mod_auth = array("index","login","off","forget","forgetpass","activation","reg","send_me_code", "send_code_tender", "regme_tender");
	
	// вытаскиваем страницу из введенного адреса
	$filter = new filter;
	$page_name = substr($filter->html_filter(@$param[1]), 0, -5);
	if (!isset($page_name) || ($page_name=="") || (!in_array($page_name, $pages_mod_auth)) ) $page_name="index";
	
	// определяем введенную страницу модуля и действия с ними
	if ($page_name=="index" || $page_name=="login") 
	{
	// если нет кукисов и сессий
		if  ($autorized!=1)
            {
			// если не вводили пароль или логин выдаем форму для его ввода
			//if (!@$_POST["submit_login"]) {
    		if ( (empty($_POST["login"])) || (empty($_POST["password"])) ) {
				$content = '<div class="auth_form_bg" style="float: left; margin-left: 20px;">
						<h4>'.$lang["6"].'</h4>
						<form id="auth_form" action="/auth/" method="post">
							<input type="text" class = "email" name="login" placeholder="'.$lang["7"].'" />
							<input type="password" class = "pass" name="password" placeholder="'.$lang["8"].'" />
							<div class="checkbox"><input type="checkbox" name="remember" value="1" /></div>  
							<div class="form_notes">
								'.$lang["9"].'
								<a href="/auth/forget.html">'.$lang["10"].'</a>
							</div>
							<input type="submit" class="button_enter" value="'.$lang["11"].'" name="submit_login" />
						</form>
					</div>';
			}
			// если вводили пароль и логин проверяем и делаем вход
			else
				{
					
					$error = error_login();
					if ($error == 1)  $content = "<h1>".$lang['15']."</h1><div class='info'>".$lang['16']."</div>";      
					else
					{
						if (is_email($_POST["login"])) 
						{	
						$mail_ok = $_POST["login"]; 
						$filter = new filter; 
						$pass_ok = $filter->html_filter($_POST["password"]);
						$whois = $filter->html_filter($_POST["whois"]);
						$remember = $filter->html_filter($_POST["remember"]);
	
							$Db->query="SELECT `id_person`,`pass`,`act` FROM `mod_person` WHERE `mail`='".$mail_ok."'";
							$Db->query();
							$lRes=mysql_fetch_assoc($Db->lQueryResult);
							if (empty($lRes['pass']))
								{
									$Db->query="SELECT ip FROM error_login WHERE ip='".$ip."'";
									$Db->query();
									$lRes=mysql_fetch_assoc($Db->lQueryResult);
										if ($ip == $lRes[ip]) 
										{
											$Db->query="UPDATE `error_login` SET `col`=col+1,`date`=NOW() WHERE `ip`='".$ip."'";
											$Db->query();
										}          
											else 
										{
												$Db->query="INSERT INTO error_login (ip,date,col) VALUES  ('".$ip."',NOW(),'1')";
												$Db->query();
										}      
								$content = "<h1>".$lang['15']."</h1><div class='info'>".$lang['17']."</div>";
								}
							else 
								{
									if ($lRes['act']!=0)
									{
											$pass_solt = pass_solt($pass_ok);
											if ($lRes['pass']==$pass_solt) 
												{
													$hash = pass_solt(generateCode(10));
													$hash_cookie = pass_solt($hash.$lRes['id_person'].$lRes['pass']);
													
													mysql_query("UPDATE `mod_person` SET `hash`='".$hash."', `hash_cookie`='".$hash_cookie."' WHERE `id_person`='".$lRes['id_person']."'");
													$_SESSION['id_person']=$lRes['id_person']; 
													$_SESSION['hash']=$hash;

													if (@$remember==1){
														if (setcookie("myid", $lRes['id_person'], time()+9999999, "/") && setcookie("hash", $hash_cookie, time()+9999999, "/")) exit("<html><head><meta  http-equiv='Refresh' content='0; URL=/office/index/'></head></html>");
													}
													else exit("<html><head><meta  http-equiv='Refresh' content='0; URL=/office/index/'></head></html>");
												}
											else 
												{
													$content = "<h1>".$lang['6']."</h1><div class='info'>".$lang['17']."</div>";
												}
									}
									else $content = '<h1>'.$lang["6"].'</h1><div class="info">'.$lang["31"].'</div>';
								}
						}
					}
							
						
				}
			}
	// страница пользователя (личный кабинет)
			else
			{
			  exit("<html><head><meta  http-equiv='Refresh' content='0; URL=/office/index.html'></head></html>");
			}
	}
if ($page_name=="reg") 
{
			$footer_script = 1;
			$title = ''.$lang["12"].' - '.$config["main"]["main_title_".$set];
			$array_terms = array("rus"=>'Согласие с <a href="/content/56-pravilasaita.html" target="_blank">правилами сайта</a>', "ger"=>'Ich stimme mit <a href="/content/64-vorschriften.html" target="_blank">Nutzungs</a>', "eng"=>'Agree with <a href="/content/61-siteterms.html" target="_blank">Terms</a>');
			
	if (isset($_POST["submit_reg"])) //если нажата кнопка
		{
			$array_span = array("yes"=>"<span class='checkmsg'>&nbsp;&nbsp;</span>","no"=>"<span class='errormsg'>&nbsp;&nbsp;</span>");
			$filter = new filter;
			$name = $filter->html_filter($_POST["firstname"]); 
			$password = $filter->html_filter($_POST["password"]); 
			$confirm_password = $filter->html_filter($_POST["confirm_password"]); 
			$code = $filter->html_filter($_POST["captcha"]); 
			$phone = $filter->html_filter($_POST["phone"]); 
			
			$msg_error = array();
			if (is_email($_POST["mymail"])) $email = $_POST["mymail"]; else $email = "error";
			
			$Db->query="SELECT `id_person` FROM `mod_person` WHERE mail='".$email."' LIMIT 1";
			$Db->query();
			if (mysql_num_rows($Db->lQueryResult)>0) { $msg_error["mail"]="<p class='small_red'>".$lang['35']."</p>"; $input_error["mail"]=" error"; }
			
			
			if (empty($name)) { $input_error["name"]=" error"; $spanname = $array_span["no"];} else $spanname = $array_span["yes"];
			if (empty($password))  $input_error["pass1"]=" error";
			if (empty($confirm_password))  $input_error["pass2"]=" fc-error";
			if ($password==$confirm_password && strlen($password)<6) { $msg_error["pass"]="<p class='small_red'>".$lang['36']."</p>";  $input_error["pass1"]=" error";  $input_error["pass2"]=" error"; $spanpassword = $array_span["no"]; $spanconfirm_password = $array_span["no"];}
			if (empty($email))  {$input_error["mail"]=" error"; } 
			if ($email=="error")  {$input_error["mail"]=" error"; $msg_error["mail"]="<p class='small_red'>".$lang['37']."</p>"; $email = ""; $spanemail = $array_span["no"];}
			if ($password!=$confirm_password)  {$msg_error["pass2"]="<p class='small_red'>".$lang['38']."</p>"; $input_error["pass1"]=" error";  $input_error["pass2"]=" error"; $spanpassword = $array_span["no"]; $spanconfirm_password = $array_span["no"];} 
			if (!@$_POST["agree"]) $input_error["agree"]=" class='error'";
			if ($code!=$_SESSION['code']) { $input_error["code"]=" error"; $spancaptha = $array_span["no"];}
			if ($_POST['agree']==1) $chek = " checked";
		}	
		if (!empty($msg_error) or !empty($input_error) or !isset($_POST["submit_reg"])) //если есть ошибки при регистрации 
		{
	
	$content = '
	<h1>'.$lang["12"].'</h1><p>'.$lang["32"].'</p>
    <form id="regform" class="formular" method="post" action="/auth/reg.html">
			<fieldset>';
			if (!empty($msg_error)) foreach ($msg_error as $keymess=>$mess) $content.= $mess;
				$content.= '<label>
					<span>'.$lang['39'].': <span class="red">*</span></span><br />
					'.$spanname.'<input type="text" name="firstname" class="input'.$input_error["name"].'" value="'.$name.'" />
				</label>
				<label><br /><br />
					<span>'.$lang['7'].': <span class="red">*</span></span><br />
					'.$spanemail.'<input type="text" name="mymail" class="input'.$input_error["mail"].'" value="'.$email.'" />
				</label>
				<label><br /><br />
					<span>'.$lang['40'].': </span><br />
					'.$spanphone.'<input type="text" name="phone" class="input'.$input_error["phone"].'" value="'.$phone.'" />
				</label>
				<label><br /><br />
					<span>'.$lang['8'].': <span class="red">*</span></span><br />
					'.$spanpassword.'<input type="password" name="password" id="password" class="input'.$input_error["pass1"].'" value="'.$password.'" />
				</label>
				<label><br /><br />
					<span>'.$lang['41'].': <span class="red">*</span></span><br />
					'.$spanconfirm_password.'<input type="password" name="confirm_password" class="input'.$input_error["pass2"].'" value="'.$confirm_password.'" />
				</label>		<br /><br />
				<span>'.$lang['42'].' (<a name="kcaptcha" style="cursor:pointer;" class="blue" onclick="document.getElementById(\'captchaimage\').src=\'/inc/captcha.php?rand=\'+Math.round(1000 * Math.random());">'.$lang['43'].'</a>): </span><br />
				'.$spancaptha.'<img id="captchaimage" src="/inc/captcha.php" align="absmiddle" /> <input type="text" name="captcha" class="captcha input'.$input_error["code"].'" style="width: 445px;" />
				
			<br />
	<div '.$input_error["agree"].' style="text-align:right"><input type="checkbox" id="checkbox" class="checkbox" '.$chek.' id="agree" name="agree" align="middle" /> <span>'.$array_terms[$set].'</span></div></fieldset>
	<input class="button_reg" type="submit" style="width:220px;" name="submit_reg" value="'.$lang['44'].'"/>

</form>
	';
		}
		else if (isset($_POST["submit_reg"]) && empty($msg_error))  //если нет ошибок - регистрируем и отправляем письмо
		{
			unset($_SESSION['code']);
			$activation  = pass_solt($email);
			$pass = pass_solt($password);
			$sendemail= base64_encode($email);
			
			$Db->query="INSERT INTO mod_person (`pass`,`reg_date`,`last_date`,`mail`,`name`,`phone`,`lang`) VALUES('".$pass."', NOW(), NOW(), '".$email."', '".$name."','".$phone."','".$set."')";
			$Db->query();
 			$subject = $lang['45'].' '.$config["main"]["main_name_".$set];
            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="30%"><p align="center"><img src="http://clubveteranwar.com/images/logo.png" /></p></td>
						  <td><p>'.$config["main"]["main_name_".$set].'<br />
						  '.$config["main"]["main_email"].'</p></td>
							</tr>
						  </table>
							'.$lang['46'].' "'.$config["main"]["main_name_".$set].'".<br />
							<br />'.$lang['47'].':<br />'.$DomenName.'auth/activation.html/'.$activation.'/'.$sendemail.'/<br /><br />
							 '.$lang['48'].': '.$email.'<br />'.$lang['49'].': '.$password.'<br /><br /><br />'.$lang['50'].' '.$DomenName;
				

						$headers= "MIME-Version: 1.0\r\n";
						$headers.= "Content-type: text/html; charset=utf-8\r\n";
						$headers.= "From: ".$config["main"]["main_name_".$set]." <noreplay@noreplay.ru>";				
							
    		if (mail($email, $subject, $message, $headers)) $content = '<h1>'.$lang["12"].'</h1><p class="normtext">'.$lang["51"].'<br /><br />
			<div class="auth_form_bg" style="float: left; margin-left: 20px;">
						<h4>'.$lang["6"].'</h4>
						<form id="auth_form" action="/auth/" method="post">
							<input type="text" class = "email" name="login" placeholder="'.$lang["7"].'" />
							<input type="password" class = "pass" name="password" placeholder="'.$lang["8"].'" />
							<div class="checkbox"><input type="checkbox" name="remember" value="1" /></div>  
							<div class="form_notes">
								'.$lang["9"].'
								<a href="/auth/forget.html">'.$lang["10"].'</a>
							</div>
							<input type="submit" class="button_enter" value="'.$lang["11"].'" name="submit_login" />
						</form>
					</div>'; 

			$Db->query="INSERT INTO `mod_stat` (`name`, `date`) VALUES ('Зарегистрировался новый пользователь \"".$name."\"', NOW())";
			$Db->query(); 
				
  					
		}
}
		//страница активации акка
	if ($page_name=="activation")
	{
		$filter = new filter;
		$code = $filter->html_filter($param[2]); 
		$get_mail = $filter->html_filter($param[3]); 
		$page_active = "activation";
		if (is_email(base64_decode($get_mail))) 
		{
			$email = base64_decode($get_mail);
			$Db->query="SELECT `id_person` FROM `mod_person` WHERE mail='".$email."' LIMIT 1";
			$Db->query();
			if (mysql_num_rows($Db->lQueryResult)>0)
			{
				$activation = pass_solt($email);
				if ($code==$activation)
				{
					$lRes=mysql_fetch_assoc($Db->lQueryResult);
					$Db->query="UPDATE `mod_person` SET `act`='1' WHERE `id_person`='".$lRes['id_person']."'";
					$Db->query();
					$content = '<h1>'.$lang["52"].'</h1>
                    <div class="auth_form_bg">
						<h4>'.$lang["6"].'</h4>
						<form id="auth_form" action="/auth/" method="post">
							<input type="text" class = "email" name="login" placeholder="'.$lang["7"].'" />
							<input type="password" class = "pass" name="password" placeholder="'.$lang["8"].'" />
							<div class="checkbox"><input type="checkbox" name="remember" value="1" /></div>  
							<div class="form_notes">
								'.$lang["9"].'
								<a href="/auth/forget.html">'.$lang["10"].'</a>
							</div>
							<input type="submit" class="button_enter" value="'.$lang["11"].'" name="submit_login" />
						</form>
					</div>
					<div class="succesful">'.$lang["53"].'</div>
                </p></div>';
				}
				else
				{
					$content = "<h1>".$lang['52']."</h1><div class='sep'></div>&nbsp;<div class='normtext'>".$lang['54']."</div>";	
				}
			}
			else
			{
				$content = "<h1>".$lang['52']."</h1><div class='sep'></div>&nbsp;<div class='normtext'>".$lang['54']."</div>";	
			}
		}
		else 
		{
			$content = "<h1>".$lang['52']."</h1><div class='sep'></div>&nbsp;<div class='normtext'>".$lang['54']."</div>";
		}	
	}

		// страница повторного кода активации
		if ($page_name=="send_me_code")
		{
			if (!@$_POST['submit_send_me_code']) {
			$footer_script = 1;
			$content = '
				<h1>'.$lang["53"].'</h1>
				<form id="foget" action="/auth/send_me_code.html" method="post" class="formular">
					<fieldset>
				<label>
								<span>'.$lang["7"].': </span>
								<p><input type="text" name="login" class="input " /></p>
				</label>
				<input type="submit" class="button_enter" name="submit_send_me_code" value="'.$lang["56"].'">
				</fieldset><br />
				</form>	
				';
			}
			else
			{
				$filter = new filter; 
				if (is_email($_POST["login"])) $login_ok = $_POST["login"];
				$Db->query="SELECT `mail`, `id_person` FROM `mod_person` WHERE `mail`='".$login_ok."' LIMIT 1";
				$Db->query();
				$lRes=mysql_fetch_assoc($Db->lQueryResult);
    				if (empty($lRes['id_person'])) {
    				$content = "<h1>".$lang['53']."</h1><div class='sep'></div>&nbsp;<p>".$lang['57']."</p>";
    				}
					else {
						if (!empty($lRes['mail'])) {
						$activation  = pass_solt($lRes['mail']);
						$sendemail= base64_encode($lRes['mail']);
							
						$subject = $lang['58'];
						$message = $lang['59'].':<br />'.$DomenName.'auth/activation.html/'.$activation.'/'.$sendemail.'/<br /><br />'.$lang['50'].' '.$DomenName;
								$headers= "MIME-Version: 1.0\r\n";
								$headers.= "Content-type: text/html; charset=utf-8\r\n";
								$headers.= "From: ".$config['main_title_'.$set]." <noreplay@noreplay.ru>";		
						
								if (mail($lRes['mail'], $subject, $message, $headers)) $content = "<h1>".$lang['53']."</h1><p class='normtext'>".$lang['60']."</p>";
						}
					}	
			}
		}

		// страница восстановления пароля шаг 1
		if ($page_name=="forget")
		{
			if (!@$_POST['submit_forget']) {
			$footer_script = 1;
			$content = '
	<h1>'.$lang["61"].'</h1>
    <form id="foget" action="/auth/forget.html" method="post" class="formular">
		<fieldset>
			<label>
							<span>'.$lang["7"].': </span>
							
							<p><input type="text" name="login" class="input " /></p>
			</label>
    <br /><input type="submit" class="button_enter" name="submit_forget" value="'.$lang["56"].'">
	</fieldset><br />
	</form>	
	';
			}
			else
			{
				$filter = new filter; 
				if (is_email($_POST["login"])) $login_ok = $_POST["login"];
				$activation  = pass_solt($login_ok);
				$who = $filter->html_filter(@$_POST["who"]);
				

				$Db->query="SELECT * FROM `mod_person` WHERE `mail`='".$login_ok."' LIMIT 1";
				$Db->query();
				$lRes=mysql_fetch_assoc($Db->lQueryResult);
    				if (empty($lRes['id_person'])) {
    				$content = "<h1>".$lang["61"]."</h1><div class='sep'></div>&nbsp;<p>".$lang["57"]."</p>";
    				}
					else {
						if (!empty($lRes['mail'])) {
						$subject = $lang["62"];
            			$message = $lang["64"]." ".$DomenName."<br />".$lang["48"].": ".$login_ok."<br />".$lang["65"].":<br />".$DomenName."auth/forgetpass.html<br />".$lang["66"].": ".$activation."<br />".$lang["67"]."<br /><br />".$lang["50"]." ".$DomenName;
			$headers= "MIME-Version: 1.0\r\n";
						$headers.= "Content-type: text/html; charset=utf-8\r\n";
						$headers.= "From: ".$config['main_title_'.$set]." <noreplay@noreplay.ru>";		
	
    		if (mail($lRes['mail'], $subject, $message, $headers)) $content = "<h1>".$lang["61"]."</h1><div class='sep'></div>&nbsp;<p>".$lang["68"]."</p>";
						}
					}	
			}
		}
		// страница восстановления пароля шаг 2
		if ($page_name=="forgetpass")
		{
			if (!@$_POST['submit_forget']) {
			$content = '
		<h1>'.$lang["69"].'</h1><div class="sep"></div>&nbsp;
    	<form action="/auth/forgetpass.html" method="post" class="formular">
		<fieldset>
		<label>
					<span>'.$lang["7"].': </span>
					<p><input type="text" name="login" class="input" /></p>
		</label>
		<label>
					<span>'.$lang["66"].': </span>
					<p><input type="text" name="code" class="input" /></p>
		</label>
    	<input type="submit" name="submit_forget" value="'.$lang["56"].'" class="submit2">
		</fieldset>
		</form>	
		';
			}
			else
			{
				$filter = new filter; 
				if (is_email($_POST["login"])) $login_ok = $_POST["login"];
    			$code_ok = $filter->html_filter($_POST['code']);
				$activation  = pass_solt($login_ok);
				if ($activation==$code_ok) {
				$Db->query="SELECT * FROM `mod_person` WHERE `mail`='".$login_ok."' LIMIT 1";
				$Db->query();
				$lRes=mysql_fetch_assoc($Db->lQueryResult);
					if (empty($lRes['id_person'])) {
    				$content = $lang["57"];
    				}
					else {
						$email = $lRes['mail'];
						$mypass = rand(10000, 99999);
						$pass = pass_solt($mypass);
						$Db->query="UPDATE `mod_person` SET `pass`='".$pass."' WHERE `mail`='".$login_ok."'";
						$Db->query();
						$subject = $lang["62"];
            			$message = $lang["70"].": ".$mypass."<br /><br />".$lang["50"]." ".$DomenName;
			$headers= "MIME-Version: 1.0\r\n";
						$headers.= "Content-type: text/html; charset=utf-8\r\n";
						$headers.= "From: ".$config['main_title_'.$set]." <noreplay@noreplay.ru>";		
	
    		if (mail($email, $subject, $message, $headers)) $content = "<h1>".$lang["69"]."</h1><div class='sep'></div>&nbsp;<p>".$lang["71"]."</p>";
					}
				}
				else
				{
					$content = $lang["54"];
				}
			}
		}
include("inc/header.php");
echo $content;
include("inc/footer.php");
?>