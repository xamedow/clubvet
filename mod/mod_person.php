<?
$PHP_SELF = $_SERVER['PHP_SELF'];
if (!stripos($PHP_SELF, "index.php")) {
    die ("Access denied");
}

$filter = new filter;
$page = $filter->html_filter(@$param[1]);
$page_name = substr($page, 0, -5);

/*Таблица соответствия друзей*/
$Db->query = "SELECT * FROM `person_friends`";
$Db->query();
$table_person_friends=array();
if (mysql_num_rows($Db->lQueryResult) > 0) while ($sql_arr_tmp=mysql_fetch_assoc($Db->lQueryResult)) 
{
	$table_person_friends[$sql_arr_tmp["id"]]["person"]=$sql_arr_tmp["person_id"];
	$table_person_friends[$sql_arr_tmp["id"]]["friend"]=$sql_arr_tmp["friend_id"];
};	

if (!@($page_name) || what_ras($page) !== 'html') {

    
    // Фильтр по GET параметрам
    $get = clean_arr($_GET, $filter);
    $get_keys = array_keys($get);
    // Массив условий для подзапроса.
    $filter_arr = array(
        'war' => array(
            'join' => 'RIGHT JOIN `mod_war_rel` ON mod_war_rel.rel_person=mod_person.id_person',
            'where' => 'rel_event',
            'h1' => $lang['349']
        ),
        'cat' => array(
            'join' => '',
            'where' => 'front',
            'h1' => $lang['350']
        ),
        'arms' => array(
            'join' => 'RIGHT JOIN list_country_arm ON list_country_arm.id_arm = mod_person.rel_arm',
            'where' => 'rel_arm',
            'h1' => $lang['351']
        ),
        'letter' => array(
            'join' => '',
            'where' => 'LEFT(name,1)',
            'where_val' => $LangArrayAlfavit,
            'h1' => $lang['352']
        ),
        'country' => array(
            'join' => '',
            'where' => 'rel_country',
            'h1' => $lang['353']
        ),
        'friends_list' => array(
            'join' => 'LEFT JOIN person_friends ON id_person = friend_id',
            'where' => 'person_id',
            'h1' => $lang['354'],
			'and' => '0'
        ),
        'album_list' => array(
            'join' => 'LEFT JOIN `photoalbums` ON id_person = album_id_owner',
            'where' => 'album_id_owner',
            'h1' => $lang['355'],
            'page' => 'all_albums'
        )
    );

	$num = 21; // кол-во выводимых на страницу
    $page = @$page;
    $query = "SELECT COUNT(id_person) FROM `mod_person` WHERE `act`='1' AND `first`='1' AND `photo`='1'";
    foreach ($filter_arr as $key => $val) {
        if (in_array($key, $get_keys)) {

            $where_val = (!empty($val['where_val'])) ? $val['where_val'][$set][$get[$key]] : $get[$key];
            $where = (!empty($val['where'])) ? " AND {$val['where']}='$where_val'" : '';
			
            $query = "SELECT `name`, `id_person`, `user`, `up_date`, `last_date` FROM `mod_person`
				LEFT JOIN `list_sessions` ON (list_sessions.user=mod_person.id_person)
			    {$val['join']}
				WHERE `act`='1' AND `first`='1' AND `lang`='$set'{$where} GROUP BY `id_person`";
        }
    }
    $Db->query = $query;
    $Db->query();
	
	
    $lRes = mysql_fetch_assoc($Db->lQueryResult);
    $posts = $lRes["COUNT(id_person)"]; //кол-во
    $total = (($posts - 1) / $num) + 1;
    $total = intval($total); // общее число страниц
    $page = intval($page);
    if (empty($page) or $page < 0) {
        $page = 1;
    }
    if ($page > $total) {
        $page = $total;
    }
    $start = $page * $num - $num;
    if ($start < 0) {
        $start = 0;
    }


    $query = "SELECT * FROM `mod_person`
				LEFT JOIN `list_sessions` ON (list_sessions.user=mod_person.id_person)
				WHERE `act`='1' AND `first`='1' AND `photo`='1' AND `private`!='3' GROUP BY `id_person` ORDER BY reg_date DESC LIMIT $start, $num";
    $h1 = $lang['356'];

    foreach ($filter_arr as $key => $val) {
        if (in_array($key, $get_keys)) {

            $where_val = (!empty($val['where_val'])) ? $val['where_val'][$set][$get[$key]] : $get[$key];
            $where = (!empty($val['where'])) ? " AND {$val['where']}='$where_val'" : '';

			$where_cat = $val['where'];
			
			if ($where_cat=='rel_event'){
				$Db->query="SELECT `link` FROM `mod_war_event` WHERE `id_event`='".$where_val."' LIMIT 1";
				$Db->query();
				$link_info = mysql_fetch_assoc($Db->lQueryResult);
				$link_info = $link_info['link'];
			}
			
			if ($val['and']==0) $tmp_and=''; else $tmp_and="AND `photo`='1'";
			
            $query = "SELECT `name`, `id_person`, `user`, `up_date`, `last_date`, `private` FROM `mod_person`
				LEFT JOIN `list_sessions` ON (list_sessions.user=mod_person.id_person)
			    {$val['join']}
				WHERE `act`='1' AND `first`='1' $tmp_and {$where} GROUP BY `id_person` ORDER BY reg_date DESC LIMIT $start, $num";
            $h1 = $val['h1'];
            if ($val['page'] == 'all_albums') $its_albums = 1; else {
                $its_albums = 0;
            }
        }
    }
    $Db->query = $query;
    $Db->query();
    $content = "<h1>$h1</h1>";
	if ($where_cat=='rel_event') {
	$content.='<a class="war_info_href" href="'.$link_info.'">'.$lang['414'].'</a>';
	}

    if ($its_albums) {

        //получаем информацию об альбомах пользователя, доступных текущему пользователю
        $global_user = ($global_user) ? $global_user : 0;
        $query = "SELECT * FROM `photoalbums`
						LEFT JOIN `person_friends`
						ON (album_id_owner=friend_id)
						LEFT JOIN `person_blacklist`
						ON (album_id_owner=person_blacklist.person_id)

						WHERE album_id_owner='{$get[$key]}'
						AND ((album_private IN(1,2) and person_friends.person_id = " . $global_user . ") OR (album_private = 1) OR (album_private = 3 and banned_id != " . $global_user . "))
						AND flag='0' 

						GROUP BY album_id ORDER BY album_id";
        $Db->query = $query;
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $tmp_alb_numb = 1;
            $alb_ids = '(';
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $alb_info[$tmp_alb_numb] = $lRes;
                $alb_ids .= "'$lRes[album_id]',";
                $tmp_alb_numb++;
            }
            $alb_ids = substr($alb_ids, 0, -1);
            $alb_ids .= ')';
        }
        //формируем массивы с фотографиями из полученных выше альбомов
        if (!empty($alb_ids)) {
			$Db->query = "SELECT * FROM `photoalbums_files` WHERE album IN ".$alb_ids." ORDER BY id_file";
            $Db->query();
            if (mysql_num_rows($Db->lQueryResult) > 0) {

                while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                    $alb_content[$lRes['album']][] = $lRes;
                }
			}
                foreach ($alb_info as $k => $v) {
					$albname = ($v[album_name]) ? $v[album_name] : 'Безымянный';
					$albtext = ($v[album_text]) ? $v[album_text] : 'Нет описания';
					$albumcover = ($v[album_cover]) ? $v[album_cover] : '/images/empty_cover_alb.jpg';
                    $all_alb_list .= '
								<a class="all_albums_in_profile" href="' . $albumcover . '" rel="album_' . $k . '">
									<div class="all_albums_in_profile_cover"><img src="' . $albumcover . '"></div>
									<div class="album_text_overlay">' . $albtext . '</div>
									<div class="all_albums_in_profile_name">' . $albname . '</div>
								</a>
								<div class="all_albums_hidden_in_profile">';
                    if (!empty($alb_content)) foreach ($alb_content[$v['album_id']] as $kk => $vv) {
                        $all_alb_list .= '
										<a class="all_albums_in_profile" href="/upload/gallery/' . $get[$key] . '/album_' . $vv[album] . '/bg' . $vv[source] . '.jpg" rel="album_' . $k . '"></a>
									';
                    }
                    $all_alb_list .= '</div>';

                }
            
            $content .= $all_alb_list;
        } else {
            $content .= $lang['357'];
        }


    } else { /*Список друзей*/

        if (mysql_num_rows($Db->lQueryResult) > 0) {

            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $img = (file_exists(DOCUMENT_ROOT . "/upload/gallery/" . $lRes['id_person'] . "/logo.jpg"))
                    ? "/upload/gallery/" . $lRes['id_person'] . "/logo.jpg"
                    : '/images/default_profile_pic.jpg';

                $name = str_replace(' ', '<br>', trim($lRes['name']));

                if (!empty($lRes["user"])) {
                    $timeFrom = strtotime(date("H:i:s", strtotime($lRes["up_date"])));
                    $timeNow = strtotime(date("H:i:s"));
                    $diff = $timeNow - $timeFrom;

                    $days = floor($diff / (3600 * 24));
                    $hours = floor(($diff - ($days * 3600 * 24)) / 3600);
                    $minutes = floor(($diff - ($hours * 3600 + $days * 24 * 3600)) / 60);
                    if ($minutes > 15) {
                        $time = news_date($lRes["last_date"]);
                        $online = '<span class="online">'.$lang['358'].' ' . $time . '</span>';
                    } else {
                        $online = '<span class="online">'.$lang['359'].'</span>';
                    }

                } else {
                    $time = news_date($lRes["last_date"]);
                    $online = '<span class="online">'.$lang['358'].' ' . $time . '</span>';
                }
				
				$private_radio='0';
				
				if ($lRes['private']!='1') {
					foreach ($table_person_friends as $v)
					{
						if (($v['person']==$global_user)&&($v['friend']==$lRes['id_person'])&&($private_radio=='0')) $private_radio='1';
					}
				}
				else $private_radio='1';
				
				
				if ($private_radio) $content .= '<div class="user_card">
								<a href="/person/' . $lRes['id_person'] . '.html">
									<img src="' . $img . '" alt="' . $lRes['name'] . '">
									<span>' . $name . '</span>
								</a>
								' . $online . '
							</div>';
            }
        } else {
            $content .= $lang['360'];
        }
    }
    for ($i = 1; $i <= $total; $i++) {
        if ($page != $i) {
            $navi .= '<a href="/person/' . $i . '/">' . $i . '</a> | ';
        } else {
            $navi .= '<b>' . $i . '</b> |';
        }
    }
    $content .= '<div class="navi">' . $navi . '</div>';
    unset($date);
    unset($lRes);
    unset($total);
    unset($page);
    unset($start);
    unset($num);
    unset($posts);
} // Если задан параметр конкретной новости выводим ее
else {
    $Db->query = "SELECT mod_person.*, list_sessions.user, list_sessions.up_date, list_country.name_country_" . $set . ", list_country_arm.name_arm, list_country_arm_pod.name_pod
				FROM `mod_person`
				LEFT JOIN `list_sessions` ON (list_sessions.user=mod_person.id_person)
				LEFT JOIN `list_country` ON (list_country.id_country=mod_person.rel_country)
				LEFT JOIN `list_country_arm` ON (list_country_arm.id_arm=mod_person.rel_arm)
				LEFT JOIN `list_country_arm_pod` ON (list_country_arm_pod.id_pod=mod_person.rel_pod)
				WHERE `id_person`='" . $page_name . "' LIMIT 1";
    $Db->query();
    if (mysql_num_rows($Db->lQueryResult) > 0) {
        $personRes = mysql_fetch_assoc($Db->lQueryResult);
        if (!empty($personRes["user"])) {
            $timeFrom = strtotime(date("H:i:s", strtotime($personRes["up_date"])));
            $timeNow = strtotime(date("H:i:s"));
            $diff = $timeNow - $timeFrom;

            $days = floor($diff / (3600 * 24));
            $hours = floor(($diff - ($days * 3600 * 24)) / 3600);
            $minutes = floor(($diff - ($hours * 3600 + $days * 24 * 3600)) / 60);
            if ($minutes > 15) {
                $time = news_date($personRes["last_date"]);
                $online = '<span class="online">'.$lang['358'].' ' . $time . '</span>';
            } else {
                $online = '<span class="online">'.$lang['359'].'</span>';
            }

        } else {
            $time = news_date($personRes["last_date"]);
            $online = '<span class="online">'.$lang['358'].' ' . $time . '</span>';
        }
		
		/*Доступ для текущего пользователя к странице просматриваемого пользователя*/
		$access_radio='0';						
		if ($personRes['private']!='1') {
			foreach ($table_person_friends as $vvv)
			{
				if (($vvv['person']==$global_user)&&($vvv['friend']==$personRes['id_person'])&&($access_radio=='0')) $access_radio='1';
			}
		}
		else $access_radio='1';		
		
        $img = (file_exists(DOCUMENT_ROOT . "/upload/gallery/" . $personRes["id_person"] . "/logo.jpg"))
            ? '<a href="/upload/gallery/' . $personRes["id_person"] . '/bg_logo.jpg" class="fancybox"><img src="/upload/gallery/' . $personRes["id_person"] . '/logo.jpg" alt="" /></a>'
            : '<img src="/images/default_profile_pic.jpg" alt="" />';
        $content .= '
				<div id="left_navigation" style="height:auto;">
					<div class="profile_image">' . $img . '</div>';

        if ($autorized == 1 && $global_user != $page_name) {
            $Db->query = "SELECT status FROM person_invitations
                            	WHERE
                            		(init_by_id = $global_user AND target_id = {$personRes["id_person"]} )
                            		OR
                            		(init_by_id = {$personRes["id_person"]} AND target_id = $global_user )
                            	ORDER BY id DESC";
            $Db->query();
            $q_res = mysql_fetch_assoc($Db->lQueryResult);
            $status = (empty($q_res)) ? '' : $q_res['status'];
            switch ($status) {
                case 'open':
                    $friend_action = "<a href=\"#\" class=\"button_enter close_invite style_button_left\">".$lang['78']."</a>";
                    break;
                case 'rejected':
                    $friend_action = "<div class=\"friend_action\">".$lang['361']."</div>
                                    <a href=\"#\" class=\"button_enter add_invite style_button_left\"
                                    data-init_id=\"$global_user\"
                                    data-person_id=\"{$personRes["id_person"]}\">".$lang['241']."</a>";
                    break;
                case 'accepted':
                    $friend_action = "<div class=\"friend_action\">".$lang['362']."</div>
                                    <a href=\"#\" class=\"button_enter delete_friend style_button_left\"
                                     data-person_id=\"{$global_user}\" data-friend_id=\"{$personRes["id_person"]}\">".$lang['363']."</a>";
                    break;
                default:
                    $friend_action = "<a href=\"#\" class=\"button_enter add_invite style_button_left\"
                                    data-init_id=\"$global_user\"
                                    data-person_id=\"{$personRes["id_person"]}\">".$lang['241']."</a>";
                    break;
            }

            if ($status !== 'accepted') {
                $Db->query = "SELECT id FROM person_blacklist WHERE person_id = $global_user AND banned_id = '{$personRes["id_person"]}'";
                $Db->query();
                $lRes = mysql_fetch_assoc($Db->lQueryResult);
                if (!empty($lRes)) {
                    $friend_action .= "<div class=\"friend_action\">".$lang['364']."</div>
                                    <a href=\"#\" class=\"button_enter small delete_blacklist style_button_left\" data-id=\"{$lRes['id']}\">".$lang['365']."</a>";
                } else {
                    $friend_action .= "<a href=\"#\" class=\"button_enter add_blacklist style_button_left\" data-person_id=\"$global_user\" data-banned_id=\"{$personRes["id_person"]}\">".$lang['366']."</a>";
                }
            }
		
		/*Кнопка "Написать сообщение", появляется только если профиль открыт*/
        if ($access_radio) $content .= '<a href="/office/message/new?to=' . $personRes["id_person"] . '" class="button_enter style_button_left">'.$lang['157'].'</a>';
		
		$content .=$friend_action;
        }
        $Db->query = "SELECT mod_person.id_person,mod_person.name,mod_person.private FROM mod_person LEFT JOIN person_friends ON id_person = friend_id WHERE person_id = '{$personRes["id_person"]}' AND id_person != '$global_user' LIMIT 3";
        $Db->query();
        while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
            $img = (file_exists(DOCUMENT_ROOT . "/upload/gallery/{$lRes['id_person']}/logo.jpg"))
                ? "/upload/gallery/{$lRes['id_person']}/logo.jpg"
                : '/images/default_profile_pic.jpg';
				
			$friendlist_radio='0';
				
			if ($lRes['private']!='1') {
				foreach ($table_person_friends as $vv)
				{
					if (($vv['person']==$global_user)&&($vv['friend']==$lRes['id_person'])&&($friendlist_radio=='0')) $friendlist_radio='1';
				}
			}
			else $friendlist_radio='1';
			
            if ($friendlist_radio) $friends_list .= <<<EOD
                                            <a class="user" href="/person/{$lRes['id_person']}.html" style="margin-left:10px;">
												<div class="user_avatar"><img src="$img"></div>
												<div class="user_name">{$lRes["name"]}</div>
											</a>
EOD;
        }
        //получаем информацию об альбомах пользователя, общедоступных
        $global_user = ($global_user) ? $global_user : 0;
        $Db->query = "SELECT * FROM `photoalbums`
						LEFT JOIN `person_friends`
						ON (album_id_owner=friend_id)
						LEFT JOIN `person_blacklist`
						ON (album_id_owner=person_blacklist.person_id)

						WHERE album_id_owner='{$personRes["id_person"]}'
						AND ((album_private IN(1,2) and person_friends.person_id = " . $global_user . ") OR (album_private = 1) OR (album_private = 3 and banned_id != " . $global_user . "))
						AND flag='0'

						GROUP BY album_id LIMIT 2";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            $tmp_alb_numb = 1;
            $alb_ids = '(';
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $alb_info[$tmp_alb_numb] = $lRes;
                $alb_ids .= "'$lRes[album_id]',";
                $tmp_alb_numb++;
            }
            $alb_ids = substr($alb_ids, 0, -1);
            $alb_ids .= ')';
        }
        //формируем массивы с фотографиями из полученных выше альбомов
        if (!empty($alb_ids)) {
            $Db->query = "SELECT * FROM `photoalbums_files` WHERE album IN $alb_ids ORDER BY id_file";
            $Db->query();
            if (mysql_num_rows($Db->lQueryResult) > 0) {
                while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                    $alb_content[$lRes['album']][] = $lRes;
                }
			}
                foreach ($alb_info as $k => $v) {
					$albname = ($v[album_name]) ? $v[album_name] : 'Безымянный';
					$albtext = ($v[album_text]) ? $v[album_text] : 'Нет описания';
					$albumcover = ($v[album_cover]) ? $v[album_cover] : '/images/empty_cover_alb_prev.jpg';
                    $fotoalbums_list .= '
								<a class="fotoalb_in_profile" href="' . $albumcover . '" rel="album_' . $k . '" title="' . $albtext . '">
									<div class="fotoalb_in_profile_cover"><img src="' . $albumcover . '"></div>
									<div class="fotoalb_in_profile_name">' . $albname . '</div>
								</a>
								<div class="fotoalb_hidden_in_profile">';
                   if (!empty($alb_content)) foreach ($alb_content[$v['album_id']] as $kk => $vv) {
                        $fotoalbums_list .= '
										<a class="fotoalb_in_profile" href="/upload/gallery/' . $personRes["id_person"] . '/album_' . $vv[album] . '/bg' . $vv[source] . '.jpg" rel="album_' . $k . '"></a>
									';
                    }
                    $fotoalbums_list .= '</div>';
                }
            
        }

        $content .= '<div class="clear"></div>';

        if ($access_radio) $content .= <<<EOD
					<div class="auth_form_bg_sm">
						<h4 style="width: 120px"><a href="/person/?album_list={$personRes["id_person"]}">{$lang['367']}</a></h4>
							$fotoalbums_list
					</div>
					<div class="auth_form_bg_sm" style="padding-bottom:10px">
						<h4 style="width: 120px"><a href="/person/?friends_list={$personRes["id_person"]}">{$lang['166']}</a></h4>
							$friends_list
					</div>
EOD;
				
        $content .= '</div><div id= "personal_data">';


				
				
if ($access_radio)  {
        if ($personRes["date_birth"] == "0000-00-00") {
            $personRes["date_birth"] = $lang['203'];
        } else {
            $date2 = explode("-", $personRes["date_birth"]);
            $date = news_oc_date($personRes["date_birth"], $set);
            $personRes["date_birth"] = $date["day"] . " " . $date["month"] . " " . $date["year"];
        }
        if ($personRes["date_death"] == "0000-00-00") {
            $personRes["date_death"] = $lang['203'];
        } else {
            $date2 = explode("-", $personRes["date_death"]);
            $date = news_oc_date($personRes["date_death"], $set);
            $personRes["date_death"] = $date["day"] . " " . $date["month"] . " " . $date["year"];
        }
        if (empty($personRes["place_birth"])) {
            $personRes["place_birth"] = $lang['203'];
        }
        if (empty($personRes["location"])) {
            $personRes["location"] = $lang['203'];
        }

        $content .= '
					<h1 class="user_profile_name">' . $online . $personRes["name"] . '</h1><br />
					<ul id="profile_menu_hor">
						<li class="active"><span date-mod="general_info">'.$lang['205'].'</span></li>
						<li><a date-mod="war_info">'.$lang['206'].'</a></li>
						<li><a date-mod="battle_info">'.$lang['207'].'</a></li>
						<li><a date-mod="award_info">'.$lang['208'].'</a></li>
						<li><a date-mod="tech_info">'.$lang['209'].'</a></li>
						<li><a date-mod="wound_info">'.$lang['210'].'</a></li>
					</ul>

					<div id="general_info" class="tabs_all" style="padding-top: 10px;" data-id="' . $personRes["id_person"] . '" class="tabs_all">
						<ul>
							<li>
								<div class="save_info_div">'.$lang['212'].':<span>' . $personRes["name"] . '</span></div>
							</li>
							<li>
								<div class="save_info_div">'.$lang['213'].':<span>' . $personRes["date_birth"] . '</span></div>
							</li>
							<li>
								<div class="save_info_div">'.$lang['214'].':<span>' . $personRes["date_death"] . '</span></div>
							</li>
							<li>
								<div class="save_info_div">'.$lang['188'].':<span>' . $personRes["place_birth"] . '</span></div>
							</li>
							<li>
								<div class="save_info_div">'.$lang['189'].':<span>' . $personRes["location"] . '</span></div>
							</li>

							<li>'.$lang['216'].':<span>' . $personRes["name_country_" . $set] . ', ' . $personRes["name_arm"] . ', ' . $personRes["name_pod"] . '</span></li>
							<li>'.$lang['198'].':<span>' . $personRes["date_war"] . '</span></li>

							<li>'.$lang['176'].': </li>
						</ul>
						</div><!--#general_info-->

					<div id="war_info" class="tabs_all" style="padding-top: 10px;" data-id="' . $personRes["id_person"] . '">
						<div class="war_list">';

        $Db->query = "SELECT * FROM `list_war_info` WHERE `rel_person`='" . $personRes["id_person"] . "' ORDER BY `date_add` DESC";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $content .= '<p class="war_list_date" data-id="war_' . $lRes["id_war"] . '">'.$lang['220'].': ' . date(
                        "H:i",
                        strtotime($lRes["date_add"])
                    ) . ', ' . date(
                        "d.m.Y",
                        strtotime($lRes["date_add"])
                    ) . '</p><div class="war_list_text" data-id="war_' . $lRes["id_war"] . '">' . $lRes["text_war"] . '</div><hr data-id="war_' . $lRes["id_war"] . '" />';
            }
        } else {
            $content .= '<div class="reply"><p>'.$lang['222'].'.</p></div>';
        }

					$content .= '</div></div>

					<div id="battle_info" class="tabs_all" style="padding-top: 10px;" data-id="' . $personRes["id_person"] . '">
							<div class="war_event_list">';

        $Db->query = "SELECT * FROM `mod_war_rel`
									LEFT JOIN `mod_war_event` ON (mod_war_event.id_event=mod_war_rel.rel_event)
									LEFT JOIN `mod_war` ON (mod_war.id_war=mod_war_event.rel_war)
									WHERE mod_war_rel.rel_person='" . $personRes["id_person"] . "' ORDER BY `id_rel` DESC";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $content .= '<div class="war_list_event" data-id="rel_' . $lRes["id_rel"] . '">' . $lRes["name_war"] . ' / <a href="' . $lRes["link"] . '" target="_blank">' . $lRes["name_event_" . $set] . '</a></div><hr data-id="rel_' . $lRes["id_war"] . '" />';
            }
        } else {
            $content .= '<div class="reply"><p>'.$lang['227'].'.</p></div>';
        }


        $content .= '</div>
					</div>

					<div id="award_info" class="tabs_all" style="padding-top: 10px;" data-id="' . $personRes["id_person"] . '">
						<div class="award_list">';

        $Db->query = "SELECT * FROM `mod_award_rel`
									LEFT JOIN `mod_award` ON (mod_award.id_award=mod_award_rel.rel_award)
									LEFT JOIN `list_country` ON (mod_award.rel_country=list_country.id_country)
									WHERE mod_award_rel.rel_person='" . $personRes["id_person"] . "' ORDER BY `id_rel` DESC";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                if (empty($lRes["img_award"])) {
                    $img = '<img src="/upload/award/no_icon.png" alt="" height="120" align="left" />';
                } else {
                    $img = '<img src="/upload/award/' . $lRes["img_award"] . '.png" alt="" align="left" height="120" />';
                }
                $content .= '<div class="list_award" data-id="award_' . $lRes["id_rel"] . '">
								' . $img . '' . $lRes["name_country_" . $set] . '<br /><a href="/award/' . $lRes["id_award"] . '.html">' . $lRes["name_award"] . '</a><br /><em>' . $lRes["text_about"] . '</em></div>';
            }
        } else {
            $content .= '<div class="reply"><p>'.$lang['368'].'.</p></div>';
        }


        $content .= '</div>
						<div class="clear"></div>
					</div>

					<div id="tech_info" class="tabs_all" style="padding-top: 10px;" data-id="' . $personRes["id_person"] . '">
						<div class="teh_list">';


        $Db->query = "SELECT * FROM `list_teh_info` WHERE `rel_person`='" . $personRes["id_person"] . "' ORDER BY `date_add` DESC";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $content .= '<p class="war_list_date" data-id="teh_' . $lRes["id_teh"] . '">'.$lang['220'].': ' . date(
                        "H:i",
                        strtotime($lRes["date_add"])
                    ) . ', ' . date(
                        "d.m.Y",
                        strtotime($lRes["date_add"])
                    ) . '</p><div class="war_list_text" data-id="teh_' . $lRes["id_teh"] . '">' . $lRes["text_teh"] . '</div><hr data-id="teh_' . $lRes["id_teh"] . '" />';
            }
        } else {
            $content .= '<div class="reply"><p>'.$lang['222'].'.</p></div>';
        }

        $content .= '</div>
					</div>

					<div id="wound_info" class="tabs_all" style="padding-top: 10px;" data-id="' . $personRes["id_person"] . '">
						<div class="wound_list">';


        $Db->query = "SELECT * FROM `list_wound_info` WHERE `rel_person`='" . $personRes["id_person"] . "' ORDER BY `date_add` DESC";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {
            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $content .= '<p class="war_list_date" data-id="wound_' . $lRes["id_wound"] . '">'.$lang['220'].': ' . date(
                        "H:i",
                        strtotime($lRes["date_add"])
                    ) . ', ' . date(
                        "d.m.Y",
                        strtotime($lRes["date_add"])
                    ) . '</p><div class="war_list_text" data-id="wound_' . $lRes["id_wound"] . '">' . $lRes["text_wound"] . '</div><hr data-id="wound_' . $lRes["id_wound"] . '" />';
            }
        } else {
            $content .= '<div class="reply"><p>'.$lang['222'].'.</p></div>';
        }

        $content .= '</div>
					</div>

					<div id="reply_wall">
							<h2 class="user_profile_name">'.$lang['235'].'</h2>';

        if ($autorized == 1 && $global_user != $page_name) {
            $content .= '<form id="send_comment" data-mod="person" data-id="' . $page_name . '" style="overflow: hidden;">
								<textarea name="comment" class="comments" placeholder="'.$lang['289'].'" required="required" style="width: 620px;"></textarea>
								<input type="hidden" class="name" name="name" value="' . $global_res["name"] . '" />
								<input type="hidden" class="email" name="email" value="' . $global_res["mail"] . '" />
								<input type="submit" class="button_enter send_comment" value="'.$lang['290'].'" style="width: 180px; margin-right: 2px;" name="submit_comment" />
							</form>';
        }
        $content .= '<div class="list_comment">';
        $Db->query = "SELECT * FROM `mod_comment` WHERE `mod`='person' AND `page_id`='" . $personRes["id_person"] . "' ORDER BY `date` DESC";
        $Db->query();
        if (mysql_num_rows($Db->lQueryResult) > 0) {

            while ($lRes = mysql_fetch_assoc($Db->lQueryResult)) {
                $id = $lRes["rel_person"];
                $img = (file_exists(DOCUMENT_ROOT . "/upload/gallery/" . $id . "/logo.jpg"))
                    ? '<img src="/upload/gallery/' . $id . '/logo.jpg" alt="" />'
                    : '<img src="/images/default_profile_pic.jpg" alt="" />';

                $content .= '<div class="reply">
											<a class="user" href="/person/' . $id . '.html">
												<div class="user_avatar">' . $img . '</div>
												<div class="user_name">' . $lRes["fio_comment"] . '</div>
											</a>
											<div class="comment">
												<div class="date">' . news_date($lRes["date"]) . '</div>
												<p>' . $lRes["text_comment"] . '</p>
											</div>
										</div>';
            }

        } else {
            $content .= '<div class="reply"><p>'.$lang['222'].'.</p></div>';
        }
        $content .= '</div>';
        $content .= '</div><!--#reply_wall-->';

		
}/*Персональная информация закончилась*/
else {
$content .='<h1 class="user_profile_name">Пользователь '.$personRes["name"].' ограничил доступ к информации на своей странице.</h1>';
}
        $content .= '
					</div><div class="clear"></div>';
					


        unset($personRes);
    } else {
        header('HTTP/1.0 404 not found');
        $content = "<h1>".$lang['295'].".</h1><p>".$lang['284'].".</p><div class='eror404'></div>";
    }
}
include("inc/header.php");
echo $content;
unset($content);
include("inc/footer.php");