﻿$(document).ready(function(){
	var albums_count = Math.ceil(($('#albums_wrap').children().length)/2);
	albums_count*=270;
	$('#albums_wrap').css("height",albums_count);
	
	
	$('.dialog_history .dialog_one_msg_nr').mouseenter(function(){
		$(this).removeClass("dialog_one_msg_nr");
	});	
	
	$('.dialog_delete_msg').click(
		function(){
			var dataset = {
			id : $(this).data('id_msg')
			};
			$.ajax({
				type: 'POST',
				url: '../../inc/deletemsg.php',
				data: dataset,
				success: $(this).parent().remove()
			});
		}
	);
	
	$('.delete_album').click(
		function(){
			if (confirm("Вы действительно хотите удалить альбом?")) {
				dataset = {
				id : $(this).data('album_id'),
				ownerid: $(this).data('owner_id')
				};
				$.ajax({
					type: 'POST',
					url: '../../inc/deletealb.php',
					data: dataset,
					success: $(this).parent().parent().remove()
				});					
			} 
			else {
			
			}
		}
	);

	$('.delete_docs').click(
		function(){
			if (confirm("Вы действительно хотите удалить этот документ?")) {
				dataset = {
				id : $(this).data('docs_id'),
				owner: $(this).data('owner_id'),
				what: 'doc'
				};
				$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset,
					success: $(this).parent().parent().remove()
				});					
			} 
			else {
			
			}
		}
	);
	
	$('.delete_creation').click(
		function(){
			if (confirm("Вы действительно хотите удалить запись?")) {
				dataset = {
				id : $(this).data('creation_id'),
				owner: $(this).data('owner_id'),
				what: 'creation'
				};
				$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset,
					success: $(this).parent().remove()
				});					
			} 
			else {
			
			}
		}
	);
	
	
	$('.delete_mf').click(
		function(){
			if (confirm("Вы действительно хотите удалить этот файл?")) {
				dataset = {
				id : $(this).data('mf_id'),
				owner: $(this).data('owner_id'),
				what: 'mf'
				};
				$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset,
					success: $(this).parent().parent().remove()
				});					
			} 
			else {
			
			}
		}
	);
	
	$('.delete_md').click(
		function(){
			if (confirm("Вы действительно хотите удалить этот файл?")) {
				dataset = {
				id : $(this).data('mf_id'),
				owner: $(this).data('owner_id'),
				what: 'md'
				};
				$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset,
					success: $(this).parent().parent().remove()
				});					
			} 
			else {
			
			}
		}
	);	

	$('#delete_mf_file').click(
		function(){
			dataset = {
				id : $(this).data('id'),
				what : 'mf_file',
				owner: $(this).data('owner')
			};
			$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset
			});
			$(this).parent().remove();
		}
	);
	
	$('#delete_md_file').click(
		function(){
			dataset = {
				id : $(this).data('id'),
				what : 'md_file',
				owner: $(this).data('owner')
			};
			$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset
			});
			$(this).parent().remove();
		}
	);
	
	$('#delete_docs_img').click(
		function(){
			dataset = {
				id : $(this).data('id'),
				what : 'doc_img',
				owner: $(this).data('owner')
			};
			$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset
			});
			$(this).prev().remove();
			$(this).next().remove();
			$(this).remove();
		}
	);
	
	$('#delete_album_cover').click(
		function(){
			dataset = {
				id : $(this).data('id'),
				what : 'cover',
				owner: $(this).data('owner')
			};
			$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset
			});
			$(this).prev().remove();
			$(this).next().remove();
			$(this).remove();
		}
	);
	
	$('.delete_album_photo').click(
		function(){
			dataset = {
				id : $(this).data('id'),
				what : 'photo',
				owner: $(this).data('owner')
			};
			$.ajax({
					type: 'POST',
					url: '../../inc/delete_inalb.php',
					data: dataset
			});
			$(this).parent().remove();			
		}
	);
	
	var pageHref=location.toString();
	var result = pageHref.slice(pageHref.search(/message/)+8).split('?');
	var whatpage = result[0];				
	
	if (whatpage=='new') 
	{
		MyFirst = $(".tabs_all").last();
		var DefaultActive = $('#profile_menu_hor').children().first();
		DefaultActive.html('<a date-mod="general_info">Входящие</a>');
		DefaultActive.removeClass('active');
		
		$('#profile_menu_hor').children().last().addClass('active');
		$('#profile_menu_hor').children().last().html("<span date-mod='new_message'>+ Новое сообщение</span>");
		
		$('#new_message').show();	
		$('.tabs_all').not('#new_message').hide();
	}
	
	if ($('#category').val()=='38') {$('#religion_category').show();$('#confession').show();}
	
	$('#category').change(
		function(){
			if ($(this).val()=='38') {$('#religion_category').show();$('#confession').show();}
			else {$('#religion_category').hide();$('#confession').hide();}
		}
	);
	
	
	
	
});	