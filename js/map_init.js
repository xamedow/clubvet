(function () {

    /**
     * Объект для работы со синхронными объектами карты.
     * @type {{mainMap: (*|jQuery|HTMLElement), blackout: (*|jQuery|HTMLElement), points: (*|jQuery|HTMLElement), marker: (*|jQuery|HTMLElement), init: init, hideMap: hideMap, showMap: showMap}}
     */
    var mapManage = {
            mainMap: $('#main_map'),
            blackout: $('div.blackout'),
            points: $('div.points'),
            marker: $('#marker'),
            init: function () {
                this.blackout.width($(document).width());
                this.blackout.height($(document).height());
                this.mainMap.height($(document).height() - 100);
                this.mainMap.width($(document).width() - 100);
                this.points.height($(document).height());
                $('.map_close').click(this.hideMap);
            },
            hideMap: function (e) {
                e.preventDefault();
                mapManage.blackout.hide();
                mapManage.points.hide();
                mapManage.marker.hide();
                $('<a/>', {
                    'href': '',
                    'class': 'map_open',
                    html: 'Отобразить карту',
                    click: mapManage.showMap
                }).appendTo($('#middle'));
            },
            showMap: function (e) {
                e.preventDefault();
                $(this).remove();
                mapManage.blackout.show();
                mapManage.points.show();
                mapManage.marker.show();
            }
        },
        /**
         * Объект для работы со списком точек захоронений.
         * @type {{points: {}, setPoints: setPoints}}
         */
            pointsInit = {
            points: {},
            setPoints: function () {
                $.ajax({
                    url: '/inc/ajax_xamedow.php',
                    type: 'post',
                    data: {
                        action: 'get_points',
                        person_id: 12
                    },
                    success: function (data) {
                        this.points = JSON.parse(data);
                        mapinit(this.points);
                    }
                });
            }
        },
        /**
         * Объект для работы с формой добавления точки захоронения.
         * @type {{form: (*|jQuery|HTMLElement), getAddForm: getAddForm, getSelect: getSelect}}
         */
            addPoint = {

            /**
             * Шаблон формы.
             */
            form: $('<form method="post" class="add_point">' +
                '<a class="map_close form_close" href="#"></a>' +
                '<h2>Добавить запись о памятном месте</h2>' +
                '<ul>' +
                '<label for="point_name">Название</label>' +
                '<li><input type="text" id="point_name" name="name"></li>' +
                '<label for="point_location">Расположение</label>' +
                '<li><input type="text" id="point_location" name="location"></li>' +
                '<label for="point_comment">Описание</label>' +
                '<li><textarea id="point_comment" name="comments"></textarea></li>' +
                '<li><input type="hidden" name="latitude"></li>' +
                '<li><input type="hidden" name="longitude"></li>' +
                '<li><input type="hidden" name="author_id"></li>'
            ),

            /**
             * Закрывает форму.
             * @param e
             */
            closeForm: function(e) {
                e.preventDefault();
                $(this).parents('form').remove();
            },

            /**
             * Создает и вставляет в DOM форму.
             * @param position - массив координат маркера точки. Индекс 0 - широта, 1 - долгота.
             */
            getAddForm: function (position) {
                var form = this.form,
                    formHolder = $('div.form_holder'),
                    applyPosition = function () {
                        form.find('input[name="latitude"]').val(position[0]);
                        form.find('input[name="longitude"]').val(position[1]);
                    };
                if ($('form.add_point')[0]) {
                    applyPosition();
                } else {
                        addPoint.getSelect('mod_burial', 'type', 'Тип', function () {
                            form.html(addPoint.form.html() +
                                '<li><input type="submit" value="Добавить" class="button_enter"/></li></ul></form>');
                            applyPosition();
                            form.prependTo('body');
                            addPoint.setListener();
                            addPoint.setDatePicker();
                            form.find('option[value="добавить тип"]').remove();
                            formHolder.prev().remove();
                            formHolder.remove();
                        });
                }
            },
            /**
             * Получает сформированные элементы select из элементов типа ENUM таблицы table, поля field с заголовком
             * label. Функция callback будет вызвана после получения данных.
             * @param table
             * @param field
             * @param label
             * @param callback
             */
            getSelect: function (table, field, label, callback) {
                $.ajax({
                    type: "post",
                    url: "/inc/ajax_xamedow.php",
                    data: {
                        scriptCharset: "utf-8",
                        table: table,
                        field: field,
                        label: label,
                        action: 'getSelect'
                    }
                }).done(function (data) {
//                    data = encodeURIComponent(data);
                    addPoint.form.html(addPoint.form.html() + data);
                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            },

            setAuthor: function (callback) {
                $.ajax({
                    type: 'post'
                });
            },
            setListener: function () {
                this.form.find('a.map_close').click(this.closeForm);
                this.form.find('input[type="submit"]').click(function (e) {
                    e.preventDefault();
                    $.ajax({
                        type: "post",
                        url: "/inc/ajax_xamedow.php",
                        data: {
                            data: $('form.add_point').serialize(),
                            action: 'saveForm'
                        }
                    }).done(function () {
                        // TODO make this reload free
                        window.location.reload();
                    });
                });
            },
            setDatePicker: function () {
                $.datepicker.setDefaults($.datepicker.regional['ru']);
                $('input[name="dob"]').datepicker({
                    dateFormat: "yy-mm-dd"
                });
                $('input[name="dod"]').datepicker({
                    dateFormat: "yy-mm-dd"
                });
            }
        },

        /**
         * Объект для работы со списком объектов на карте.
         * @type {{geoObjects: {}, elemsOnPage: number, menu: {}, pages: number, pageClass: string, points: {}, target: string, init: init, makePages: makePages, createMenu: createMenu, addListener: addListener, addPlaceMark: addPlaceMark, createBalloon: createBalloon}}
         */
            objectsList = {
            geoObjects: {},
            elemsOnPage: 10,
            fullname: '',
            menu: {},
            pages: 0,
            pageClass: '',
            points: {},
            target: '',
            /**
             * Конструктор для объекта. Возвращает ссылку на объект.
             * @param geoObjects - ссылка на свойство geoObjects, объекта ymaps.Map.
             * @param points - массив объектов.
             * @param pageClass - класс для единичного списка(страницы) объектов.
             * @param target - placeholder для списков объектов.
             * @returns {objectsList}
             */
            init: function (geoObjects, points, pageClass, target) {
                this.geoObjects = geoObjects;
                this.points = points;
                this.pageClass = pageClass;
                this.target = target;
                this.elemsOnPage = Math.floor(($(document).height() - 200) / 38);
                return this;
            },

            /**
             * Создает списки(страницы) объектов и считает их количесвто.
             */
            makePages: function () {
                for (var j = 0, l = this.points.length; j < l; j += this.elemsOnPage) {
                    this.menu = $(document.createElement('ul'));
                    this.menu.addClass(this.pageClass);
                    for (var i = j; i < j + this.elemsOnPage && i < l; ++i) {
                        if (this.points[i]) {
                            this.createEntry(this.points[i]);
                        }
                    }
                    this.menu.appendTo(this.target);
                    ++this.pages;
                }
            },

            /**
             * Callback функция для ассинхронного удаления точки с карты.
             * @param e - ссылка на событие.
             */
            pointEraser: function (e) {
                e.preventDefault();
                if (confirm('Вы действительно хотите удалить объект с карты?')) {
                    var that = $(this),
                        author_id = that.data('author_id'),
                        id = that.data('id');
                    $.ajax({
                        type: 'post',
                        url: '/inc/ajax_xamedow.php',
                        data: {
                            action: 'deletePoint',
                            id: id,
                            author_id: author_id
                        }
                    }).done(function () {
                        that.parent().remove();
                    });
                }
            },

            /**
             * Создает контролы редактирования и удаления для элемента списка.
             * @param objId - id элемента
             * @param authorId - id пользователя, добавившего объект.
             * @returns {*}
             */
            createControls: function (objId, authorId) {
                var data = 'data-author_id="' + authorId + '" data-id="' + objId + '"',
                    controls = {
                        eraser: '<a href="#" class="delete_map_point" title="Удалить объект"' + data + '><img src="/images/del_icon.png"></a>',
                        editor: '<a href="#" class="edit_map_point" title="Редактировать объект"' + data + '><img src="/images/pencil_icon.png"></a>'
                    };
                return controls.eraser + controls.editor;
            },

            /**
             * Создает элемент списка и добавляет объект на карту.
             * @param point - элемент массива points.
             */
            createEntry: function (point) {
                var controls = point['current_user'] === point['author_id']
                        ? this.createControls(point['id'], point['current_user']) : '',
                    name = point['name'] ? point['name'] : this.parseName(point['person_name'])[1],
                    entry = $('<li><a href="" class="p2p"> ' + name + '</a>' + controls + '<span>' + point['type'] + '</span>' + '</li>'),
                    placemark = this.addPlaceMark(point);
                this.addListener(entry, placemark);
                entry.appendTo(this.menu);
            },

            /**
             * Назначает обработчики на контролы в списке объектов.
             */
            addControlsListeners: function () {
                $('a.delete_map_point').click(this.pointEraser);
            },
            /**
             * Добавляет обработчик клика на элемент списка и маркер объекта на карте.
             * @param entry - элемент списка.
             * @param placemark - объект на карте.
             */
            addListener: function (entry, placemark) {
                entry.find('a.p2p').click(function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    placemark.balloon.open();
                });
            },

            /**
             * Создает объект элемента для карты, привязывая обрабочики карточки объекта к методам.
             * @param point - элемент массива points.
             * @returns {ymaps.Placemark} - ссылка на объект на карте.
             */
            addPlaceMark: function (point) {
                var url = '/upload/markers/' + point['marker'] + '.png',
                    body = this.chooseBalloonType(point),
                    BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                        body, {

                            // Переопределяем функцию build, чтобы при создании макета начинать
                            // слушать событие click на кнопке-счетчике.
                            build: function () {
                                // Сначала вызываем метод build родительского класса.
                                BalloonContentLayout.superclass.build.call(this);
                                // А затем выполняем дополнительные действия.
                                $('a.delete_map_point').bind('click', objectsList.pointEraser);
                                $('h4').bind('click', objectsList.foldPersonList);
                            }}),
                    placemark = new ymaps.Placemark(
                        [point['latitude'], point['longitude']],
                        {
                            hintContent: this.fullname
                        },
                        {
                            balloonContentLayout: BalloonContentLayout,
                            // Запретим замену обычного балуна на балун-панель.
                            // Если не указывать эту опцию, на картах маленького размера откроется балун-панель.
                            balloonPanelMaxMapArea: 0,
                            // тип макета.
                            iconLayout: 'default#image',
                            // Своё изображение иконки метки.
                            iconImageHref: url,
                            // Размеры метки.
                            iconImageSize: [34, 42],
                            // Смещение левого верхнего угла иконки относительно
                            // её "ножки" (точки привязки).
                            iconImageOffset: [-11, -37]
                        }
                    );
                this.geoObjects.add(placemark);
                return placemark;
            },

            foldPersonList: function(e) {
                var holder = $(this).find('span'),
                    dir = holder.html() === '▼' ? '▲' : '▼';
                holder.html(dir);
                $(this).next().toggle();
            },

            /**
             *  Выбирает метод - конструктор карточки объекта, по наличию личного кабинета, связанного с объектом.
             * @param point
             * @returns {*}
             */
            chooseBalloonType: function (point) {
                if (point['person_name']) {
                    return this.createPersonBalloon(point);
                }
                return this.createTextBalloon(point);
            },

            /**
             * Создает контролы для карточки объекта.
             * @param point
             * @returns {{message: string, erase: string}}
             */
            createBalloonControls: function (point) {
                var message = point['author_id']
                        ? '<a href="http://clubvet/office/message/new?to=' + point['author_id'] + '">Отправить сообщение автору</a>'
                        : '',
                    data = 'data-author_id="' + point['current_user'] + '" data-id="' + point['id'] + '"',
                    erase = point['current_user'] && point['author_id']
                        ? '<a href="#" class="delete_map_point" title="Удалить объект"' + data + '>Удалить объект с карты</a>'
                        : '';
                return {message: message, erase: erase};
            },

            /**
             * Разбирает переданную строку на массив с элементами [0] - id пользователя, [1] - имя пользователя
             * @param idNameString
             * @returns {*|Array}
             */
            parseName: function (idNameString) {
                return idNameString.split('|');
            },

            /**
             * Конструктор ссылок на ЛК пользователей.
             * @param point
             * @returns {*}
             */
            createPersonsLinks: function (point) {
                var names = point['person_name'],
                    idName, namesArr,
                    out = '', i = 0, l;
                if (names.indexOf(',') >= 0) {
                    namesArr = names.split(',');
                    for (l = namesArr.length; i < l; ++i) {
                        idName = this.parseName(namesArr[i]);
                        out += '<li>' + this.createPersonLink(idName[0], idName[1]) + '</li>';
                    }
                    return '<h4>' + point['name'] + ' (' + l + ' чел.) <span>▲</span></h4><ul>' + out + '</ul>';
                }

                idName = this.parseName(names);
                return this.createPersonLink(idName[0], idName[1], true);
            },

            /**
             * Конструктор ссылки на ЛК пользователя.
             * @param id
             * @param name
             * @param withImg
             * @returns {string}
             */
            createPersonLink: function (id, name, withImg) {
                withImg = withImg || false;
                var img = withImg ? '<img src="/upload/gallery/' + id + '/logo.jpg" alt="' + name + '">' : '';
                return '<a class="person_link" href="/person/' + id + '.html">' + img + '<strong>' + name + '</strong></a>';
            },

            /**
             * Создает карточку объекта на карте из параметров ЛК пользователей(я).
             * @param point
             * @returns {string}
             */
            createPersonBalloon: function (point) {
                var controls = this.createBalloonControls(point),
                    location = point['location']
                        ? 'Расположение объекта: ' + point['location'] + '<br>'
                        : '';

                return [
                    this.createPersonsLinks(point),
                    '<br>',
                    location,
                    'Тип объекта: ' + point['type'],
                    '<br>',
                    controls.message,
                    controls.erase
                ].join('');
            },

            /**
             * Создает карточку объекта на карте из параметров элемента, без привязки к ЛК.
             * @param point - элемент массива points.
             * @returns {string} - код сформированной карточки.
             */
            createTextBalloon: function (point) {
                var controls = this.createBalloonControls(point);

                return [
                    '<strong>' + point['name'] + '</strong>',
                    '<br/>',
                    'Дополнительная информация: ' + (point['comments'] ? point['comments'] : 'не указана'),
                    '<br/>',
                    'Тип объекта: ' + point['type'],
                    '<br/>',
                    controls.erase,
                    controls.message
                ].join('');
            },

            /**
             * Отображает страницу со списком объектов.
             * @param event
             */
            movePages: function (event) {
                event.preventDefault();
                var holder = $('div.points_holder'),
                    left = parseInt(holder.css('left'), 10),
                    width = holder.find('li').outerWidth(),
                    maxLength = (+objectsList.pages) * width - width;
                if ($(this).next()[0]) {
                    if (left < 0) {
                        holder.css('left', left + width + 'px');
                    } else {
                    }
                } else {
                    if (left > -maxLength) {
                        holder.css('left', left - width + 'px');
                    } else {
                    }
                }
            },

            /**
             * Подключает контролы для пагинации.
             */
            createPaginator: function () {
                if (this.pages > 1) {
                    $('<a/>', {
                        'href': '',
                        'class': 'mover',
                        html: '&#9668;',
                        click: this.movePages
                    }).appendTo(this.target.parent());
                    $('<a/>', {
                        'href': '',
                        'class': 'mover',
                        html: '&#9658;',
                        click: this.movePages
                    }).appendTo(this.target.parent());
                }
            }
        };


    function mapinit(points) {

        ymaps.ready(init);

        function init() {
            var mainMap = new ymaps.Map('main_map', {
                    center: [55.753994, 37.622093],
                    zoom: 12,
                    controls: [],
                    type: 'yandex#hybrid'
                }),
                markerElement = jQuery('#marker'),
                dragger = new ymaps.util.Dragger({
                    // Драггер будет автоматически запускаться при нажатии на элемент 'marker'.
                    autoStartElement: markerElement[0]
                }),
            // Смещение маркера относительно курсора.
                markerOffset,
                markerPosition,
            // Поиск по объектам.
                mySearchControl = new ymaps.control.SearchControl({
                    options: {
                        // Заменяем стандартный провайдер данных (геокодер) нашим собственным.
                        provider: new CustomSearchProvider(points),
                        // Не будем показывать еще одну метку при выборе результата поиска,
                        // т.к. метки коллекции myCollection уже добавлены на карту.
                        noPlacemark: true,
                        resultsPerPage: 5
                    }});

            // Вывод и пагинация списка объектов.
            objectsList.init(mainMap.geoObjects, points, 'points_menu', $('div.points_holder'));
            objectsList.makePages();
            objectsList.createPaginator();
            objectsList.addControlsListeners();

            // Поиск объектов
            function CustomSearchProvider(points) {
                this.points = points;
            }

            CustomSearchProvider.prototype.geocode = function (request, options) {
                var deferred = new ymaps.vow.defer(),
                    geoObjects = new ymaps.GeoObjectCollection(),
                // Сколько результатов нужно пропустить.
                    offset = options.skip || 0,
                // Количество возвращаемых результатов.
                    limit = options.results || 20;

                var points = [];
                // Ищем в свойстве text каждого элемента массива.
                for (var i = 0, l = this.points.length; i < l; i++) {
                    var point = this.points[i],
                        fullname = point['name'];

                    if (fullname.toLowerCase().indexOf(request.toLowerCase()) != -1) {
                        points.push(point);
                    }
                }
                // При формировании ответа можно учитывать offset и limit.
                points = points.splice(offset, limit);
                // Добавляем точки в результирующую коллекцию.
                for (var i = 0, l = points.length; i < l; i++) {
                    var point = points[i],
                        coords = [point['latitude'], point['longitude']],
                        text = point['name'];

                    geoObjects.add(new ymaps.Placemark(coords, {
                        name: text,
                        balloonContentBody: '<p>' + text + '</p>',
                        boundedBy: [coords, coords]
                    }));
                }

                deferred.resolve({
                    // Геообъекты поисковой выдачи.
                    geoObjects: geoObjects,
                    // Метаинформация ответа.
                    metaData: {
                        geocoder: {
                            // Строка обработанного запроса.
                            request: request,
                            // Количество найденных результатов.
                            found: geoObjects.getLength(),
                            // Количество возвращенных результатов.
                            results: limit,
                            // Количество пропущенных результатов.
                            skip: offset
                        }
                    }
                });

                // Возвращаем объект-обещание.
                return deferred.promise();
            };

            // Выставляем масштаб карты чтобы были видны все группы.
            mainMap.setBounds(mainMap.geoObjects.getBounds());
            // Добавляем контрол в верхний правый угол,
            mainMap.controls.add(mySearchControl, { right: 10, top: 10 });

            dragger.events
                .add('start', onDraggerStart)
                .add('move', onDraggerMove)
                .add('stop', onDraggerEnd);

            function onDraggerStart(event) {
                var offset = markerElement.offset(),
                    position = event.get('position');
                // Сохраняем смещение маркера относительно точки начала драга.
                markerOffset = [
                    position[0] - offset.left,
                    position[1] - offset.top
                ];
                markerPosition = [
                    position[0] - markerOffset[0],
                    position[1] - markerOffset[1]
                ];

                applyMarkerPosition();
            }

            function onDraggerMove(event) {
                applyDelta(event);
            }

            function onDraggerEnd(event) {
                applyDelta(event);
                markerPosition[0] += markerOffset[0];
                markerPosition[1] += markerOffset[1];
                // Переводим координаты страницы в глобальные пиксельные координаты.
                var markerGlobalPosition = mainMap.converter.pageToGlobal(markerPosition),
                // Получаем центр карты в глобальных пиксельных координатах.
                    mapGlobalPixelCenter = mainMap.getGlobalPixelCenter(),
                // Получением размер контейнера карты на странице.
                    mapContainerSize = mainMap.container.getSize(),
                    mapContainerHalfSize = [mapContainerSize[0] / 2, mapContainerSize[1] / 2],
                // Вычисляем границы карты в глобальных пиксельных координатах.
                    mapGlobalPixelBounds = [
                        [mapGlobalPixelCenter[0] - mapContainerHalfSize[0], mapGlobalPixelCenter[1] - mapContainerHalfSize[1]],
                        [mapGlobalPixelCenter[0] + mapContainerHalfSize[0], mapGlobalPixelCenter[1] + mapContainerHalfSize[1]]
                    ];
                // Проверяем, что завершение работы драггера произошло в видимой области карты.
                if (containsPoint(mapGlobalPixelBounds, markerGlobalPosition)) {
                    // Теперь переводим глобальные пиксельные координаты в геокоординаты с учетом текущего уровня масштабирования карты.
                    var geoPosition = mainMap.options.get('projection').fromGlobalPixels(markerGlobalPosition, mainMap.getZoom());
                    addPoint.getAddForm(geoPosition, markerPosition);
                }
            }

            function applyDelta(event) {
                // Поле 'delta' содержит разницу между положениями текущего и предыдущего события драггера.
                var delta = event.get('delta');
                markerPosition[0] += delta[0];
                markerPosition[1] += delta[1];
                applyMarkerPosition();
            }

            /**
             *  без фрагмента "- ($(document).width() - 980) / 2" некорректно работает dragger.
             */
            function applyMarkerPosition() {
                markerElement.css({
                    left: markerPosition[0],
                    top: markerPosition[1]
                });
            }

            function containsPoint(bounds, point) {
                return point[0] >= bounds[0][0] && point[0] <= bounds[1][0] &&
                    point[1] >= bounds[0][1] && point[1] <= bounds[1][1];
            }

        }

    }

    pointsInit.setPoints();
    mapManage.init();
}());