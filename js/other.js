var trans_obj;
$(function() {
    // Translantion handling.
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            table: 'translations'
        },
        success: function (data) {
            trans_obj = (JSON.parse(data));
        }
    });
    function replaceLinks(obj) { // Подстановка ссылок в текстах разделов "Боевой путь" и "Техника".
        // Получить массив имен/ссылок из таблицы в бд.
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                table: 'tech'
            },
            // Для каждого элемента полученного массива, искать совпадения в тексте и подставлять ссылки.
            success: function (data) {
                var techObj = (JSON.parse(data)),
                    target = $(obj),
                    temp = target.text();

                for (var key in techObj) {

                    var patt = new RegExp(techObj[key]['name_t'], 'ig');

                    var temp = temp.replace(
                        patt, '<a class="red_e" target="_blank" href="' + techObj[key]['link_t'] + '">' + techObj[key]['name_t'] + '</a>'
                    );
                }
                target.html(temp);
            }
        });
    }

    function showBanners() {
        var banners = $('.banners'),
            bannersWidth = banners.width() + banners.next().width(),
            docW = $('html').width(),
            mu = $('#menu_under'),
            muW = mu.width();

        if (docW - bannersWidth <= muW) {
            banners.css('display', 'none');
        } else {
            banners.css('display', 'block');
            $('.banners.left').css('left', mu.offset().left - $('.banners.left').width() + 'px');
            $('.banners.right').css('left', mu.offset().left + muW + 'px');
        }
    }
    showBanners();
    $(window).resize(function() {
        showBanners();
    });

    // Feed.
    var feed = $('.new_feed');
//    feed.hover(function() {
//        $(this).text('Лента обновлений');
//        $(this).css('color', '#000');
//        $(this).animate(
//            {left: 0, top: 0},
//            500
//        );
//    });
//    feed.mouseout(function() {
//        $(this).animate(
//            {left: '-50px', top: '-50px', color: '#FFF'},
//            500
//        );
//    });
    var id = feed.data('id');
    feed.fold({'id':id});
    feed.css('background', '#FFF url(\'/images/cloth.png\') 0 0 repeat');
    $('#turn_fold').click(function(e) {
        e.preventDefault();
        $(this).css('display','none');
        var id = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                "action" : "feed",
                "id": id
            },
            success: function (data) {
                $('body').prepend('<div class="darkout"><div class="feed_holder"><h2>Лента обновлений</h2></div>');

                var content = JSON.parse(data);
                var count = 0;
                for (var k in content) {
                        $('.feed_holder').append(content[k]);
                        ++count;
                }

                if(count === 0) {
                    $('.feed_holder').append('<p>Нет новостей обновлений.</p>');
                }

                $('.darkout').click(function() {
                    $(this).css('display', 'none');
                    $('#turn_fold').css('display','block');
                });
            }
        });

    });

});

	jQuery(document).ready(function(){


	$('#i_agree').click(function() {
				$("#robopay").removeAttr("disabled");
		});

		// фикс див

	var mytest = $(document).find("#fixed_div").length;
	if (mytest>0)
	{
	  var fixed = $("#fixed_div");
	  var offset = fixed.offset();
	  var topPadding = 35;
	  $(window).scroll(function() {
	  if ($(window).scrollTop() > offset.top) {
		fixed.stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding});
	  }
	  else {
		fixed.stop().animate({marginTop: "10px"});
	  };});
	}

		// для личного кабинета

		jQuery('.del_profile_image').click(function() {
			if(confirm(trans_obj[82]))
    		 {
				id=jQuery(this).attr('data-id');
				jQuery.post( "/inc/ajax.php", { 'comid' : id, 'page' : 'delimg'}, function( data ) {
					jQuery('.profile_image').html('<a class="edit_profile_image" href="/office/photo1/"></a><img src="/images/default_profile_pic.jpg" alt="" />');
				});

			 }
			 else
			 {
			   return false;
			 }
		});
		jQuery('#cropbox').Jcrop({
			aspectRatio:  152/169,
			onSelect: updateCoords
		});
					  function updateCoords(c)
					  {
						$('#x').val(c.x);
						$('#y').val(c.y);
						$('#w').val(c.w);
						$('#h').val(c.h);
					  };
					  function checkCoords()
					  {
						if (parseInt($('#w').val())) return true;
						alert(trans_obj[83]);
						return false;
					  };

		// сохранение информаци
		jQuery('#general_info').find("[data-role='button']").click(function() {
				jQuery(".save_info").hide();
				jQuery(".save_info_div").show();

				MyParent = jQuery(this).parent();
				MyParent.hide();
				MyParent.parent().children(".save_info").show();
			});

		jQuery('.save_button').click(function(e){
			e.preventDefault();
			MyParent = jQuery(this).parent();
			var m_data=MyParent.children(".mycount").val();
			var param=MyParent.children(".mycount").attr("name");

			jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'edit_info', 'param' : param}, function( data ) {
				/*MyParent.parent().html('<p><img src="/images/check.png" alt="" />&nbsp;&nbsp;&nbsp;Информация сохранена</p>');*/
				MyParent.parent().children(".save_info_div").children("span").text(m_data);
				if (param=='name') jQuery('h1.user_profile_name').text(m_data);
				MyParent.parent().children(".save_info_div").show();
				MyParent.hide();
			});
		});

		jQuery('.cancel_edit').click(function(e){
				MyParent = jQuery(this).parent();
				MyParent.hide();
				MyParent.parent().children(".save_info_div").show();
		});

		  // Календарь
		  jQuery("#date_birth, #date_death").datepicker({
			  changeMonth: true,
			  changeYear: true,
			  yearRange: "1850:2014"
			});

		  // переключение по вкладкам
		  // скрываем все кроме первой по началу
			 var MyFirst = $(".tabs_all").first();
			 $('.tabs_all').not(MyFirst).hide();
			 $('#profile_menu_hor li').click(function(e){
					var MyTabs = $(this).children().attr("date-mod");
					var MyClass = $(this).children().attr("class");
					var MyText = $(this).children().html();

					var MyActive = $('#profile_menu_hor').find("[class='active']");
					MyActive.html('<a class="'+MyActive.children("span").attr("class")+'" date-mod="'+MyActive.children("span").attr("date-mod")+'">'+MyActive.children("span").html()+'</a>');
					MyActive.removeClass("active");

					$(this).addClass("active");
					$(this).html("<span class='"+MyClass+"' date-mod='"+MyTabs+"'>"+MyText+"</span>");
					$('#'+MyTabs).show();
					$('.tabs_all').not('#'+MyTabs).hide();
			});

		// сохранение записей Боевой путь
		jQuery('.save_button_war').click(function(e){
			e.preventDefault();
			MyParent = jQuery(this).parent();
			var m_data=MyParent.children(".new_event").val();
			if (m_data!='')
			{
			jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'save_war'}, function( data ) {
//				MyParent.slideUp('slow');
				jQuery(".war_list").prepend('<p class="war_list_date">'+trans_obj[84]+'</p><div class="war_list_text">'+nl2br(m_data)+'</div><hr />');
			});
			}
			else alert(trans_obj[86]);
		});

		// удаление записей боевой путь
		jQuery('.war_list_del').click(function() {
			 if(confirm(trans_obj[85]))
    		 {
				id=jQuery(this).attr('data-id');
				jQuery.post( "/inc/ajax.php", {comid: id, page: 'del_list_war'},onAjaxSuccess);
					function onAjaxSuccess(data) {

						jQuery('.war_list').find("[data-id='war_"+id+"']").slideUp('slow');
					}
				}
			 else
			 {
			   return false;
			 }
		});

		// удаление записей сражения
		jQuery('.war_event_del').click(function() {
			 if(confirm(trans_obj[85]))
    		 {
				id=jQuery(this).attr('data-id');
				jQuery.post( "/inc/ajax.php", {comid: id, page: 'del_event_war'},onAjaxSuccess);
					function onAjaxSuccess(data) {
						jQuery('.war_event_list').find("[data-id='event_"+id+"']").slideUp('slow');
					}
				}
			 else
			 {
			   return false;
			 }
		});

		// сохранение записей Техника
		jQuery('.save_button_teh').click(function(e){
			e.preventDefault();
			MyParent = jQuery(this).parent();
			var m_data=MyParent.children(".new_event").val();
			if (m_data!='')
			{
			jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'save_teh'}, function( data ) {
				MyParent.slideUp('slow');
				jQuery(".teh_list").prepend('<p class="war_list_date">'+trans_obj[84]+'</p><div class="war_list_text">'+nl2br(m_data)+'</div><hr />');
			});
			}
			else alert(trans_obj[86]);
		});

		// удаление записей Техника
		jQuery('.teh_list_del').click(function() {
			 if(confirm(trans_obj[85]))
    		 {
				id=jQuery(this).attr('data-id');
				jQuery.post( "/inc/ajax.php", {comid: id, page: 'del_list_teh'},onAjaxSuccess);
					function onAjaxSuccess(data) {
						jQuery('.teh_list').find("[data-id='teh_"+id+"']").slideUp('slow');
					}
				}
			 else
			 {
			   return false;
			 }
		});

		// сохранение записей ранения
		jQuery('.save_button_wound').click(function(e){
			e.preventDefault();
			MyParent = jQuery(this).parent();
			var m_data=MyParent.children(".new_event").val();
			if (m_data!='')
			{
			jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'save_wound'}, function( data ) {
				MyParent.slideUp('slow');
				jQuery(".wound_list").prepend('<p class="war_list_date">Дата добавления: только что</p><div class="war_list_text">'+nl2br(m_data)+'</div><hr />');
			});
			}
			else alert(trans_obj[86]);
		});

		// удаление записей ранения
		jQuery('.wound_list_del').click(function() {
			 if(confirm(trans_obj[85]))
    		 {
				id=jQuery(this).attr('data-id');
				jQuery.post( "/inc/ajax.php", {comid: id, page: 'del_list_wound'},onAjaxSuccess);
					function onAjaxSuccess(data) {
						jQuery('.wound_list').find("[data-id='wound_"+id+"']").slideUp('slow');
					}
				}
			 else
			 {
			   return false;
			 }
		});

		// выбор и добавление сражений, вкладка "Сражения"
        function war_event(){
            jQuery("#event").attr('disabled', 'disabled');
            var war = jQuery("#war :selected").val();
            var m_name=jQuery("#war :selected").attr("data-name");

            jQuery.post( "/inc/ajax.php", { 'page' : 'select_war','war' : war, 'name' : m_name }, function( data ) {
                jQuery("#event").removeAttr("disabled");
                jQuery("#event").html(data);
            });

        }
        jQuery('#war').change(function(){
            jQuery("#event").attr('disabled', 'disabled');
            var war = jQuery("#war :selected").val();
            var m_name=jQuery("#war :selected").attr("data-name");

            jQuery.post( "/inc/ajax.php", { 'page' : 'select_war','war' : war, 'name' : m_name }, function( data ) {
                jQuery("#event").removeAttr("disabled");
                jQuery("#event").html(data);
            });

        });
        $('button[type="reset"]').click(function() {
        	var e = $("#event");
        	e.attr('disabled', 'disabled');
        	e.find('option').remove();
        	e.append('<option>Сражение</option>');
        });
//        war_event();

		jQuery('.save_event_war').click(function(e){
			e.preventDefault();
			MyParent = jQuery(this).parent();
			var m_data = MyParent.children("#event").val();
			var m_name = jQuery("#event :selected").attr("data-name");
			var m_id = jQuery("#event :selected").attr("data-id");

			if (m_data!=0)
			{
			jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'save_event_war'}, function( data ) {
				//MyParent.slideUp('slow');
					jQuery("#event").html('<option>'+trans_obj[100]+'</option>').attr('disabled', 'disabled');
					jQuery("#war option:nth-child(1)").attr("selected", "selected");

				jQuery(".war_event_list").prepend('<div class="war_list_event">'+m_id+' / '+m_name+'</div><hr />');
			});
			}
			else alert(trans_obj[99]);
		});

		// выбор и добавление наград, вкладка "Награды"
		jQuery('#country').change(function(){
			jQuery("#award").attr('disabled', 'disabled');
				var country = jQuery("#country :selected").val();
				var m_name=jQuery("#country :selected").attr("data-name");
				if (country!=5) // если выбрана Россия тогда показываем категории, см. ниже
				{
					jQuery.post( "/inc/ajax.php", { 'page' : 'select_country','country' : country, 'name' : m_name }, function( data ) {
						jQuery("#award").removeAttr("disabled");
						jQuery("#award").html(data);
					});
				}
				else
				{
					jQuery.post( "/inc/ajax.php", { 'page' : 'select_cat_award', 'name' : m_name }, function( data ) {
						jQuery("#rus_cat").removeAttr("disabled");
						jQuery("#rus_cat").show().html(data);
					});
				}

		});

		jQuery('#rus_cat').change(function(){
				var cat_award = jQuery("#rus_cat :selected").val();

					jQuery.post( "/inc/ajax.php", { 'page' : 'select_cat_award2','cat_award' : cat_award }, function( data ) {
						jQuery("#award").removeAttr("disabled");
						jQuery("#award").html(data);
					});
		});

		jQuery('#award').change(function(){
			var m_img=jQuery("#award :selected").attr("data-rel");
			if(m_img!='') jQuery(".award_preview img").attr("src","/upload/award/"+m_img+".png"); else jQuery(".award_preview img").attr("src","/upload/award/no_icon.png");
		});

		jQuery('.save_award').click(function(e){
			e.preventDefault();
			MyParent = jQuery(this).parent();
			var m_data = MyParent.children("#award").val();
			var m_text = MyParent.children(".new_award").val();
			var m_name = jQuery("#award :selected").attr("data-name");
			var m_img = jQuery("#award :selected").attr("data-rel");
			var m_country = jQuery("#country :selected").attr("data-name")
			if (m_data!='0')
			{
				jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'text' : m_text, 'page' : 'save_award'}, function( data ) {
					jQuery(".new_award").val('');
					jQuery("#award").empty().attr('disabled', 'disabled');
					jQuery("#rus_cat").empty().attr('disabled', 'disabled').hide();
					jQuery("#country option:nth-child(1)").attr("selected", "selected");

					//MyParent.slideUp('slow');
					if (m_img=='')
					{
						jQuery(".award_list").prepend('<div class="list_award"><img src="/upload/award/no_icon.png" height="120" alt="" align="left" />'+m_country+'<br />'+m_name+'<br /><em>'+nl2br(m_text)+'</em></div>');
					}
					else
					{
						jQuery(".award_list").prepend('<div class="list_award"><img src="/upload/award/'+m_img+'.png" height="120" alt="" align="left" />'+m_country+'<br />'+m_name+'<br /><em>'+nl2br(m_text)+'</em></div>');
					}
				});
			}
			else alert(trans_obj[87]);
		});
		// удаление наград
		jQuery('.award_del').click(function() {
			 if(confirm(trans_obj[88]))
    		 {
				id=jQuery(this).attr('data-id');
				jQuery.post( "/inc/ajax.php", {comid: id, page: 'del_award'},onAjaxSuccess);
					function onAjaxSuccess(data) {
						jQuery('.award_list').find("[data-id='award_"+id+"']").slideUp('slow');
					}
				}
			 else
			 {
			   return false;
			 }
		});

		// сохранение информации о службе
		jQuery('#war_info_save').submit(function(e){
			e.preventDefault();
			var m_data=jQuery(this).serialize();
			if (m_data!='')
			{
				jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'save_war_info'}, function( data ) {
					jQuery("#war_info_save").html('<p>'+trans_obj[89]+'</p>');
				});
			}
			else alert(trans_obj[90]);
		});


		// отправка заявки на родословную
		jQuery('#family_tree').submit(function(e){
			e.preventDefault();
			var m_data=jQuery(this).serialize();
			if (m_data!='')
			{
				jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'send_family_tree'}, function( data ) {
					jQuery("#family_tree").html('<div class="succesful">Ваша заявка отправлена</div>');
				});
			}
			else alert(trans_obj[90]);
		});


		// отправка заявки и комментариев на странице помогите найти

		jQuery('#i_search').submit(function(e){
			e.preventDefault();

            // Использование FormData, обусловлено невозможностью передать файлы аяксом через метод serialize.
            var form = document.forms.i_search,
                formData = new FormData(form),
                xhr = new XMLHttpRequest();

            formData.append('page', 'send_i_search');
            xhr.open('POST', '/inc/ajax.php');

            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    data = xhr.responseText;
                    if(data) {
                        $("#i_search").html("<div class='succesful'>Ваша заявка принята и после проверки модератором появится на сайте.</div>");
                    } else {
                        $("#i_search").html("<p>"+trans_obj[90]+"<p>");
                    }
                }
            };

            xhr.send(formData);

//			var m_data=jQuery(this).serialize();
//			if (m_data!='')
//			{
//				jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'send_i_search'}, function( data ) {
//					jQuery("#i_search").html('<div class="succesful">Ваша заявка принята и после проверки модератором появится на сайте.</div>');
//                    console.log(data);
//				});
//			}
//			else alert(trans_obj[90]);
		});

		jQuery('#send_comment').submit(function(e){
			e.preventDefault();
			var m_data=jQuery(this).serialize();
			var m_mod=jQuery(this).attr("data-mod");
			var m_id=jQuery(this).attr("data-id");
			var fio=jQuery('#send_comment .name').val();
			var comment=jQuery('#send_comment .comments').val();
			if (m_data!='')
			{
				jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'send_comment', 'mod' : m_mod, 'id' : m_id}, function( data ) {
					jQuery("#send_comment").slideUp("slow");
					jQuery(".list_comment").prepend('<div class="one_comment"><div class="date_comment">'+trans_obj[91]+'</div><span>'+fio+'</span><p>'+nl2br(comment)+'</p></div>');
				});
			}
			else alert(trans_obj[90]);
		});

		// прикрипление файлов в Нужна помощь
		var i = $('.dynamic-form input').size() + 1;

		if(i==2) jQuery('#remove').hide(); else jQuery('#remove').show();

		$('#remove').click(function() {
			if(i > 1) {
				$('.field:last').remove();
				i--;
			}
			if(i==2) jQuery('#remove').hide(); else jQuery('#remove').show();
		});
		$('#add').click(function() {
			$('<div class="field"><input name="file[]" type="file" required="required" /></div>').fadeIn('slow').appendTo('.inputs');
			i++;
			if(i==2) jQuery('#remove').hide(); else jQuery('#remove').show();
		});

		// ответить и удалить на своей стене
		jQuery('#reply_wall').find("[data-action='delete_comment']").click(function() {
			var m_id=jQuery(this).attr("data-id");
			jQuery.post( "/inc/ajax.php", { 'page' : 'delete_comment', 'id' : m_id}, function( data ) {
					jQuery("#reply_wall").find("[data-id='"+m_id+"']").slideUp("slow");
				});
		});

		jQuery('#reply_wall').find("[data-action='answer_comment']").click(function() {
			var m_id=jQuery(this).attr("data-id");
			jQuery(".answer_"+m_id).show("slow");
		});


		// ответ на стене отзывов
		jQuery('#send_comment_answer').submit(function(e){
			e.preventDefault();
			var m_data=jQuery(this).serialize();
			var m_id=jQuery(this).attr("data-id");
			var fio=jQuery('#send_comment_answer .name').val();
			var comment=jQuery('#send_comment_answer .comments').val();
			if (comment!='')
			{
				jQuery.post( "/inc/ajax.php", { 'value' : m_data, 'page' : 'send_comment_answer', 'id' : m_id}, function( data ) {
					jQuery(".answer_"+m_id).html('<div class="one_comment"><div class="date_comment">'+trans_obj[91]+'</div><span>'+fio+'</span><p>'+nl2br(comment)+'</p></div>');
				});
			}
			else alert(trans_obj[92]);
		});
		// конец

		// пошаговая фигня
		jQuery('#country2').change(function(){
				jQuery("#arm").attr('disabled', 'disabled');
				var country = jQuery("#country2 :selected").val();
				var m_name=jQuery("#country2 :selected").attr("data-name");
				if(country=="999")
				{
					jQuery("#country2").remove();
					jQuery("#arm").remove();
					jQuery("#pod").remove();
					jQuery(".rod_voisk").prepend('<div id="rod_voisk"><input type="text" name="rod_voisk" value="" placeholder="'+trans_obj[93]+'" /></div>');
					jQuery(".voin_form").prepend('<div id="voin_form"><input type="text" name="voin_form" value="" placeholder="'+trans_obj[94]+'" /></div>');
					jQuery(".country_form").prepend('<div id="country_form"><input type="text" name="country_form" value="" placeholder="'+trans_obj[96]+'" /></div>');
				}
				else
				{
					jQuery.post( "/inc/ajax_form.php", { 'page' : 'select_country2','country' : country, 'name' : m_name }, function( data ) {
						if (data==0)
						{
							if ($(document).find("#rod_voisk").length==0) jQuery(".rod_voisk").prepend('<div id="rod_voisk"><input type="text" name="rod_voisk" value="" placeholder="'+trans_obj[93]+'" /></div>');
							if ($(document).find("#voin_form").length==0) jQuery(".voin_form").prepend('<div id="voin_form"><input type="text" name="voin_form" value="" placeholder="'+trans_obj[94]+'" /></div>');
							jQuery("#arm").remove();
							jQuery("#pod").remove();
						}
						else
						{
							
							jQuery("#rod_voisk").remove();
							jQuery("#voin_form").remove();
							jQuery("#arm").show();
							jQuery("#pod").show();
							jQuery("#arm").removeAttr("disabled");
							jQuery("#arm").html(data);
						}
					});
				}
		});

		$("#my_summ").keypress(function (e)
        {
          //Если символ - не цифра, ввыодится сообщение об ошибке, другие символы не пишутся
          if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
          {
            //Вывод сообщения об ошибке
            $("#my_summ_span").html(trans_obj[98]).show().fadeOut("slow");
            return false;
          }
        });

		// пожертвования
		jQuery('#i_can_help').submit(function(e){
			e.preventDefault();
			var m_data=jQuery(this).serialize();
			var my_summ=jQuery('#my_summ').val();
			var name_need=jQuery('#name_need').val();
			var my_name=jQuery('#my_name').val();
			var id_need=jQuery('#rel_need').val();

			jQuery.post( "/inc/ajax_form.php", { 'value' : m_data, 'page' : 'pay_need_help', 'my_summ' : my_summ, 'my_name' : my_name, 'name_need' : name_need, 'id_need' : id_need}, function( data ) {
				jQuery("#hide_help").html(data);
			});
		});


		jQuery('#arm').change(function(){
			jQuery("#pod").attr('disabled', 'disabled');
				var arm = jQuery("#arm :selected").val();
				var m_name=jQuery("#arm :selected").attr("data-name");

				if(arm=="999")
				{
					jQuery("#pod").hide();
					jQuery(".rod_voisk").html('<input type="text" name="rod_voisk" value="" placeholder="'+trans_obj[93]+'" />');
					jQuery(".voin_form").html('<input type="text" name="voin_form" value="" placeholder="'+trans_obj[94]+'" />');
				}
				else
				{
					jQuery.post( "/inc/ajax_form.php", { 'page' : 'select_arm','arm' : arm, 'name' : m_name }, function( data ) {
						if (data==0)
						{
							if ($(document).find("#voin_form").length==0) jQuery(".voin_form").prepend('<div id="voin_form"><input type="text" name="voin_form" value="" placeholder="'+trans_obj[94]+'" /></div>');
							jQuery("#pod").remove();
						}
						else
						{
							jQuery("#voin_form").remove();
							jQuery("#pod").show();
							jQuery("#pod").removeAttr("disabled");
							jQuery("#pod").html(data);
						}
					});
				}

		});
		jQuery('#pod').change(function(){
			var pod = jQuery("#pod :selected").val();
				if(pod=="999")
				{
					jQuery(".voin_form").html('<input type="text" name="voin_form" value="" placeholder="'+trans_obj[94]+'" />');
				}
		});

		jQuery(".checkbox").mousedown(
			function() {
				changeCheck(jQuery(this));
			});
		jQuery(".checkbox").each(
			function() {
				changeCheckStart(jQuery(this));
		});
    });



	function nl2br( str ) {	return str.replace(/([^>])\n/g, '$1<br/>');	}


	function changeCheck(el) {
		var el = el,
		input = el.find("input").eq(0);
		if(!input.attr("checked")) {
			el.css("background-position","0 -18px");
			input.attr("checked", true)
		} else {
			el.css("background-position","center");
			input.attr("checked", false)
		}
		return true;
	}
	function changeCheckStart(el) {
		var el = el,
		input = el.find("input").eq(0);
		if(input.attr("checked")) {
			el.css("background-position","0 -18px");
		}
     return true;
	}

	var mytest2 = $(document).find("#scroll").length;
	if (mytest2>0)
	{
	$('#scroll').tinycarousel();
	var scroll= document.getElementById('scroll');

		var wrapper_bottom= document.getElementById('wrapper_bottom');
		var fixWidth = function() {
			if (document.body.clientWidth<=980) {
				scroll.style.marginLeft="-23px";
				scroll.style.marginRight="-23px";
			}
			else {
				scroll.style.margin="0 auto";
				wrapper_bottom.style.margin="0 auto";
			}
		}
		fixWidth();
		window.onresize =function(){fixWidth();}
	}

// Including xamedow.js

    // Fields check
    $('[name="group_name"]').blur(function () {
        var value = $(this).val();
        var target = $(this).next();
        target.html('<img>');

        function getWarn(target, text) {
            text = text || trans_obj[72];
            target.find('img').attr('src', '/images/del_icon.png');
            target.html(target.html() + text);
        }

        if (!value) {
            getWarn(target);
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                action: 'query',
                table: 'group'
            },
            success: function (data) {
                if (value && (data.toLowerCase().indexOf(value.toLowerCase() + '/')) >= 0) {
                    getWarn(target, trans_obj[73]);
                }
            }
        });
    });


    // Friends remover.
    $('.delete_friend').click(function (e) {
        e.preventDefault();
        if (confirm(trans_obj[63])) {
            var id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: '/inc/ajax_xamedow.php',
                data: {
                    person_id: $(this).data('person_id'),
                    friend_id: $(this).data('friend_id'),
                    action: 'delete',
                    table: 'friends'
                },
                success: function () {
                    $('#card' + id).remove();
                    $('.friend_action').text(trans_obj[74]);
                    $('.delete_friend').remove();
                }
            });
        }
    });
    // Banned remover.
    $('.delete_banned').click(function (e) {
        if (confirm(trans_obj[75])) {
            e.preventDefault();
            var id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: '/inc/ajax_xamedow.php',
                data: {
                    id: id,
                    action: 'delete',
                    table: 'blacklist'
                },
                success: function () {
                    $('#card' + id).remove();
                }
            });
        }
    });
    // Group remover.
    $('.delete_group').click(function (e) {
        e.preventDefault();
        if (confirm(trans_obj[76])) {
            var id = $(this).data('id');
            $.ajax({
                type: 'POST',
                url: '/inc/ajax_xamedow.php',
                data: {
                    id: id,
                    action: 'delete',
                    table: 'group'
                },
                success: function () {
                    $('#group_card' + id).remove();
                    $('.delete_group').remove();
                    window.location = '/groups/';
                }
            });
        }
    });
    $('.quit_group').click(function (e) {
        e.preventDefault();
        if (confirm(trans_obj[77])) {
            $.ajax({
                type: 'POST',
                url: '/inc/ajax_xamedow.php',
                data: {
                    person_id: $(this).data('person_id'),
                    group_id: $(this).data('group_id'),
                    action: 'quit',
                    table: 'group'
                },
                success: function (data) {
                    $('#card' + $(this).data('person_id')).remove();
                    $('.quit_group').remove();
                }
            });
        }
    });
    $('.delete_from_group').click(function (e) {
        e.preventDefault();
        if (confirm('Удалить пользователя?')) {
            $.ajax({
                type: 'POST',
                url: '/inc/ajax_xamedow.php',
                data: {
                    person_id: $(this).data('person_id'),
                    group_id: $(this).data('group_id'),
                    action: 'delete_from',
                    table: 'group'
                },
                success: function (data) {
                    $('#card' + $(this).data('person_id')).remove();
                    $('.delete_from_group').remove();
                    console.log(data);
                }
            });
        }
    });

    // Invites adding.
    $('.add_invite').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                init_id: $(this).data('init_id'),
                person_id: $(this).data('person_id'),
                action: 'add',
                table: 'invitations'
            },
            success: function () {
                $('.add_invite').text(trans_obj[78]);
                $('.friend_action').text(trans_obj[79]);
            }
        });
    });
    // Group invites adding.
    $('.add_invite').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                init_id: $(this).data('init_id'),
                person_id: $(this).data('person_id'),
                action: 'add',
                table: 'group_invitations'
            },
            success: function () {
                $('.add_invite').text(trans_obj[78]);
                $('.friend_action').text(trans_obj[79]);
            }
        });
    });

    // BlackList adding.
    $('.add_blacklist').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                banned_id: $(this).data('banned_id'),
                person_id: $(this).data('person_id'),
                action: 'add',
                table: 'blacklist'
            },
            success: function () {
                $('.add_blacklist').remove();
                $('.friend_action').text(trans_obj[80]);
            }
        });
    });
    // BlackList removing.
    $('.delete_blacklist').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                id: $(this).data('id'),
                action: 'delete',
                table: 'blacklist'
            },
            success: function () {
                $('.delete_blacklist').remove();
                $('.friend_action').text(trans_obj[81]);
            }
        });
    });

    // Invites rejecting
    $('.reject_invite').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var counter = $('i.counter');
        var counterText = +counter.text();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                id: id,
                action: 'rejected',
                table: 'invitations'
            },
            success: function () {
                if (counterText > 1) {
                    $('i.counter').text(--counterText);
                    $('#profile_menu_vert span').text(--counterText);
                } else {
                    counter.parent().removeClass('with_counter');
                    counter.remove();
                    $('#profile_menu_vert span div.counter').remove();
                }
                $('#card' + id).remove();
            }
        });
    });

    // Invites accepting
    $('.accept_invite').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var counter = $('i.counter');
        var counterText = +counter.text();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                id: $(this).data('id'),
                init_id: $(this).data('init_id'),
                person_id: $(this).data('person_id'),
                action: 'accepted',
                table: 'invitations'
            },
            success: function () {
                if (counterText > 1) {
                    $('i.counter').text(--counterText);
                    $('#profile_menu_vert span').text(--counterText);
                } else {
                    counter.parent().removeClass('with_counter');
                    counter.remove();
                    $('#profile_menu_vert span div.counter').remove();
                }
                $('#card' + id).remove();
            }
        });
    });
    // Group invites rejecting
    $('.reject_group_invite').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var counter = $('i.counter');
        var counterText = +counter.text();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                id: id,
                action: 'rejected',
                table: 'group_invitations'
            },
            success: function () {
                if (counterText > 1) {
                    $('i.counter').text(--counterText);
                    $('#profile_menu_vert span').text(--counterText);
                } else {
                    counter.parent().removeClass('with_counter');
                    counter.remove();
                    $('#profile_menu_vert span div.counter').remove();
                }
                $('#card' + id).remove();
            }
        });
    });

    // Group invites accepting
    $('.accept_group_invite').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var counter = $('i.counter');
        var counterText = +counter.text();
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                id: $(this).data('id'),
                init_id: $(this).data('init_id'),
                person_id: $(this).data('person_id'),
                action: 'accepted',
                table: 'group_invitations'
            },
            success: function () {
                if (counterText > 1) {
                    $('i.counter').text(--counterText);
                    $('#profile_menu_vert span').text(--counterText);
                } else {
                    counter.parent().removeClass('with_counter');
                    counter.remove();
                    $('#profile_menu_vert span div.counter').remove()
                }
                $('#card' + id).remove();
            }
        });
    });

    // Group invitation autocomplete.
    $(function() {
        var cache = {};
        var target = $( "#target_list" );
        target.autocomplete({
            minLength: 2,
            source: function( request, response ) {
                var term = request.term;
                var request_id = request;
                request.id = $('[name="init_by_id"]').val();
                request.group_id = $('.delete_group').data('id');

                if ( term in cache ) {
                    response( cache[ term ] );
                    return;
                }

                $.getJSON( "/inc/ajax_xamedow.php", request_id, function( data ) {
                    cache[ term ] = data;
                    response( data );
                });
            },
            select: function( event, ui ) {
                target.val( ui.item.label );
                $('[name="person_id"]').val(ui.item.id );
                return false;
            }
        });
    });

    // Group joining.
    $('.join_group').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var group_id = $(this).data('group_id');
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                person_id: id,
                init_id: group_id,
                action: 'accepted',
                table: 'group_invitations'
            },
            success: function () {
                window.location = '/groups/' + group_id + '.html';
            }
        });
    });