// Translantion handling.
var trans_obj = 23;
$(function() {
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            table: 'translations'
        },
        success: function (data) {
            trans_obj = (JSON.parse(data));

        }
    });
});

$(".user_card").click(function (e){
    e.preventDefault();
    console.log(trans_obj[63]);
});

// Fields check
$('[name="group_name"]').blur(function () {
    var value = $(this).val();
    var target = $(this).next();
    target.html('<img>');

    function getWarn(target, text) {
        text = text || 'Поле не заполнено';
        target.find('img').attr('src', '/images/del_icon.png');
        target.html(target.html() + text);
    }

    if (!value) {
        getWarn(target);
        return;
    }

    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            action: 'query',
            table: 'group'
        },
        success: function (data) {
            if (value && (data.toLowerCase().indexOf(value.toLowerCase() + '/')) >= 0) {
                getWarn(target, 'Название уже существует');
            }
        }
    });
});


// Friends remover.
$('.delete_friend').click(function (e) {
    e.preventDefault();
    if (confirm('Удалить пользователя из списка друзей?')) {
        var id = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                person_id: $(this).data('person_id'),
                friend_id: $(this).data('friend_id'),
                action: 'delete',
                table: 'friends'
            },
            success: function () {
                $('#card' + id).remove();
                $('.friend_action').text('Удален из друзей');
                $('.delete_friend').remove();
            }
        });
    }
});
// Banned remover.
$('.delete_banned').click(function (e) {
    if (confirm('Удалить пользователя из черного списка?')) {
        e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                id: id,
                action: 'delete',
                table: 'blacklist'
            },
            success: function () {
                $('#card' + id).remove();
            }
        });
    }
});
// Group remover.
$('.delete_group').click(function (e) {
    e.preventDefault();
    if (confirm('Удалить сообщество?')) {
        var id = $(this).data('id');
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                id: id,
                action: 'delete',
                table: 'group'
            },
            success: function () {
                $('#group_card' + id).remove();
                $('.delete_group').remove();
                window.location = '/groups/';
            }
        });
    }
});
$('.quit_group').click(function (e) {
    e.preventDefault();
    if (confirm('Выйти из сообшества ?')) {
        $.ajax({
            type: 'POST',
            url: '/inc/ajax_xamedow.php',
            data: {
                person_id: $(this).data('person_id'),
                group_id: $(this).data('group_id'),
                action: 'quit',
                table: 'group'
            },
            success: function (data) {
                $('#group_card' + $(this).data('person_id')).remove();
                $('.quit_group').remove();
                console.log(data);
            }
        });
    }
});

// Invites adding.
$('.add_invite').click(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            init_id: $(this).data('init_id'),
            person_id: $(this).data('person_id'),
            action: 'add',
            table: 'invitations'
        },
        success: function () {
            $('.add_invite').text('Отменить приглашение');
            $('.friend_action').text('Приглашение отправлено');
        }
    });
});
// Group invites adding.
$('.add_invite').click(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            init_id: $(this).data('init_id'),
            person_id: $(this).data('person_id'),
            action: 'add',
            table: 'group_invitations'
        },
        success: function () {
            $('.add_invite').text('Отменить приглашение');
            $('.friend_action').text('Приглашение отправлено');
        }
    });
});

// BlackList adding.
$('.add_blacklist').click(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            banned_id: $(this).data('banned_id'),
            person_id: $(this).data('person_id'),
            action: 'add',
            table: 'blacklist'
        },
        success: function () {
            $('.add_blacklist').remove();
            $('.friend_action').text('Добавлен в черный список');
        }
    });
});
// BlackList removing.
$('.delete_blacklist').click(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            id: $(this).data('id'),
            action: 'delete',
            table: 'blacklist'
        },
        success: function () {
            $('.delete_blacklist').remove();
            $('.friend_action').text('Удален из черного списка');
        }
    });
});

// Invites rejecting
$('.reject_invite').click(function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var counter = $('i.counter');
    var counterText = +counter.text();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            id: id,
            action: 'rejected',
            table: 'invitations'
        },
        success: function () {
            if (counterText > 1) {
                $('i.counter').text(--counterText);
                $('#profile_menu_vert span').text(--counterText);
            } else {
                counter.parent().removeClass('with_counter');
                counter.remove();
                $('#profile_menu_vert span div.counter').remove();
            }
            $('#card' + id).remove();
        }
    });
});

// Invites accepting
$('.accept_invite').click(function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var counter = $('i.counter');
    var counterText = +counter.text();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            id: $(this).data('id'),
            init_id: $(this).data('init_id'),
            person_id: $(this).data('person_id'),
            action: 'accepted',
            table: 'invitations'
        },
        success: function () {
            if (counterText > 1) {
                $('i.counter').text(--counterText);
                $('#profile_menu_vert span').text(--counterText);
            } else {
                counter.parent().removeClass('with_counter');
                counter.remove();
                $('#profile_menu_vert span div.counter').remove();
            }
            $('#card' + id).remove();
        }
    });
});
// Group invites rejecting
$('.reject_group_invite').click(function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var counter = $('i.counter');
    var counterText = +counter.text();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            id: id,
            action: 'rejected',
            table: 'group_invitations'
        },
        success: function () {
            if (counterText > 1) {
                $('i.counter').text(--counterText);
                $('#profile_menu_vert span').text(--counterText);
            } else {
                counter.parent().removeClass('with_counter');
                counter.remove();
                $('#profile_menu_vert span div.counter').remove();
            }
            $('#card' + id).remove();
        }
    });
});

// Group invites accepting
$('.accept_group_invite').click(function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var counter = $('i.counter');
    var counterText = +counter.text();
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            id: $(this).data('id'),
            init_id: $(this).data('init_id'),
            person_id: $(this).data('person_id'),
            action: 'accepted',
            table: 'group_invitations'
        },
        success: function () {
            if (counterText > 1) {
                $('i.counter').text(--counterText);
                $('#profile_menu_vert span').text(--counterText);
            } else {
                counter.parent().removeClass('with_counter');
                counter.remove();
                $('#profile_menu_vert span div.counter').remove()
            }
            $('#card' + id).remove();
        }
    });
});

// Group invitation autocomplete.
$(function() {
    var cache = {};
    var target = $( "#target_list" );
    target.autocomplete({
        minLength: 2,
        source: function( request, response ) {
            var term = request.term;
            var request_id = request;
            request.id = $('[name="init_by_id"]').val();
            request.group_id = $('.delete_group').data('id');

            if ( term in cache ) {
                response( cache[ term ] );
                return;
            }

            $.getJSON( "/inc/ajax_xamedow.php", request_id, function( data ) {
                cache[ term ] = data;
                response( data );
            });
        },
        select: function( event, ui ) {
            target.val( ui.item.label );
            $('[name="person_id"]').val(ui.item.id );
            return false;
        }
    });
});

// Group joining.
$('.join_group').click(function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var group_id = $(this).data('group_id');
    $.ajax({
        type: 'POST',
        url: '/inc/ajax_xamedow.php',
        data: {
            person_id: id,
            init_id: group_id,
            action: 'accepted',
            table: 'group_invitations'
        },
        success: function () {
            window.location = '/groups/' + group_id + '.html';
        }
    });
});